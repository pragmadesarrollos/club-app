import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { FCM } from '@capacitor-community/fcm';
import { PushNotifications } from '@capacitor/push-notifications';

import { UsuarioService } from 'src/app/services/usuario.service';
import { Platform } from '@ionic/angular';
import { ClubService } from './services/club.service';

import { NotificacionService } from './services/notificacion.service';
import { EncuestaService } from './services/encuesta.service';
import { EventsService } from './services/events.service';

import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  loadinMain: boolean;
  applicationLoaded: boolean = false;
  constructor(
    private router: Router,
    private _usuarioService: UsuarioService,
    public platform: Platform,
    private clubService: ClubService,
    private notificactionService: NotificacionService,
    private encuestasService: EncuestaService,
    private events: EventsService,
    public toastController: ToastController
  ) {
    this.platform.ready().then(() => {
      if (this.platform.is("capacitor") || this.platform.is("cordova")) {
        this.defineNotificacions();
      }
    });
    this.router.navigateByUrl('splash');

    this.clubService.getLoading().subscribe(b => {
      this.loadinMain = b;
    });

    /*setTimeout(() => {
      this.notificactionService.setNotificationId(43);
      this.presentToastWithOptions("Notificación");
    }, 3000);*/
  }


  private defineNotificacions() {
    const self = this;
    PushNotifications.checkPermissions().then((hasPermission) => {
      if (hasPermission.receive === 'granted') {
        console.log('FCM Has permission!');
      } else {
        console.error('FCM No Has permission');
      }
    });
    PushNotifications.addListener('registration', (token) => {
      console.log('FCM on registration TOKEN: ', JSON.stringify(token));
      this._usuarioService.guardarToken(token.value).subscribe((data) => {
        console.log(data);
      });
    });
    PushNotifications.addListener("registrationError", (error) => {
      console.error(`FCM on registrationError ${JSON.stringify(error)}`);
    });
    PushNotifications.addListener("pushNotificationReceived", (notification) => {
      console.log(`FCM on pushNotificationReceived ${JSON.stringify(notification)}`);
      if (notification.data.idNoti) {
        const id = notification.data.idNoti;
        this.notificactionService.setNotificationId(id);
        this.presentToastWithOptions("Notificación");
        this.events.broadcast("new.notification", {type: "notification", id: id});
      } else if (notification.data.idEncuesta) {
        const id = notification.data.idEncuesta;
        this.encuestasService.setEncuestaId(id);
        this.presentToastWithOptions("Encuesta");
        this.events.broadcast("event.notification");
        this.events.broadcast("new.notification", {type: "encuesta", id: id});
      }
    });
    PushNotifications.addListener("pushNotificationActionPerformed", (notification) => {
      console.log(`FCM on pushNotificationActionPerformed ${JSON.stringify(notification)}`);
      if (notification.notification.data.idNoti) {
        const id = notification.notification.data.idNoti;
        this.notificactionService.setNotificationId(id);
      } else if (notification.notification.data.idEncuesta) {
        const id = notification.notification.data.idEncuesta;
        this.encuestasService.setEncuestaId(id);
      }
      if (this.applicationLoaded) {
        this.events.broadcast("event.notification");
      }
    });
    PushNotifications.register();

    setTimeout(() => {
      this.applicationLoaded = true;
    }, 3000);

  }


  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Your settings have been saved.',
      duration: 2000
    });
    toast.present();
  }

  async presentToastWithOptions(type) {
    const toast = await this.toastController.create({
      message: 'Nueva ' + type,
      position: 'bottom',
      duration: 3500,
      buttons: [
        {
          icon: 'open',
          role: 'open',
          text: 'Abrir',
          handler: () => {
            toast.dismiss(null,'open');
            this.events.broadcast("event.notification");
            console.log('Abrir clicked');
          }
        }
      ]
    });
    await toast.present();

    const { role } = await toast.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);

    if(role !== 'open'){
      this.notificactionService.setNotificationId(null);
      this.encuestasService.setEncuestaId(null);
    }
  }


}
