export interface Rubro {
    id: number;
    nombre: string;
}



export interface Club {
    id: number;
    nombre: string;
    descripcion: string;
    logo: string;
    colorPrimario: string;
    colorTextoPrimario: string;
    colorSecundario: string;
    colorTextoSecundario: string;
    nombre_visible?: any;
    activo: number;
    email: string;
    telefono: string;
    cuit: string;
    instagram: string;
    facebook: string;
    twitter: string;
    cp: string;
    urlImage?: any;
    direccionId: number;
    personaId: number;
}

export interface Persona {
    id: number;
    nombre: string;
    apellido: string;
    documento: string;
    sexo: string;
    avatar?: any;
    correo: string;
    telefono: string;
    fechaNacimiento: string;
    direccionPersonaId?: any;
    tipoDocumentId: number;
}

export interface Usuario {
    id: number;
    idFirebase: string;
    ultimoIngreso: string;
    activo: number;
    personaId: number;
    persona: Persona;
}

export interface Beneficio {
    id: number;
    nombre: string;
    descripcion: string;
    telefono: string;
    web: string;
    instagram: string;
    correo: string;
    pathImage: string;
    activo: number;
    rubroId: number;
}

export interface BeneficiosPorClub {
    id: number;
    activo: number;
    clubId: number;
    usuarioId: number;
    beneficioId: number;
    club: Club;
    usuario: Usuario;
    beneficio: Beneficio;
}




export interface Rubro {
    id: number;
    nombre: string;
}

export interface BeneficioDetalle {
    id: number;
    nombre: string;
    descripcion: string;
    telefono: string;
    web: string;
    instagram: string;
    correo: string;
    pathImage: string;
    activo: number;
    rubroId: number;
    rubro: Rubro;
}