import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ReservaService {
  constructor(private http: HttpClient) {}

  getReservaByUsuario(idUsuario): Observable<any> {
    return this.http.get(
      `${environment.endpoint}/api/reserva/usuario/${idUsuario}`
    );
  }

  getReservaByUsuarioXClub(idUsuario, idClub): Observable<any> {
    return this.http.get(
      `${environment.endpoint}/api/reserva/usuario/${idUsuario}/${idClub}`
    );
  }

  postReserva(idUsuario, turno): Observable<any> {
    return this.http.post(
      `${environment.endpoint}/api/reserva/${idUsuario}/${turno}`,
      null
    );
  }

  eliminarReserva(id) {
    return this.http.delete(
      `${environment.endpoint}/api/reserva/${id}`
    );
  }

  getReservasActivasByUsuario(idClub, idUser) {
    return this.http.get(
      `${environment.endpoint}/api/reserva/usuario/club/${idUser}/${idClub}`
    );
  }

  getReservaById(id) {
    return this.http.get(
      `${environment.endpoint}/api/reserva/${id}`
    );
  }

  
  getReservaByEspacioByFecha(idEspacio, fecha) {
    return this.http.get(
      `${environment.endpoint}/api/reserva-filtro/${idEspacio}/${fecha}`
    );
  }

  getReservasInactivasByUsuario(idUsuario) {
    return this.http.get(
      `${environment.endpoint}/api/reserva/usuario/estado/${idUsuario}/0`
    );
  }

}
