import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, from, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { List } from 'linqts';
import * as moment from 'moment';
import { environment } from 'src/environments/environment';
import { UsuarioService } from './usuario.service';

@Injectable({
  providedIn: 'root'
})
export class NotificacionService {
  myAppUrl: string;

  idNotificationReceived = null;
  // Almacenamos datos de la notificación
  idNotificacion: number;
  titulo: string;
  descripcion: string;
  destinatarios = [];
  descripcionL: string;
  updateNotis: BehaviorSubject<number> = new BehaviorSubject(0);

  constructor(private http: HttpClient, private usuarioService: UsuarioService) {
    this.myAppUrl = environment.endpoint
  }

  updateNewNotifications() {
    this.updateNotis.next(new Date().getTime());
  }

  bindUpdateNotifications() {
    return this.updateNotis.asObservable();
  }

  setNotificationId(id) {
    this.idNotificationReceived = id;
  }

  getNotificationId() {
    const val = this.idNotificationReceived;
    this.idNotificationReceived = null;
    return val;
  }

  obtenerNotificaciones(): Observable<any> {
    return this.http.get(this.myAppUrl + '/api/notificacion')
  }

  obtenerNotificacionesPorId(id): Observable<any> {
    return this.http.get(this.myAppUrl + '/api/notificacion/' + id)
  }

  obtenerMisNotificacionesRecibidas(): Observable<any> {
    let tokenInfo = JSON.parse(localStorage.getItem('tokenInfo'));
    return this.http.get(`${this.myAppUrl}/api/notificacion/usuario/club/${tokenInfo.userId}/${tokenInfo.clubId}`).pipe(map((data: any[]) => {
      const list = new List(data);
      const alist = list.Where(p => p?.clubxusuario && p.clubxusuario.clubId === this.usuarioService.idClub)
        .Select(p => p?.notificacion)
        .DistinctBy(p => p.id)
        .OrderByDescending(p => moment(p.fecha, 'YYYY-MM-DD').toDate().getTime()).ThenByDescending(p=>p.id)
        .ToArray();
      return alist;
    }))
  }

  obtenerMisNotificacionesNoLeidas(): Observable<any> {
    let tokenInfo = JSON.parse(localStorage.getItem('tokenInfo'));
    return this.http.get(`${this.myAppUrl}/api/notificaciones-no-leidos/${tokenInfo.userId}/${tokenInfo.clubId}`);
  }

  obtenerMisNotificacionesLeidas(): Observable<any> {
    let tokenInfo = JSON.parse(localStorage.getItem('tokenInfo'));
    return this.http.get(`${this.myAppUrl}/api/notificaciones-leidas/${tokenInfo.userId}`).pipe(map((data: any[]) => {
      const list = new List(data);
      const alist = list.Where(p => p?.clubxusuario && p.clubxusuario.clubId === this.usuarioService.idClub)
        .Select(p => p?.notificacion)
        .DistinctBy(p => p.id)
        .OrderByDescending(p => moment(p.fecha, 'YYYY-MM-DD').toDate().getTime()).ThenByDescending(p=>p.id)
        .ToArray();
      return alist;
    }))
  }

  obtenerMisNotificacionesEnviadas(): Observable<any> {
    if (!this.usuarioService?.idUsuario || !this.usuarioService?.idClub) {
      return from([]);
    }
    return this.http.get(`${this.myAppUrl}/api/notificaciones-enviadas-por`).pipe(map((data: any[]) => {
      const list = new List(data);
      let alist = list.Where(p =>  p?.club && p.club.clubId === this.usuarioService.idClub)
        .Select(p => p?.club?.notificacion)
        .DistinctBy(p => p.id)
        .OrderByDescending(p => moment(p.fecha, 'YYYY-MM-DD').toDate().getTime()).ThenByDescending(p=>p.id).ToArray();
      return alist;
    }))
  }

  obtenerNotificacionesXClubxUsuario(idClub): Observable<any> {
    return this.http.get(this.myAppUrl + '/api/notificacion-clubxusuario/' + idClub)
  }

  crearNotificacion(notificacion): Observable<any> {
    return this.http.post(this.myAppUrl + '/api/notificacion', notificacion);
  }

  crearNotificacionPorClub(notificacion): Observable<any> {
    return this.http.post(this.myAppUrl + '/api/notificacion', notificacion);
  }

  crearNotificacionPorClubPorUsuario(idNotificacion: number, idClub: number): Observable<any> {
    return this.http.post(`${this.myAppUrl}/api/notificacion/${idNotificacion}/${idClub}`, null);
  }

  enviarNotificacion(notificacion, destinatarios) {
    return this.http.post(`${this.myAppUrl}/api/notificacion/crear`, {
      notificacion: notificacion, usuarios: destinatarios
    });
  }

  obtenerViewers(id) {
    return this.http.get(this.myAppUrl + '/api/notificacion/vistas/' + id);
  }
}
