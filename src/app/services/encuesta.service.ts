import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { UsuarioService } from './usuario.service';
import { List } from 'linqts';
import * as moment from 'moment';
import { Observable, from } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EncuestaService {
  myAppUrl: string;

  // Almacenamos datos de la encuesta
  idEncuesta: number;
  titulo: string;
  descripcion: string;
  preguntas: any[];
  destinatarios = [];
  idEncuestaReceived = null;

  constructor(private http: HttpClient, private usuarioService: UsuarioService) {
    this.myAppUrl = environment.endpoint
  }

  setEncuestaId(id) {
    this.idEncuestaReceived = id;
  }

  getEncuestaId() {
    const val = this.idEncuestaReceived;
    this.idEncuestaReceived = null;
    return val;
  }

  obtenerEncuestas(): Observable<any> {
    return this.http.get(this.myAppUrl + '/api/encuesta')
  }

  crearEncuesta(encuesta): Observable<any> {
    return this.http.post(this.myAppUrl + '/api/encuesta', encuesta);
  }

  crearPregunta(idEncuesta, pregunta): Observable<any> {
    return this.http.post(this.myAppUrl + '/api/pregunta/' + idEncuesta, pregunta);
  }

  //crear respuesta de id de una pregunta
  crearRespuesta(idPregunta, respuesta): Observable<any> {
    return this.http.post(this.myAppUrl + '/api/respuesta/pregunta/' + idPregunta, respuesta);
  }



  obtenerMisEncuestasRecibidas(): Observable<any> {
    if (!this.usuarioService?.idUsuario || !this.usuarioService?.idClub) {
      console.error("encuestas recibidas idUsuario || idClub is null");
      return from([]);
    }
    let tokenInfo = JSON.parse(localStorage.getItem('tokenInfo'))
    return this.http.get(`${this.myAppUrl}/api/encuesta/usuario/${this.usuarioService.idUsuario}/${tokenInfo.clubId}`).pipe(map((data: any[]) => {
      const list = new List(data);
 
      const alist = list
        .Where(p=>p && p?.encuesta)
        .Select(p => p?.encuesta)
        .DistinctBy(p => p.id)
        .OrderByDescending(p=>p.id)
        .ToArray();
      
      return alist;
    }))
  }

  obtenerMisEncuestasEnviadas(idUser): Observable<any> {
    let tokenInfo = JSON.parse(localStorage.getItem('tokenInfo'))
    return this.http.get(`${this.myAppUrl}/api/encuesta-enviada-por/${idUser}/${tokenInfo.clubId}`).pipe(map((data: any[]) => {
      const list = new List(data);

      let alist = list
        .Where(p=>p && p?.encuesta)
        .Select(p => p?.encuesta)
        .DistinctBy(p => p.id)
        .OrderByDescending(p=>p.id)
        .ToArray();
   
      return alist;
    }))
  }


  //traer a todas la preguntas de una encuesta": "GET /pregunta/:encuesta"
  obtenerEncustaPorId(idEncuesta): Observable<any> {
    return this.http.get(this.myAppUrl + '/api/encuesta/' + idEncuesta)
  }

  //traer a todas la preguntas de una encuesta": "GET /pregunta/:encuesta"
  obtenerPreguntas(idEncuesta): Observable<any> {
    return this.http.get(this.myAppUrl + '/api/pregunta/' + idEncuesta)
  }

  //trae toda las respuesta de una pregunta": "GET /respuesta/pregunta/:pregunta",
  obtenerRespuestas(idPregunta): Observable<any> {
    return this.http.get(this.myAppUrl + '/api/respuesta/pregunta/' + idPregunta)
  }

  enviarEncuesta(idEncuesta, destinatarios) {
    return this.http.post(`${this.myAppUrl}/api/encuesta-post`, {
      encuesta: idEncuesta, usuarios: destinatarios
    });
  }

  responderEncuesta(respuestas) {
    return this.http.post(`${this.myAppUrl}/api/respuesta/usuario/`, respuestas);
  }

  obtenerRespuestasPorUsuario(idEncuesta): Observable<any> {
    return this.http.get(this.myAppUrl + '/api/respuesta-encuesta/' + idEncuesta)
  }

  /*
"traer encuesta": "GET /encuesta",
"traer encuesta por id": "GET /encuesta/:id",
"modificar encuestar por id ": "PUT /encuesta/:id",
"eliminar encuestar por id ": "DELETE /encuesta/:id",
"enviar encuesta": "POST /encuesta-post",
"traer  pregunta por id": "GET /pregunta/id/:id",
"modificar pregunta por id": "PUT /pregunta/:id",
"eliminar pregunta por id": "DELETE /pregunta/:id",
"trae una respuesta por su id": "GET /respuesta/:id",
"modifica respuesta por su id": "PUT /respuesta/:id",
"elimina una respuesta por su id": "DELETE /respuesta/:id",*/



}
