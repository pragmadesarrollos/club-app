import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { OpenTelemetryService } from './opentelemetryService';



@Injectable()
export class TokenInterceptor implements HttpInterceptor {

    constructor(private openTelemetryService: OpenTelemetryService) {

    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const token: string = localStorage.getItem('token');
        const modifiedReq = req.clone({
            headers: req.headers.set('Authorization', `Bearer ${token}`),
        });

        const span = null; //this.openTelemetryService.createSpan(`${modifiedReq?.method?.toUpperCase()} ${modifiedReq?.url}`);
        span?.setAttribute('request.url', modifiedReq?.url);
        span?.setAttribute('request.body', JSON.stringify(modifiedReq?.body));
        span?.setAttribute('request.method', modifiedReq?.method?.toUpperCase());


        return next.handle(modifiedReq).pipe(map(event => {
            if (event instanceof HttpResponse) {
                event = event.clone({ body: event.body });
    

                span?.setAttribute('response.body', JSON.stringify(event.body));
                span?.setAttribute('status.code', event.status);

                span?.addEvent('test ' + event.status);

                if (event.status < 200 || event.status >= 400) {
                    span?.setAttribute('error', true);
                }
            } else {
                // console.error("response", event);
            }
            span?.end();
            return event;
        }));
    }
}