import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ClubInfo } from '../models/club.model';

@Injectable({
  providedIn: 'root'
})
export class ClubService {
  myAppUrl: string;
  loadingMain = new BehaviorSubject(false);

  clubChanged: BehaviorSubject<number> = new BehaviorSubject(-1);
  dashboardChanged: BehaviorSubject<number> = new BehaviorSubject(-1);

  constructor(private http: HttpClient) {
    this.myAppUrl = environment.endpoint;
  }

  changeClub(id) {
    this.clubChanged.next(id);
  }

  bindChangeClub() {
    return this.clubChanged.asObservable();
  }

  bindDashboardChanges() {
    return this.dashboardChanged.asObservable();
  }

  dashboardChanges() {
    this.dashboardChanged.next(new Date().getTime())
  }
  
  getLoading() {
    return this.loadingMain.asObservable();
  }

  setLoading(b) {
    return this.loadingMain.next(b);
  }

  obtenerClubes(): Observable<any> {
    return this.http.get(this.myAppUrl + '/api/clubs');
  }

  obtenerClubesXuser(): Observable<any> {
    let tokenInfo = JSON.parse(localStorage.getItem('tokenInfo'))
    return this.http.get(this.myAppUrl + '/api/usuario/no-clubs/' + tokenInfo.userId);
  }

  obtenerDisciplinasByIdClub(idClub: number): Observable<any> {
    return this.http.get(this.myAppUrl + '/api/disciplina-club/' + idClub);
  }

  obtenerClubesxId(idClub: number): Observable<ClubInfo> {
    return this.http.get<ClubInfo>(this.myAppUrl + '/api/clubs/' + idClub);
  }

  obtenerClubesxUsuario(idUsuario: number): Observable<any> {
    return this.http.get(this.myAppUrl + '/api/usuario/clubs/' + idUsuario);
  }
  //Posiciones de una disciplina
  obtenerPosicionesxDisciplina(idDisciplina: number): Observable<any> {
    return this.http.get(this.myAppUrl + '/api/posiciones/' + idDisciplina);
  }
  //Divisiones de una disciplina
  obtenerDivisionesxDisciplina(idClub: number, idDisciplina: number): Observable<any> {
    return this.http.get(this.myAppUrl + '/api/div/' + idClub + '/' + idDisciplina);
  }

  //Usuarios por club
  obtenerUsuariosFiltrosPorClub(idClub: number): Observable<any> {
    return this.http.get(this.myAppUrl + '/api/filtro-usuarios/' + idClub);
  }

  //Usuarios por club por disciplina
  obtenerUsuariosFiltrosPorClubPorDisciplina(idClub: number, idDisciplina: number): Observable<any> {
    return this.http.get(this.myAppUrl + '/api/filtro-usuarios/' + idClub + '/' + idDisciplina);
  }

  //Usuarios por club por disciplina por division
  obtenerUsuariosFiltrosPorClubPorDisciplinaPorDivision(idClub: number, idDisciplina: number, idDivision: number): Observable<any> {
    return this.http.get(this.myAppUrl + '/api/filtro-usuarios/' + idClub + '/' + idDisciplina + '/' + idDivision);
  }

  //Usuarios por club por disciplina por division por posicion
  obtenerUsuariosFiltrosPorClubPorDisiplinaPorDivisionPorPosicion(idClub: number, idDisciplina: number, idDivision: number, idPosicion: number): Observable<any> {
    return this.http.get(this.myAppUrl + '/api/filtro-usuarios/' + idClub + '/' + idDisciplina + '/' + idDivision + '/' + idPosicion);
  }

  //Obtener disciplinas que pertenecen a un usuario
  obtenerDisciplinasByUserId(idUser) {
    return this.http.get(this.myAppUrl + '/api/disciplina-usuario/' + idUser);
  }

  obtenerDisciplinasByUserIdAndClubId(idUser, idClub) {
    return this.http.get(this.myAppUrl + '/api/disciplina-usuario/' + idClub + '/' + idUser);
  }




}
