import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DisciplinaService {
  myAppUrl: string;

  constructor(private http: HttpClient) {
    this.myAppUrl = environment.endpoint
   }


  obtenerDisciplinasXClub(idClub): Observable<any> {
    return this.http.get(this.myAppUrl + '/api/disciplina-club/'+ idClub)
  }

  obtenerDisciplinasXId(idDisciplina): Observable<any> {
    return this.http.get(this.myAppUrl + '/api/disciplina/'+ idDisciplina)
  }

  obtenerDisciplinaXClubId(clubId, idDisciplina): Observable<any> {
    return this.http.get(this.myAppUrl + '/api/disciplina-club/'+ clubId + '/' + idDisciplina)
  }

  obtenerPosicionXDisciplinaId(idDisciplina): Observable<any> {
    return this.http.get(this.myAppUrl + '/api/posiciones/' + idDisciplina)
  }

  obtenerDivisionesxDisciplina(idClub: number, idDisciplina: number): Observable<any> {
    return this.http.get(this.myAppUrl + '/api/div/' + idClub + '/' + idDisciplina);
  }

  obtenerPosicionXDivisionId(idDivision) {
    return this.http.get(this.myAppUrl + '/api/div/' + idDivision);
  }

  agregarDeporteAlUsuario(userId, clubId, disciplina, division, posicion) {
    return this.http.post(this.myAppUrl + '/api/usuario-perfil/deporte/' + userId + '/' + clubId  + '/' + disciplina + '/' + division + '/' + posicion, null);
  }
 
}
