import { Injectable } from '@angular/core';
import { Subject, Observable, Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';

interface BroadcastEvent {
  key: string;
  data?: any;
}

@Injectable({
  providedIn: 'root'
})
export class EventsService {
  private _eventBus: Subject<BroadcastEvent>;
  private _subscriptions: {};

  constructor() {
    this._eventBus = new Subject<BroadcastEvent>();
    this._subscriptions = {};
  }

  broadcast(key: string, data?: any) {
    this._eventBus.next({ key, data });
  }

  on<T>(key: any): Observable<T> {
    return this._eventBus.asObservable()
      .pipe(filter(event => event.key === key))
      .pipe(map(event => <T>event.data));
  }

  publish(key: string, data?: any) {
    this._eventBus.next({ key, data });
  }

  subscribe<T>(key: string, callback: any) {
    const s = <Subscription>this._subscriptions[key];
    if (s) { s.unsubscribe(); }
    this._subscriptions[key] = this._eventBus.asObservable()
      .pipe(filter(event => event.key === key))
      .pipe(map(event => <T>event.data)).subscribe(callback);
  }

  unsubscribe(key: string) {
    const s = <Subscription>this._subscriptions[key];
    if (s) { s.unsubscribe(); }
  }
}
