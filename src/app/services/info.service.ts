import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class InfoService {
  constructor(private http: HttpClient) {}

  obtenerInfoxCategoria(cat): Observable<any> {
    let id = JSON.parse(localStorage.getItem('tokenInfo')).clubId;
    return this.http.get(`${environment.endpoint}/api/info/${id}/${cat}`);
  }

  obtenerInfoxClub(page?) {
    let id = JSON.parse(localStorage.getItem('tokenInfo')).clubId;
    return this.http.get(`${environment.endpoint}/api/page/info/${id}/${page || 1}`);
  }

  obtenerInfoxId(id): Observable<any> {
    return this.http.get<any>(`${environment.endpoint}/api/info/${id}`);
  }

}
