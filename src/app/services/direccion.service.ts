import { HttpClient  } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})

export class DireccionService {
  myAppUrl = environment.endpoint;

  constructor(private http: HttpClient) { }

  getTodasLasDirecciones(): Observable<any> {
    return this.http.get(this.myAppUrl + '/api/direccion')
  }

  getDireccionPorId(id): Observable<any> {
    return this.http.get(this.myAppUrl + '/api/direccion/' + id)
  }

}
