import { Injectable } from '@angular/core';
import { Device } from '@ionic-native/device/ngx';
import { UsuarioService } from './usuario.service';

/*
import { WebTracerProvider } from '@opentelemetry/web';
import { SimpleSpanProcessor,ConsoleSpanExporter, Tracer } from '@opentelemetry/tracing';
import {  CollectorTraceExporter }  from '@opentelemetry/exporter-collector';
*/
declare var require: any
const { WebTracerProvider } = require('@opentelemetry/web');
const { SimpleSpanProcessor, ConsoleSpanExporter } = require('@opentelemetry/tracing');
const { CollectorTraceExporter } = require('@opentelemetry/exporter-collector');

@Injectable({
  providedIn: 'root',
})
export class OpenTelemetryService {

  private tracer: any;
  private activeSpan: Span | null;
  constructor(private device: Device, private usuarioService: UsuarioService) {
    this.configureOpenTelemetry();
  }

  private configureOpenTelemetry() {
    const tracerProvider = new WebTracerProvider();
    const collectorOptions = {
      serviceName: 'proclubapp-tracer',
      url: 'https://jaegercollector.pragmadesarrollos.com/v1/trace',
      headers: {},
      concurrencyLimit: 10,
    };
    tracerProvider.addSpanProcessor(new SimpleSpanProcessor(new ConsoleSpanExporter()));
    tracerProvider.addSpanProcessor(new SimpleSpanProcessor(new CollectorTraceExporter(collectorOptions)));
    tracerProvider.register();

    this.tracer = tracerProvider.getTracer('agendapp-proclubapp');

  }

  createSpan(name: string): Span | null {
    return null;
    this.activeSpan = <Span>this.tracer.startSpan(name);
    try {
      this.activeSpan.setAttribute('device.uuid', this.device?.uuid);
      this.activeSpan.setAttribute('device.version', this.device?.version);
      this.activeSpan.setAttribute('device.model', this.device?.model);
      this.activeSpan.setAttribute('device.serial', this.device?.serial);
      this.activeSpan.setAttribute('device.isVirtual', this.device?.isVirtual);
      this.activeSpan.setAttribute('device.manufacturer', this.device?.manufacturer);
      this.activeSpan.setAttribute('device.cordova', this.device?.cordova);
    }
    catch (ex) {
      /*span.setStatus({
        code: SpanStatusCode.ERROR,
        message: 'error on load device properties'
      });*/
      this.activeSpan.recordException(ex);
    }

    try {
      const usService = this.usuarioService;
      this.activeSpan.setAttribute('user.name', usService?.nombre);
      this.activeSpan.setAttribute('user.surname', usService?.apellido);
      this.activeSpan.setAttribute('user.nombreUsuario', usService?.nombreUsuario);
      this.activeSpan.setAttribute('user.idUsuario', usService?.idUsuario);
      this.activeSpan.setAttribute('user.rol', usService?.rol);
      this.activeSpan.setAttribute('user.idClub', usService?.idClub);
      this.activeSpan.setAttribute('user.idDisciplina', usService?.idDisciplina);
      this.activeSpan.setAttribute('user.documento', usService?.documento);
    }
    catch (ex) {
      /*span.setStatus({
        code: SpanStatusCode.ERROR,
        message: 'error on load user properties'
      });*/
      this.activeSpan.recordException(ex);
    }

    this.activeSpan.setAttribute('span.name', name);
    return this.activeSpan;
  }

  endActiveSpan(){
    this.activeSpan?.end();
    this.activeSpan = null;
  }

  getActiveSpan(): Span | null {
    return this.activeSpan;
  }
  /*
  Ejemplo de uso:
  const span = createSpan('doSomething')
  try {
      var event1Atributes = {
        attr1: 'test',
        attr2: 2
      };
      span.addEvent('event1', event1Atributes)
      const result = await this.doSomethingElse();
      span.setAttribute('result', result);

      var event2Atributes = {
        attr1: 'test',
        attr2: 2
      };
      span.addEvent('event2', event2Atributes);

      span.end();
    } catch (err) {
      span.setStatus({
        // use an appropriate status code here
        code: 2,
        message: err.message,
      });
      span.recordException(err);
      span.end();
    }

    private async doSomethingElse() {
      return new Promise<number>((result, reject) => {
        setTimeout(() => {
          result(2600);
        }, 1500);
      });
    }
  */

}

export interface Span {
  /**
   * Sets an attribute to the span.
   *
   * Sets a single Attribute with the key and value passed as arguments.
   *
   * @param key the key for this attribute.
   * @param value the value for this attribute. Setting a value null or
   *              undefined is invalid and will result in undefined behavior.
   */
  setAttribute(key: string, value: SpanAttributeValue): this;
  /**
   * Sets attributes to the span.
   *
   * @param attributes the attributes that will be added.
   *                   null or undefined attribute values
   *                   are invalid and will result in undefined behavior.
   */
  setAttributes(attributes: SpanAttributes): this;
  /**
   * Adds an event to the Span.
   *
   * @param name the name of the event.
   * @param [attributesOrStartTime] the attributes that will be added; these are
   *     associated with this event. Can be also a start time
   *     if type is {@type TimeInput} and 3rd param is undefined
   * @param [startTime] start time of the event.
   */
  addEvent(name: string, attributesOrStartTime?: SpanAttributes | TimeInput, startTime?: TimeInput): this;
  /**
   * Sets a status to the span. If used, this will override the default Span
   * status. Default is {@link SpanStatusCode.UNSET}. SetStatus overrides the value
   * of previous calls to SetStatus on the Span.
   *
   * @param status the SpanStatus to set.
   */
  setStatus(status: SpanStatus): this;
  /**
   * Updates the Span name.
   *
   * This will override the name provided via {@link Tracer.startSpan}.
   *
   * Upon this update, any sampling behavior based on Span name will depend on
   * the implementation.
   *
   * @param name the Span name.
   */
  updateName(name: string): this;
  /**
   * Marks the end of Span execution.
   *
   * Call to End of a Span MUST not have any effects on child spans. Those may
   * still be running and can be ended later.
   *
   * Do not return `this`. The Span generally should not be used after it
   * is ended so chaining is not desired in this context.
   *
   * @param [endTime] the time to set as Span's end time. If not provided,
   *     use the current time as the span's end time.
   */
  end(endTime?: TimeInput): void;
  /**
   * Returns the flag whether this span will be recorded.
   *
   * @returns true if this Span is active and recording information like events
   *     with the `AddEvent` operation and attributes using `setAttributes`.
   */
  isRecording(): boolean;
  /**
   * Sets exception as a span event
   * @param exception the exception the only accepted values are string or Error
   * @param [time] the time to set as Span's event time. If not provided,
   *     use the current time.
   */
  recordException(exception: Exception, time?: TimeInput): void;
}

interface ExceptionWithCode {
  code: string | number;
  name?: string;
  message?: string;
  stack?: string;
}
interface ExceptionWithMessage {
  code?: string | number;
  message: string;
  name?: string;
  stack?: string;
}
interface ExceptionWithName {
  code?: string | number;
  message?: string;
  name: string;
  stack?: string;
}
/**
* Defines Exception.
*
* string or an object with one of (message or name or code) and optional stack
*/
export declare type Exception = ExceptionWithCode | ExceptionWithMessage | ExceptionWithName | string;
export { };



export interface SpanAttributes {
  [attributeKey: string]: SpanAttributeValue | undefined;
}
/**
* Attribute values may be any non-nullish primitive value except an object.
*
* null or undefined attribute values are invalid and will result in undefined behavior.
*/
export declare type SpanAttributeValue = string | number | boolean | Array<null | undefined | string> | Array<null | undefined | number> | Array<null | undefined | boolean>;


export interface SpanStatus {
  /** The status code of this message. */
  code: SpanStatusCode;
  /** A developer-facing error message. */
  message?: string;
}
/**
* An enumeration of status codes.
*/
export enum SpanStatusCode {
  /**
   * The default status.
   */
  UNSET = 0,
  /**
   * The operation has been validated by an Application developer or
   * Operator to have completed successfully.
   */
  OK = 1,
  /**
   * The operation contains an error.
   */
  ERROR = 2
}

export declare type HrTime = [number, number];
/**
 * Defines TimeInput.
 *
 * hrtime, epoch milliseconds, performance.now() or Date
 */
export declare type TimeInput = HrTime | number | Date;