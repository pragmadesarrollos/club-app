import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DocumentacionService {
  myAppUrl: string;
  titulo: string;
  descripcion: string;
  destinatarios = [];
  categoria;
  descripcionL: string;

  constructor(private http: HttpClient) {
    this.myAppUrl = environment.endpoint
   }


  obtenerDocumentosxClub(idClub:number): Observable<any> {
    //:club
    return this.http.get(this.myAppUrl + '/api/documento/club/' + idClub)
  }

  obtenerDocumentacionAprobada(isManager) {
    let tokenInfo = JSON.parse(localStorage.getItem('tokenInfo'));
    if(isManager) {
      return this.http.get(`${this.myAppUrl}/api/documentacion/solicitud/usuarios/${tokenInfo.clubId}/${tokenInfo.userId}/3`);
    } else {
      return this.http.get(`${this.myAppUrl}/api/documentacion/estado/${tokenInfo.clubId}/${tokenInfo.userId}/3`);
    }
  }

  obtenerDocumentacionPendiente(isManager) {
    let tokenInfo = JSON.parse(localStorage.getItem('tokenInfo'));
    if(isManager) {
      return this.http.get(`${this.myAppUrl}/api/documentacion-enviadas/estado/${tokenInfo.clubId}/${tokenInfo.userId}/1`);
    } else {
      return this.http.get(`${this.myAppUrl}/api/documentacion/estado/${tokenInfo.clubId}/${tokenInfo.userId}/1`);
    }
  }

  obtenerDocumentacionEnRevision(isManager) {
    let tokenInfo = JSON.parse(localStorage.getItem('tokenInfo'));
    if(isManager) {
      return this.http.get(`${this.myAppUrl}/api/documentacion-enviadas/estado/${tokenInfo.clubId}/${tokenInfo.userId}/2`);
    } else {
      return this.http.get(`${this.myAppUrl}/api/documentacion/estado/${tokenInfo.clubId}/${tokenInfo.userId}/2`);
    }
    
    
  }

  obtenerDocumentosxUsuarioClub(idClub:number, idUsuario:number): Observable<any> {
    //:club/:usuario
    return this.http.get(this.myAppUrl + '/api/documento/club/usuario/' + idClub +'/' + idUsuario)
  }

  obtenerDocumentacionEnviada(){
    let tokenInfo = JSON.parse(localStorage.getItem('tokenInfo'));
    return this.http.get(`${this.myAppUrl}/api/documentacion-enviadas/estado/${tokenInfo.clubId}/${tokenInfo.userId}/null`);
  }

  obtenerDocumentacionEnviadaAprobada(solicitudId){
    let tokenInfo = JSON.parse(localStorage.getItem('tokenInfo'));
    return this.http.get(`${this.myAppUrl}/api/documentacion/solicitud/usuarios/${tokenInfo.clubId}/${solicitudId}`);
  }

  obtenerDocumentacionxId(id, userId) {
    let tokenInfo = JSON.parse(localStorage.getItem('tokenInfo'));
    return this.http.get(`${this.myAppUrl}/api/documentacion/solicitud/${tokenInfo.clubId}/${userId}/${id}`);
  }

  enviarSolicitud(solicitud) {
    return this.http.post(`${this.myAppUrl}/api/documentacion/crear`, solicitud);
  }

  eliminarDocumento(id) {
    return this.http.delete(`${this.myAppUrl}/api/documentacion/eliminar/${id}`);
  }

  obtenerCategorias() {
    let tokenInfo = JSON.parse(localStorage.getItem('tokenInfo'));
    return this.http.get(`${this.myAppUrl}/api/categoria-documento/${tokenInfo.clubId}`);
  }

  aprobarDocumentacion(id, userId) {
    return this.http.put(`${this.myAppUrl}/api/documentacion/aprobado/${userId}/${id}`, {});
  }

  rechazarDocumentacion(id, userId) {
    return this.http.put(`${this.myAppUrl}/api/documentacion/rechazado/${userId}/${id}`, {});
  }

  getAprobadosList(id) {
    let tokenInfo = JSON.parse(localStorage.getItem('tokenInfo'));
    return this.http.get(`${this.myAppUrl}/api/documentacion/solicitud/usuarios/${tokenInfo.clubId}/${id}/3`);
  }

  obtenerUsuariosPorSolicitud(id) {
    let tokenInfo = JSON.parse(localStorage.getItem('tokenInfo'));
    return this.http.get(`${this.myAppUrl}/api/documentacion/solicitud/usuarios/${tokenInfo.clubId}/${id}`);
  }

}
