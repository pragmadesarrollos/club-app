import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ConfiguracionDiasHs, Espacio } from '../models/ingresos.model';

@Injectable({
  providedIn: 'root'
})
export class EspacioService {

  constructor(private http: HttpClient) { }

  getEspacios(): Observable<Espacio[]> {
    return this.http.get<Espacio[]>(`${environment.endpoint}/api/espacio`);
  }

  getEspacio(id): Observable<Espacio> {
    return this.http.get<Espacio>(`${environment.endpoint}/api/espacio/${id}`);
  }

  getEspaciosPorDisciplina(id): Observable<Espacio> {
    return this.http.get<Espacio>(`${environment.endpoint}/api/espacio/disciplina/${id}`);
  }

  getConfiguracionPorEspacio(id): Observable<ConfiguracionDiasHs[]> {
    return this.http.get<ConfiguracionDiasHs[]>(`${environment.endpoint}/api/configuracion/${id}`);
    /*return of<ConfiguracionDiasHs[]>([
      {
        desde: '09:00',
        hasta: '18:00',
        lunes: 1,
        martes: 1,
        miercoles: 1,
        jueves: 1,
        viernes: 1,
        sabado: 0,
        domingo: 0,
        espacioId: 1,
        id: 185
      }
    ]);*/
  }

  getEspaciosDelClub(clubId) {
    return this.http.get<Espacio[]>(`${environment.endpoint}/api/espacio/club/${clubId}`);
  }

}
