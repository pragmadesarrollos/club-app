import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { BeneficioDetalle, BeneficiosPorClub } from '../models/beneficio.model';

@Injectable({
  providedIn: 'root',
})
export class BeneficioService {
  constructor(private http: HttpClient) {}

  obtenerBeneficios(page?): Observable<any> {
    return this.http.get(`${environment.endpoint}/api/beneficios/${page || 1}`);
  }

  obtenerBeneficiosxId(id): Observable<BeneficioDetalle> {
    return this.http.get<BeneficioDetalle>(`${environment.endpoint}/api/beneficios/${id}`);
  }

  obtenerBeneficiosxClub(): Observable<BeneficiosPorClub[]> {
    let id = JSON.parse(localStorage.getItem('tokenInfo')).clubId;
    return this.http.get<BeneficiosPorClub[]>(`${environment.endpoint}/api/beneficios/mobile/club/${id}`);
  }
}
