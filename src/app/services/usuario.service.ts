import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  myAppUrl = environment.endpoint;

  // Almacenamos el nombre del usuario NO registrado
  nombreUsuario: string;
  nombre: string;
  apellido: string;
  documento: string;
  password: string;
  idClub: number;
  idDisciplina: number;
  idUsuario: number;
  rol: string;
  globalTokenFirebase: string;

  profilePhoto: BehaviorSubject<number> = new BehaviorSubject(1)

  constructor(private http: HttpClient) {
    this.cagarInformacionDelUsuarioLogueado();
  }

  updatePhoto(n) {
    this.profilePhoto.next(n)
  }

  bindProfilePhoto() {
    return this.profilePhoto.asObservable();
  }

  registrarFirebase(usuario: any): Observable<any> {
    return this.http.post(this.myAppUrl + '/api/registrar-firebase', usuario);
  }

  validarUsuario(mail) {
    return this.http.get(this.myAppUrl + '/api/validar-usuario/' + mail)
  }

  guardarToken(token: any): Observable<any> {
    if (token == null) {
      token = this.globalTokenFirebase;
    }
    return this.http.post(this.myAppUrl + '/api/actualizar-token-firebase', {
      firebaseToken: token,
      userId: this.idUsuario
    });
  }

  recuperarPassword(usuario: any): Observable<any> {
    return this.http.post(this.myAppUrl + '/api/reset-password', usuario)
  }

  signup(usuario: any): Observable<any> {
    return this.http.post(this.myAppUrl + '/api/signup', usuario);
  }

  obtenerUsuario(id): Observable<any> {
    return this.http.get(this.myAppUrl + '/api/usuario/' + id)
  }

  nuevaClave(camposClave): Observable<any> {
    let header = new HttpHeaders().set("Authorization", "Bearer " + localStorage.getItem('token'));


    return this.http.post(this.myAppUrl + '/api/nueva-clave', camposClave, { headers: header });
  }

  modificarUsuario(idClub, idUsuario, data) {
    let httpOptions = {
      headers: new HttpHeaders({ 'Accept': 'application/json;' })
    };
    return this.http.put(this.myAppUrl + '/api/usuario/' + idClub + '/' + idUsuario, { data: JSON.stringify(data) }, httpOptions);
  }

  agregarClub(idUsuario, idClub): Observable<any> {
    return this.http.post(this.myAppUrl + '/api/agregar-club/' + idUsuario + '/' + idClub, '');
  }

  cagarInformacionDelUsuarioLogueado() {
    try {
      const info = localStorage.getItem('tokenInfo');
      if (!info) return;
      const jinfo = JSON.parse(info);
      if (!info) return;
      this.idUsuario = jinfo.userId;
      this.rol = jinfo.rol;
      this.idClub = jinfo.clubId;
    } catch (err) {
      console.error(err);
    }

  }

  uploadFile(userId, formData) {
    return this.http.put(this.myAppUrl + '/api/personas/update/image/' + userId, formData);
  }

  resetPassword(email) {
    return this.http.post(this.myAppUrl + '/api/reset-password', { email: email });
  }


  cerrarSession(): Observable<any> {
    return this.http.post(this.myAppUrl + '/api/signout', null);
  }

}
