import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, LoadingController, ToastController } from '@ionic/angular';
import { EncuestaService } from 'src/app/services/encuesta.service';
import { NotificacionService } from 'src/app/services/notificacion.service';

@Component({
  selector: 'app-enviar',
  templateUrl: './enviar.page.html',
  styleUrls: ['./enviar.page.scss'],
})
export class EnviarPage implements OnInit {
  titulo = 'Enviar'
  loading: HTMLIonLoadingElement;
  tituloEncuesta;
  descripcion;
  descripcionL;
  pregunta;
  opcion;
  destinatarios = [];

  constructor(private router: Router,
    public loadingController: LoadingController,
    public toastController: ToastController,
    public notificacionService: NotificacionService,
    public alertController: AlertController) {

    this.titulo = this.notificacionService.titulo;
    this.descripcion = this.notificacionService.descripcion;
    this.descripcionL = this.notificacionService.descripcionL;
    //this.pregunta = this._encuestaService.pregunta;
    //this.opcion = this._encuestaService.opcion;
    this.destinatarios = this.notificacionService.destinatarios;
  }

  ngOnInit() {


  }


  siguiente() {
    this.presentLoading().then((loadRes: any) => {
      this.loading = loadRes
      this.loading.present()

      this.notificacionService.enviarNotificacion({
        titulo: this.titulo,
        descripcion: this.descripcionL,
        descripcion_corta: this.descripcion
      }, this.destinatarios).subscribe(async data => {
        console.log(data);
        this.dismissLoading();
        this.router.navigate(['/menu']);

        const alert = await this.alertController.create({
          header: 'Notificacion',
          message: 'Enviada correctamente.',
          buttons: ['OK']
        });
        await alert.present();

      }, error => {
        console.log(error);
        this.dismissLoading();
        this.mensaje('Opss.. ocurrio un error');
      })
    });
  }



  presentLoading() {
    if (this.loading) {
      this.loading.dismiss();
    }
    return new Promise((resolve) => {
      resolve(this.loadingController.create({
        message: 'Espere...'
      }));
    })
  }

  async dismissLoading(): Promise<void> {
    if (this.loading) {
      this.loading.dismiss();
    }
  }

  async mensaje(mensaje: string) {
    const toast = await this.toastController.create({
      message: mensaje,
      color: 'dark',
      duration: 3000
    });
    toast.present();
  }


}
