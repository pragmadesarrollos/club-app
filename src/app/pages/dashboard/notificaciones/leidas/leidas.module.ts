import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LeidasPageRoutingModule } from './leidas-routing.module';

import { LeidasPage } from './leidas.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LeidasPageRoutingModule
  ],
  declarations: [LeidasPage]
})
export class LeidasPageModule {}
