import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotificacionesPage } from '../notificaciones.page';

import { LeidasPage } from './leidas.page';

const routes: Routes = [
  {
    path: '',
    component: LeidasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LeidasPageRoutingModule {}
