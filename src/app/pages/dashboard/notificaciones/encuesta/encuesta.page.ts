import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, LoadingController, ToastController } from '@ionic/angular';
import { EncuestaService } from 'src/app/services/encuesta.service';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-encuesta',
  templateUrl: './encuesta.page.html',
  styleUrls: ['./encuesta.page.scss'],
})
export class EncuestaPage implements OnInit {
  encuesta;
  preguntas;
  editable = false;
  loading: HTMLIonLoadingElement;
  respuestas = {};
  respuestasDelUsuario = {};
  isManager;

  constructor(private encuestasService: EncuestaService,
    private localService: StorageService,
    private router: Router,
    private route: ActivatedRoute,
    public loadingController: LoadingController,
    public toastController: ToastController,
    public alertController: AlertController) {
  }

  ngOnInit() {
    console.log(`EncuestaPage editable: ${this.route.snapshot.queryParams.editable}, id: ${this.route?.snapshot?.params?.id}`);

    let id = this.route?.snapshot?.params?.id;
    this.editable = (this.route.snapshot.queryParams.editable == "true" || this.route.snapshot.queryParams.editable == true);
    if (id?.indexOf('?') !== -1) {
      this.editable = id.split('?')[1]?.replace("editable=", "") == "true";
      id = id.split('?')[0];
    }

    console.log(`EncuestaPage editable: ${this.editable}, id: ${id}`);

    let userInfo = this.localService.localGet('userInfo');
    this.isManager = userInfo.rol == 'manager';

    if (!id) {
      this.router.navigate(['/menu/notificaciones']);
      return;
    }

    this.encuestasService.obtenerEncustaPorId(id).subscribe(data => {
      console.log('encuesta', data);
      this.encuesta = data;
    }, error => {
      //this.dismissLoading();
      console.error(error);
      //this.mensaje('Opss.. ocurrio un error');
    });

    this.encuestasService.obtenerPreguntas(id).subscribe(data => {
      console.log('preguntas', data);
      this.preguntas = data;
      this.obtenerOpcionesDesdePreguntas(data);
    }, error => {
      //this.dismissLoading();
      console.error(error);
      //this.mensaje('Opss.. ocurrio un error');
    });

    this.encuestasService.obtenerRespuestasPorUsuario(id).subscribe(data => {
      console.log('respuestas del usuario', data);
      data.forEach(p => {
        this.respuestasDelUsuario[p.preguntaId] = p.id;

      });
    }, error => {
      //this.dismissLoading();
      console.error(error);
      //this.mensaje('Opss.. ocurrio un error');
    });
  }

  obtenerOpcionesDesdePreguntas(preguntas) {
    for (const key in preguntas) {
      if (Object.prototype.hasOwnProperty.call(preguntas, key)) {
        const element = preguntas[key];
        this.obtenerOpciones(element.id);
      }
    }
  }

  async obtenerOpciones(idPregunta) {
    const opciones = await this.encuestasService.obtenerRespuestas(idPregunta).toPromise();
    const pregunta = this.preguntas.find(p => p.id == idPregunta);
    pregunta.opciones = opciones;
  }

  selectResponse(preguntaId, respuestaId) {
    this.respuestas[preguntaId] = respuestaId;
  }

  siguiente() {
    this.presentLoading().then((loadRes: any) => {
      var obj = { respuesta: [] };
      for (var key in this.respuestas) {
        const idResp = this.respuestas[key];
        obj.respuesta.push({ respuesta: idResp });
      }

      if (obj.respuesta.length == 0) {
        this.mensaje('Debe seleccionar al menos una respuesta');
        return;
      }

      this.loading = loadRes;
      this.loading.present();

      this.encuestasService.responderEncuesta(obj).subscribe(async data => {
        console.log('respuesta de encuensta ok', data);
        this.dismissLoading();
        this.router.navigate(['/menu']);
        const alert = await this.alertController.create({
          header: '¡Muchas gracias por participar!',
          message: 'El club recibió correctamente tu respuesta',
          buttons: ['OK']
        });
        await alert.present();
      }, error => {
        console.error(error);
        this.dismissLoading();
        this.mensaje('Opss.. ocurrio un error');
      });

    });

  }

  presentLoading() {
    if (this.loading) {
      this.loading.dismiss();
    }
    return new Promise((resolve) => {
      resolve(this.loadingController.create({
        message: 'Espere...'
      }));
    })
  }

  async dismissLoading(): Promise<void> {
    if (this.loading) {
      this.loading.dismiss();
    }
  }

  async mensaje(mensaje: string) {
    const toast = await this.toastController.create({
      message: mensaje,
      color: 'dark',
      duration: 3000
    });
    toast.present();
  }
}
