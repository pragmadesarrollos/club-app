import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PreguntasPageRoutingModule } from './preguntas-routing.module';

import { PreguntasPage } from './preguntas.page';
import { HeaderComponent } from 'src/app/components/header/header.component';
import { ReactiveFormsModule } from '@angular/forms';
import { PreguntaPage } from './pregunta/pregunta.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PreguntasPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [
    PreguntasPage,
    HeaderComponent,
    PreguntaPage
  ]
})
export class PreguntasPageModule { }
