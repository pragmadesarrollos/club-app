import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertController, LoadingController, ToastController } from '@ionic/angular';
import { EncuestaService } from 'src/app/services/encuesta.service';
import { List } from 'linqts';

interface Pregunta {
  id: number;
  titulo: string;
  opciones: any[];
  expanded: boolean;
}

interface Opcion {
  id: number;
  titulo: string;
}

@Component({
  selector: 'app-pregunta',
  templateUrl: './pregunta.page.html',
  styleUrls: ['./pregunta.page.scss'],
})
export class PreguntaPage implements OnInit {
  
  @Output() addOption = new EventEmitter<string>();

  @Input() pregunta: Pregunta;
  @Input() i: any;
  expanded = true;
  
  constructor(private router: Router, private fb: FormBuilder,
    public loadingController: LoadingController,
    public toastController: ToastController,
    public alertCtrl: AlertController,
    public encuestaService: EncuestaService) {

   

  }

  ngOnInit() {
    
  }

  agregarOpcion(id) {
      this.addOption.emit(id);
  }

  
 

}
