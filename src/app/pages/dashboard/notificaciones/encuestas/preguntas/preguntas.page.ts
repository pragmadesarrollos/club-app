import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertController, LoadingController, ToastController } from '@ionic/angular';
import { EncuestaService } from 'src/app/services/encuesta.service';
import { List } from 'linqts';

interface Pregunta {
  id: number;
  titulo: string;
  opciones: any[];
  expanded: boolean;
}

interface Opcion {
  id: number;
  titulo: string;
}

@Component({
  selector: 'app-preguntas',
  templateUrl: './preguntas.page.html',
  styleUrls: ['./preguntas.page.scss'],
})
export class PreguntasPage implements OnInit {
  titulo = 'Preguntas'
  form: FormGroup;
  loading: HTMLIonLoadingElement;
  idPregunta;
  preguntas: Pregunta[] = [];

  opciones: Opcion[] = [
    {
      id: 1,
      titulo: 'Opción 1',
    },
    {
      id: 2,
      titulo: 'Opción 2',
    },
    {
      id: 3,
      titulo: 'Opción 3',
    }
  ];

  preguntasAux = [
    {
      activo: 1,
      encuestaId: 1395,
      id: 965,
      opciones: [
        {
          activo: 1,
          contadorDeRespuestas: 0,
          id: 625,
          preguntaId: 965,
          titulo: "gfdgdf"
        },
        {
          activo: 1,
          contadorDeRespuestas: 0,
          id: 625,
          preguntaId: 965,
          titulo: "gfdgdf"
        },
        {
          activo: 1,
          contadorDeRespuestas: 0,
          id: 625,
          preguntaId: 965,
          titulo: "gfdgdf"
        }
      ],
      titulo: "Holaaaaa"
    },
    {
      activo: 1,
      encuestaId: 1395,
      id: 965,
      opciones: [
        {
          activo: 1,
          contadorDeRespuestas: 0,
          id: 625,
          preguntaId: 965,
          titulo: "gfdgdf"
        },
        {
          activo: 1,
          contadorDeRespuestas: 0,
          id: 625,
          preguntaId: 965,
          titulo: "gfdgdf"
        },
        {
          activo: 1,
          contadorDeRespuestas: 0,
          id: 625,
          preguntaId: 965,
          titulo: "gfdgdf"
        }
      ],
      titulo: "Holaaaaa"
    },
    {
      activo: 1,
      encuestaId: 1395,
      id: 965,
      opciones: [
        {
          activo: 1,
          contadorDeRespuestas: 0,
          id: 625,
          preguntaId: 965,
          titulo: "gfdgdf"
        },
        {
          activo: 1,
          contadorDeRespuestas: 0,
          id: 625,
          preguntaId: 965,
          titulo: "gfdgdf"
        },
        {
          activo: 1,
          contadorDeRespuestas: 0,
          id: 625,
          preguntaId: 965,
          titulo: "gfdgdf"
        }
      ],
      titulo: "Holaaaaa"
    },
    {
      activo: 1,
      encuestaId: 1395,
      id: 965,
      opciones: [
        {
          activo: 1,
          contadorDeRespuestas: 0,
          id: 625,
          preguntaId: 965,
          titulo: "gfdgdf"
        },
        {
          activo: 1,
          contadorDeRespuestas: 0,
          id: 625,
          preguntaId: 965,
          titulo: "gfdgdf"
        },
        {
          activo: 1,
          contadorDeRespuestas: 0,
          id: 625,
          preguntaId: 965,
          titulo: "gfdgdf"
        }
      ],
      titulo: "Holaaaaa"
    }
  ]

  constructor(private router: Router, private fb: FormBuilder,
    public loadingController: LoadingController,
    public toastController: ToastController,
    public alertCtrl: AlertController,
    public encuestaService: EncuestaService) {

    this.form = this.fb.group({
      pregunta: ['', Validators.required],
      opcion: ['', Validators.required],
    })

  }

  ngOnInit() {
    //this.obtenerPreguntas()
  }

  obtenerPreguntas() {
    this.presentLoading().then((loadRes: any) => {
      this.loading = loadRes
      this.loading.present();
      let idEncuesta = this.encuestaService.idEncuesta;

      this.encuestaService.obtenerPreguntas(idEncuesta).subscribe(data => {
        this.preguntas = data;
        console.warn(data);
        this.obtenerOpcionesDesdePreguntas(data);
        this.dismissLoading();
      }, error => {
        console.log(error);
        this.dismissLoading();
        this.mensaje('Opss.. ocurrio un error');
      })
    })

  }

  onChange(e) {
    console.log(e.target.value)
  }

  async agregarPregunta() {
    const alert = await this.alertCtrl.create({
      header: 'Nueva Pregunta',
      inputs: [
        {
          name: 'titulo',
          type: 'text',
          placeholder: 'Pregunta'
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: (data) => {
            console.log(data)
            this.guardarPregunta(data.titulo);
            console.log('Confirm Ok');
          }
        }
      ]
    });

    await alert.present();
  }

  async agregarOpcion(idPregunta) {
    this.idPregunta = idPregunta;
    const alert = await this.alertCtrl.create({
      header: 'Nueva Opción',
      inputs: [
        {
          name: 'titulo',
          type: 'text',
          placeholder: 'Opción'
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: (data) => {
            console.log(data)
            this.guardarRespuesta(data.titulo);
            console.log('Confirm Ok');
          }
        }
      ]
    });

    await alert.present();

  }

  guardarPregunta(pregunta) {
    let titulo = pregunta;
    this.presentLoading().then((loadRes: any) => {
      this.loading = loadRes
      this.loading.present();
      let idEncuesta = this.encuestaService.idEncuesta;

      const pregunta: any = {
        titulo: titulo
      }

      this.encuestaService.crearPregunta(idEncuesta, pregunta).subscribe(data => {
        console.log(data);
        this.obtenerPreguntas();
        this.dismissLoading();

      }, error => {
        console.log(error);
        this.dismissLoading();
        this.mensaje('Opss.. ocurrio un error');
      })
    })

  }


  guardarRespuesta(respuesta) {
    let titulo = respuesta;
    this.presentLoading().then((loadRes: any) => {
      this.loading = loadRes
      this.loading.present();


      const respuesta: any = {
        titulo: titulo,
      }

      this.encuestaService.crearRespuesta(this.idPregunta, respuesta).subscribe(data => {
        console.log(data);
        this.obtenerPreguntas();
        this.dismissLoading();

      }, error => {
        console.log(error);
        this.dismissLoading();
        this.mensaje('Opss.. ocurrio un error');
      })
    })

  }

  obtenerOpcionesDesdePreguntas(preguntas) {
    for (const key in preguntas) {
      if (Object.prototype.hasOwnProperty.call(preguntas, key)) {
        const element = preguntas[key];
        this.obtenerOpciones(element.id);
      }
    }
  }

  async obtenerOpciones(idPregunta) {
    const opciones = await this.encuestaService.obtenerRespuestas(idPregunta).toPromise();
    const pregunta = this.preguntas.find(p => p.id == idPregunta);
    pregunta.opciones = opciones;
  }

  async siguiente() {
    let message = null;
    if (!this.preguntas || this.preguntas.length == 0) {
      message = 'Debes ingresar como mínimo una pregunta para poder enviar una encuesta';
    }
    if (new List(this.preguntas).Any(p => !p.opciones || p.opciones.length == 0)) {
      message = 'Todas las preguntas deben tener al menos una opción';
    }
    if (message) {
      const alert = await this.alertCtrl.create({
        header: 'Encuesta incompleta',
        message: message,
        buttons: ['OK']
      });
      await alert.present();
      return;
    }

    this.encuestaService.preguntas = this.preguntas;


    this.router.navigate(['/menu/notificaciones/destinatarios']);
  }



  presentLoading() {
    if (this.loading) {
      this.loading.dismiss();
    }
    return new Promise((resolve) => {
      resolve(this.loadingController.create({
        message: 'Espere...'
      }));
    })
  }

  async dismissLoading(): Promise<void> {
    if (this.loading) {
      this.loading.dismiss();
    }
  }

  async mensaje(mensaje: string) {
    const toast = await this.toastController.create({
      message: mensaje,
      color: 'dark',
      duration: 3000
    });
    toast.present();
  }


}
