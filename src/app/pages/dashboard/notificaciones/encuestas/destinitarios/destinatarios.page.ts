import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoadingController, ToastController } from '@ionic/angular';
import { EncuestaService } from 'src/app/services/encuesta.service';
import { Usuario } from 'src/app/class/usuario.model';
import { Club } from 'src/app/class/club.model';
import { Disciplina } from 'src/app/class/disciplina.model';
import { Destinatario } from 'src/app/class/disciplina.model copy';
import { UsuarioService } from 'src/app/services/usuario.service';
import { List } from 'linqts';
import { ClubService } from 'src/app/services/club.service';


@Component({
  selector: 'app-destinatarios',
  templateUrl: './destinatarios.page.html',
  styleUrls: ['./destinatarios.page.scss'],
})
export class DestinatariosPage implements OnInit {
  titulo = 'Destinatarios'
  loading: HTMLIonLoadingElement;
  listDisciplina;
  listDivisiones;
  listPosiciones;
  listUsuarios;

  disciplinaSeleccionada;
  divisionSeleccionada;
  posicionSeleccionada;
  usuarioSeleccionado;


  mostrarDivisiones = true;
  mostrarPosiciones = true;
  mostrarUsuarios = true;


  constructor(private router: Router, private fb: FormBuilder,
    public loadingController: LoadingController,
    public toastController: ToastController,
    public usuarioService: UsuarioService,
    private clubService: ClubService,
    public encuestaService: EncuestaService) {
  }

  ngOnInit() {

    // Obtenemos idClub
    //const idClub = this.form.value.club;
    const idClub = this.usuarioService.idClub;
    // Reseteamos array de disciplinas y el valor actual de disciplina
    this.listDisciplina = [];


    // Spinner.. 
    this.presentLoading().then((loadRes: any) => {
      this.loading = loadRes;
      this.loading.present();

      this.clubService.obtenerDisciplinasByIdClub(idClub).subscribe((data: []) => {
        const listD = new List<any>(data);
        listD.Insert(0, {
          id: -1,
          activo: 1,
          clubId: idClub,
          disciplinaId: -1,
          disciplina: {
            id: -1,
            activo: 1,
            descripcion: "",
            icono: "none",
            nombre: "Todas",
          }
        });
        const arr = listD.ToArray();
        console.log("disciplinasArr", arr);
        this.listDisciplina = arr;
        this.dismissLoading();
      }, error => {
        console.log(error);
        this.dismissLoading();
        this.mensaje('Opss.. ocurrio un error');
      })
    })
  }

  cambioDisciplina(e) {
    this.listDivisiones = [];
    const idClub = this.usuarioService.idClub;
    this.disciplinaSeleccionada = e.target.value;

    if (!this.disciplinaSeleccionada) return;

    console.log('disciplinaSeleccionada', this.disciplinaSeleccionada);
    if (this.disciplinaSeleccionada.id === -1) {
      this.mostrarDivisiones = false;
      this.mostrarPosiciones = false;
      this.mostrarUsuarios = false;
      return;
    } else {
      this.mostrarDivisiones = true;
      this.mostrarPosiciones = true;
      this.mostrarUsuarios = true;
    }
    this.divisionSeleccionada = null;
    this.posicionSeleccionada = null;
    this.usuarioSeleccionado = null;

    // Spinner.. 
    this.presentLoading().then((loadRes: any) => {
      this.loading = loadRes;
      this.loading.present();
      this.clubService.obtenerDivisionesxDisciplina(idClub, this.disciplinaSeleccionada.disciplina.id).subscribe(data => {
        const listD = new List<any>(data);
        listD.Insert(0, {
          id: -1,
          activo: 1,
          nombre: "Todas",
        });
        const arr = listD.ToArray();
        console.log("divisionesArr", arr);
        this.listDivisiones = arr;
        this.dismissLoading();
      }, error => {
        console.log(error);
        this.dismissLoading();
        this.mensaje('Opss.. ocurrio un error');
      });
    })
  }

  cambioDivision(e) {
    this.listPosiciones = [];

    this.divisionSeleccionada = e.target.value;
    if (!this.divisionSeleccionada || this.divisionSeleccionada == '') return;

    console.log('divisionSeleccionada', this.divisionSeleccionada);

    if (this.divisionSeleccionada.id === -1) {
      this.mostrarPosiciones = false;
      this.mostrarUsuarios = false;
      return;
    } else {
      this.mostrarPosiciones = true;
      this.mostrarUsuarios = true;
    }
    this.posicionSeleccionada = null;
    this.usuarioSeleccionado = null;
    // Spinner.. 
    this.presentLoading().then((loadRes: any) => {
      this.loading = loadRes;
      this.loading.present();
      this.clubService.obtenerPosicionesxDisciplina(this.disciplinaSeleccionada.disciplina.id).subscribe(data => {
        const listD = new List<any>(data);
        listD.Insert(0, {
          id: -1,
          activo: 1,
          nombre: "Todas",
        });
        const arr = listD.ToArray();
        console.log("posicionesArr", arr);
        this.listPosiciones = arr;
        this.dismissLoading();
      }, error => {
        console.log(error);
        this.dismissLoading();
        this.mensaje('Opss.. ocurrio un error');
      });
    })
  }

  cambioPosicion(e) {
    this.listUsuarios = [];

    const idClub = this.usuarioService.idClub;
    this.posicionSeleccionada = e.target.value;
    if (!this.posicionSeleccionada || this.posicionSeleccionada == '') return;

    console.log('posicionSeleccionada', this.posicionSeleccionada);

    if (this.posicionSeleccionada.id === -1) {
      this.mostrarUsuarios = false;
      return;
    } else {
      this.mostrarUsuarios = true;
    }

    this.usuarioSeleccionado = null;
    // Spinner.. 
    this.presentLoading().then((loadRes: any) => {
      this.loading = loadRes;
      this.loading.present();
      this.clubService.obtenerUsuariosFiltrosPorClubPorDisiplinaPorDivisionPorPosicion(idClub, this.disciplinaSeleccionada.disciplina.id, this.divisionSeleccionada.id, this.posicionSeleccionada.id).subscribe(data => {
        const listD = new List<any>(data);
        listD.Insert(0, {
          id: -1,
          activo: 1,
          nombre: "Todos",
        });
        const arr = listD.ToArray();
        console.log("usuariosArr", arr);
        this.listUsuarios = arr;
        this.dismissLoading();
      }, error => {
        console.log(error);
        this.dismissLoading();
        this.mensaje('Opss.. ocurrio un error');
      });
    });
  }

  async siguiente() {

    const idClub = this.usuarioService.idClub;
    let usuarios = [];
    if (this.disciplinaSeleccionada?.disciplina?.id === -1) {
      usuarios = await this.clubService.obtenerUsuariosFiltrosPorClub(idClub).toPromise();
    } else if (this.divisionSeleccionada?.id === -1) {
      usuarios = await this.clubService.obtenerUsuariosFiltrosPorClubPorDisciplina(idClub, this.disciplinaSeleccionada.disciplina.id).toPromise();
    } else if (this.posicionSeleccionada?.id === -1) {
      usuarios = await this.clubService.obtenerUsuariosFiltrosPorClubPorDisciplinaPorDivision(idClub, this.disciplinaSeleccionada.disciplina.id, this.divisionSeleccionada.id).toPromise();
    }

    console.log('usuarios', usuarios);

    this.encuestaService.destinatarios = usuarios;

    this.router.navigate(['/menu/notificaciones/enviar'])

  }


  presentLoading() {
    if (this.loading) {
      this.loading.dismiss();
    }
    return new Promise((resolve) => {
      resolve(this.loadingController.create({
        message: 'Espere...'
      }));
    })
  }

  async dismissLoading(): Promise<void> {
    if (this.loading) {
      this.loading.dismiss();
    }
  }

  async mensaje(mensaje: string) {
    const toast = await this.toastController.create({
      message: mensaje,
      color: 'dark',
      duration: 3000
    });
    toast.present();
  }

  formularioValido() {
    if (this.disciplinaSeleccionada && this.disciplinaSeleccionada.disciplina && this.disciplinaSeleccionada.disciplina.id === -1) {
      return true;
    }
    if (this.disciplinaSeleccionada && this.disciplinaSeleccionada.disciplina && this.disciplinaSeleccionada.disciplina.id !== -1 &&
      this.divisionSeleccionada && this.divisionSeleccionada.id === -1) {
      return true;
    }
    if (this.disciplinaSeleccionada && this.disciplinaSeleccionada.disciplina && this.disciplinaSeleccionada.disciplina.id !== -1 &&
      this.divisionSeleccionada && this.divisionSeleccionada.id !== -1 &&
      this.posicionSeleccionada && this.posicionSeleccionada.id === -1) {
      return true;
    }
    if (this.disciplinaSeleccionada && this.disciplinaSeleccionada.disciplina && this.disciplinaSeleccionada.disciplina.id !== -1 &&
      this.divisionSeleccionada && this.divisionSeleccionada.id !== -1 &&
      this.posicionSeleccionada && this.posicionSeleccionada.id !== -1 &&
      this.usuarioSeleccionado && this.usuarioSeleccionado.id === -1) {
      return true;
    }
    return false;
  }

}
