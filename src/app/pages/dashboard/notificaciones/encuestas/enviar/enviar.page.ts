import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, LoadingController, ToastController } from '@ionic/angular';
import { EncuestaService } from 'src/app/services/encuesta.service';

@Component({
  selector: 'app-enviar',
  templateUrl: './enviar.page.html',
  styleUrls: ['./enviar.page.scss'],
})
export class EnviarPage implements OnInit {
  titulo = 'Revisá y enviá'
  loading: HTMLIonLoadingElement;
  tituloEncuesta;
  descripcion;
  preguntas;
  destinatarios = [];

  constructor(private router: Router,
    public loadingController: LoadingController,
    public toastController: ToastController,
    public alertController: AlertController,
    public encuestaService: EncuestaService) {
    this.descripcion = this.encuestaService.descripcion;
    this.preguntas = this.encuestaService.preguntas;
    this.destinatarios = this.encuestaService.destinatarios;
  }

  ngOnInit() {

  }


  siguiente() {
    this.presentLoading().then((loadRes: any) => {
      this.loading = loadRes;
      this.loading.present();

      this.encuestaService.enviarEncuesta(this.encuestaService.idEncuesta, this.destinatarios).subscribe(async data => {
        console.log(data);
        this.dismissLoading();
        this.router.navigate(['/menu']);

        const alert = await this.alertController.create({
          header: '¡Excelente!',
          message: 'Tu encuesta ha sido enviada correctamente',
          buttons: ['OK']
        });
        await alert.present();
      }, error => {
        console.log(error);
        this.dismissLoading();
        this.mensaje('Opss.. ocurrio un error');
      })
    });
  }



  presentLoading() {
    if (this.loading) {
      this.loading.dismiss();
    }
    return new Promise((resolve) => {
      resolve(this.loadingController.create({
        message: 'Espere...'
      }));
    })
  }

  async dismissLoading(): Promise<void> {
    if (this.loading) {
      this.loading.dismiss();
    }
  }

  async mensaje(mensaje: string) {
    const toast = await this.toastController.create({
      message: mensaje,
      color: 'dark',
      duration: 3000
    });
    toast.present();
  }


}
