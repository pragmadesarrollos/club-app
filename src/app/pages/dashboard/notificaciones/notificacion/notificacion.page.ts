import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificacionService } from 'src/app/services/notificacion.service';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-notificacion',
  templateUrl: './notificacion.page.html',
  styleUrls: ['./notificacion.page.scss'],
})
export class NotificacionPage implements OnInit {
  notificacion;
  viewers;
  isManager = false;

  constructor(private localService: StorageService, private _notificacionService: NotificacionService, private router: Router, private route: ActivatedRoute,) {
   
   }

  ngOnInit() {
    console.log(`NotificacionPage id: ${this.route?.snapshot?.params?.id}`);
    let userInfo = this.localService.localGet('userInfo');
    this.isManager = userInfo.rol == 'manager';

    this._notificacionService.obtenerNotificacionesPorId(this.route.snapshot.params.id).subscribe(data => {
      console.log(data);
      this.setViewers(data.id);
      this.notificacion = data;
    }, error => {
      //this.dismissLoading();
      console.log(error);
      //this.mensaje('Opss.. ocurrio un error');
    })
  }

  setViewers(id) {
    this._notificacionService.obtenerViewers(id).subscribe(d => {
      
      this.viewers = d;
      console.warn(this.viewers);
    })
  }



}
