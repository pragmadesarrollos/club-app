import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, LoadingController } from '@ionic/angular';
import * as moment from 'moment';
import { EncuestaService } from 'src/app/services/encuesta.service';
import { NotificacionService } from 'src/app/services/notificacion.service';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-recibidas',
  templateUrl: './recibidas.page.html',
  styleUrls: ['./recibidas.page.scss'],
})
export class RecibidasPage implements OnInit {
  notificacionesRecibidas;
  encuestasRecibidas;
  isManager;

  constructor(private localService: StorageService, private alertController: AlertController, private notificacionService: NotificacionService, 
    private encuestasService: EncuestaService,
    private router: Router, public loadingController: LoadingController) {
      let userInfo = this.localService.localGet('userInfo');
      this.isManager = userInfo.rol == 'manager';
  
     }

  ngOnInit() {
    console.warn('entro a recibidassssssss')
    this.presentLoading();
    this.notificacionService.obtenerMisNotificacionesNoLeidas().subscribe(data => {
      console.error(data)
      this.notificacionesRecibidas = data;
      this.loadingController.dismiss();
    }, error => {
      //this.dismissLoading();
      this.loadingController.dismiss();
      //this.mensaje('Opss.. ocurrio un error');
    });
    this.encuestasService.obtenerMisEncuestasRecibidas().subscribe(data => {
      this.encuestasRecibidas = data;
      console.warn(data)
    }, error => {
      //this.dismissLoading();
      console.log(error);
      //this.mensaje('Opss.. ocurrio un error');
    });
  }

  verNotificacion(id) {
    this.router.navigate(['menu/notificaciones/notificacion/', id])
  }

  verEncuesta(enc) {
    this.router.navigate(['menu/notificaciones/encuesta/', enc.id], {
      queryParams: {
        editable: true
      }
    })
    
  }

  async showError(msg) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Error',
      message: msg,
      buttons: [
        {
          text: 'Ok',
          role: 'cancel',
          cssClass: 'secondary',
          
        }
      ]
    });

    await alert.present();
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'Cargando notificaciones...',
      duration: 10000
    });
    await loading.present();
  }

  getFecha(f) {
    return moment(f).locale('es').format('dddd DD [de] MMMM')
  }

}
