import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NotificacionesPageRoutingModule } from './notificaciones-routing.module';

import { NotificacionesPage } from './notificaciones.page';
import { HeaderComponent } from 'src/app/components/header/header.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RecibidasPageModule } from './recibidas/recibidas.module';
import { LeidasPageModule } from './leidas/leidas.module';
import { EnviadasPageModule } from './enviadas/enviadas.module';
import { RecibidasPage } from './recibidas/recibidas.page';
import { LeidasPage } from './leidas/leidas.page';
import { EnviadasPage } from './enviadas/enviadas.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NotificacionesPageRoutingModule,
    ReactiveFormsModule,
    RecibidasPageModule,
    LeidasPageModule,
    EnviadasPageModule
  ],
  declarations: [
    NotificacionesPage,
    HeaderComponent,
    RecibidasPage,
    LeidasPage,
    EnviadasPage
  ]
})
export class NotificacionesPageModule { }
