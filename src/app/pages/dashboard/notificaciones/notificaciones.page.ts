import { Component, OnInit } from '@angular/core';

import { FCM } from '@capacitor-community/fcm';
import { PushNotifications } from '@capacitor/push-notifications';

import { PopoverController } from '@ionic/angular';

import { NotificacionService } from 'src/app/services/notificacion.service';
import { PopinfoComponent } from './popinfo/popinfo.component';
import { Platform } from '@ionic/angular';
import { Router } from '@angular/router';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-notificaciones',
  templateUrl: './notificaciones.page.html',
  styleUrls: ['./notificaciones.page.scss'],
})
export class NotificacionesPage implements OnInit {
  titulo = 'Notificaciones';
  notificacionesRecibidas;
  isManager = false;
  segmentSelected = 'recibidas';

  constructor(private localService: StorageService, private popoverCtrl: PopoverController,
    public platform: Platform, private router: Router) {
      let userInfo = this.localService.localGet('userInfo');
      this.isManager = userInfo.rol == 'manager';
  }

  ngOnInit() {
  }

  async mostrarPop(event) {

    const popover = await this.popoverCtrl.create({
      component: PopinfoComponent,
      event: event,
      id: 'popinfo'
    });

    await popover.present();

  }

  segmentChanged(e) {
    console.log(e);
    this.segmentSelected = e.detail.value;
  }
}
