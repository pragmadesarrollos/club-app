import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NotificacionService } from 'src/app/services/notificacion.service';

@Component({
  selector: 'app-notificacion-enviada',
  templateUrl: './notificacion-enviada.page.html',
  styleUrls: ['./notificacion-enviada.page.scss'],
})
export class NotificacionEnviadaPage implements OnInit {
  titulo = 'Notificacion enviada';
  notificacion;
  constructor(private _notificacionService: NotificacionService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    const id = this.route.snapshot.params.id;
    this._notificacionService.obtenerNotificacionesPorId(id).subscribe(data => {
      console.log(data)
      this.notificacion = data;
    }, error => {
      console.log(error);
      //this.mensaje('Opss.. ocurrio un error');
    })
  }

}
