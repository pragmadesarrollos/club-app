import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { EncuestaService } from 'src/app/services/encuesta.service';
import { NotificacionService } from 'src/app/services/notificacion.service';

@Component({
  selector: 'app-enviadas',
  templateUrl: './enviadas.page.html',
  styleUrls: ['./enviadas.page.scss'],
})
export class EnviadasPage implements OnInit {
  notificacionesEnviadas;
  encuestasEnviadas;
  tokenInfo;

  constructor(private notificacionService: NotificacionService, 
    private encuestaService: EncuestaService, 
    private router: Router, public loadingController: LoadingController) { }

  ngOnInit() {
    this.tokenInfo = JSON.parse(localStorage.getItem('tokenInfo'))
    this.presentLoading();
    this.notificacionService.obtenerMisNotificacionesEnviadas().subscribe(data => {
      this.notificacionesEnviadas = data;
      this.loadingController.dismiss();
    }, error => {
      //this.dismissLoading();
      console.log(error);
      this.loadingController.dismiss();
      //this.mensaje('Opss.. ocurrio un error');
    })


    this.encuestaService.obtenerMisEncuestasEnviadas(this.tokenInfo.userId).subscribe(data => {
      this.encuestasEnviadas = data;
    }, error => {
      //this.dismissLoading();
      console.log(error);
      //this.mensaje('Opss.. ocurrio un error');
    })
  }

  verNotificacion(id) {
    this.router.navigate(['/menu/notificaciones/notificacion-enviada', id])
  }

  verEncuesta(id) {
    this.router.navigate(['menu/notificaciones/encuesta/', id], {
      queryParams: {
        editable: false
      }
    })
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'Cargando notificaciones...',
      duration: 10000
    });
    await loading.present();
  }

}
