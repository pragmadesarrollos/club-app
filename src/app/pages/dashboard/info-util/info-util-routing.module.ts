import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InfoUtilPage } from './info-util.page';

const routes: Routes = [
  {
    path: '',
    component: InfoUtilPage
  },
  {
    path: 'detalle-info/:idInfo',
    loadChildren: () => import('./detalle-info/detalle-info.module').then( m => m.DetalleInfoPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InfoUtilPageRoutingModule {}
