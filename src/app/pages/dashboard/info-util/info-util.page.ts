import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { InfoService } from 'src/app/services/info.service';

@Component({
  selector: 'app-info-util',
  templateUrl: './info-util.page.html',
  styleUrls: ['./info-util.page.scss'],
})
export class InfoUtilPage implements OnInit {
  titulo = 'Info útil';
  infoArray;

  constructor(private infoService: InfoService, private router: Router) { }

  ngOnInit() {
    this.infoService.obtenerInfoxClub().subscribe((d: any) => {
      console.error(d)
      if(d) {
        this.infoArray = d.result;
      }
    })
  }

  gotoDetalle(item) {
    this.router.navigate(['/menu/info-util/detalle-info', item.id]);
  }

}
