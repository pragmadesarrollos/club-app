import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetalleInfoPageRoutingModule } from './detalle-info-routing.module';

import { DetalleInfoPage } from './detalle-info.page';
import { HeaderComponent } from 'src/app/components/header/header.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetalleInfoPageRoutingModule,
  ],
  declarations: [DetalleInfoPage, HeaderComponent],
})
export class DetalleInfoPageModule {}
