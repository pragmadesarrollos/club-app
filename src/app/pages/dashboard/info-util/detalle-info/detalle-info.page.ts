import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Beneficio, BeneficioDetalle } from 'src/app/models/beneficio.model';
import { BeneficioService } from 'src/app/services/beneficio.service';
import { InfoService } from 'src/app/services/info.service';

@Component({
  selector: 'app-detalle-info',
  templateUrl: './detalle-info.page.html',
  styleUrls: ['./detalle-info.page.scss'],
})
export class DetalleInfoPage implements OnInit {
  titulo = 'Info util';
  info: any;

  constructor(private route: ActivatedRoute, private infoService: InfoService) {
    this.infoService.obtenerInfoxId(this.route.snapshot.params.idInfo).subscribe(data => {
      this.info = data;
      console.log('info DETALLE: ', this.info);
    });
  }

  ngOnInit() {}
}
