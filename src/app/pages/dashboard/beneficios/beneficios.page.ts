import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Beneficio, BeneficiosPorClub, Rubro } from 'src/app/models/beneficio.model';
import { BeneficioService } from 'src/app/services/beneficio.service';
import { RubroService } from 'src/app/services/rubro.service';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-beneficios',
  templateUrl: './beneficios.page.html',
  styleUrls: ['./beneficios.page.scss'],
})
export class BeneficiosPage implements OnInit {
  titulo = 'Beneficios';
  beneficios: BeneficiosPorClub[];
  beneficiosOriginal: BeneficiosPorClub[];
  filteredBeneficios: BeneficiosPorClub[];

  rubros: Rubro[];
  rubroSeleccionado: Rubro;

  searchbar: string;
  tokenInfo;

  constructor(
    private beneficioService: BeneficioService,
    private router: Router,
    private rubroService: RubroService,
    private storageService: StorageService
  ) {}

  ngOnInit() {
    this.tokenInfo = this.storageService.localGet('tokenInfo');
    this.beneficioService.obtenerBeneficiosxClub().subscribe(
      (data: any) => {
        this.beneficios = data;
        this.beneficiosOriginal = data;
        this.filteredBeneficios = data;
        console.log('BENEFICIOS: ', this.beneficios);
      },
      (error) => {
        console.log(error);
      }
    );
    this.rubroService.obtenerRubros().subscribe(
      (data) => {
        this.rubros = data;
        console.log('RUBROS: ', this.rubros);
      },
      (error) => {
        console.log(error);
      }
    );
  }

  goToDetalle(data: BeneficiosPorClub) {
    this.router.navigate(['/menu/beneficios/detalle-beneficio', data.beneficioId]);
  }

  filterList(evt) {
    const searchTerm = evt.srcElement.value;
    if (!searchTerm) {
      this.beneficios = this.filteredBeneficios;
    }

    if (searchTerm.length > 0) {
      this.beneficios = this.filteredBeneficios.filter((beneficio) => {
        if (beneficio.beneficio.nombre) {
          if (
            beneficio.beneficio.nombre.toLowerCase().indexOf(searchTerm.toLowerCase()) >
            -1
          ) {
            return true;
          }
          return false;
        }
      });
    }
  }

  haveItems(rubro) {
    
    let r = false;
    this.beneficios && this.beneficios.forEach((b: any) => {
      b.rubro && b.rubro.forEach( ru => {
        if(ru.rubroId == rubro.id) {
          r = true;
          return true;
        }
      })
    });
    return r;
  }

  selectedRubro(item: Rubro) {
    this.searchbar = '';
    this.filterList({ srcElement: {value: ''}});
    const self = this;
    this.rubroSeleccionado = item;
    this.setFilteredList();
    this.beneficios = this.filteredBeneficios.filter(
        (data) => self.matchRubroAndBeneficio(data, this.rubroSeleccionado.id)
      );
  }

  setFilteredList() {
    const self = this;
    this.filteredBeneficios = this.beneficiosOriginal.filter(
      (data) => self.matchRubroAndBeneficio(data, this.rubroSeleccionado.id)
    );
  }

  matchRubroAndBeneficio(beneficio, rubroId) {
    let match = false
    beneficio.rubro.forEach(r => {
      if(r.rubroId == rubroId) {
        match = true;
      }
    });
    return match;
  }

  selectedTodos(item) {
    if (this.rubroSeleccionado) {
      if (item.id === this.rubroSeleccionado?.id) {
        this.rubroSeleccionado = null;
        this.beneficios = this.beneficiosOriginal;
      } else {
        this.rubroSeleccionado = item;
        this.beneficios = this.beneficiosOriginal;
      }
    } else {
      this.rubroSeleccionado = item;
      this.beneficios = this.beneficiosOriginal;
    }
  }

  getEmptyImg() {
    return 'tito.png'
  }
}
