import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MenuService } from 'src/app/services/menu.service';
import { Router } from '@angular/router';
import { StorageService } from 'src/app/services/storage.service';
import { IonicSelectableComponent } from 'ionic-selectable';
import { ClubService } from 'src/app/services/club.service';
import { Club } from '../../../class/club.model';
import { LoadingController, ToastController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { NotificacionService } from 'src/app/services/notificacion.service';
import * as $ from 'jquery';
import { EncuestaService } from 'src/app/services/encuesta.service';
import { EventsService } from 'src/app/services/events.service';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {
  listClubesCambiar: Club[];
  listClubesAgregar: Club[];
  clubCambiar: Club;
  clubAgregar: Club;
  menu: any[] = [];
  loading: HTMLIonLoadingElement;
  userInfo;
  tokenInfo;
  cantidadClubes: Number;
  userData;
  localData;

  @ViewChild('selectAgregar') selectAgregar: IonicSelectableComponent;

  notificationRedirectUrl = null;

  constructor(
    private _menuService: MenuService,
    private router: Router,
    private _storageService: StorageService,
    private _clubService: ClubService,
    public loadingController: LoadingController,
    public toastController: ToastController,
    private actRoute: ActivatedRoute,
    private notificationService: NotificacionService,
    private encuestasService: EncuestaService,
    private events: EventsService,
    private usersService: UsuarioService
  ) {
    this._clubService.bindDashboardChanges().subscribe(d => {
      this.tokenInfo = this._storageService.localGet('tokenInfo');
      this.userInfo = this._storageService.localGet('userInfo');
    });
    this.usersService.bindProfilePhoto().subscribe(() => {
      this.updatePhoto()
    })
  }

  ngOnInit() {
    this.setData();
  }

  updatePhoto() {
    this.tokenInfo = this._storageService.localGet('tokenInfo');
    this.usersService.obtenerUsuario(this.tokenInfo.userId).subscribe(u => {
      this.userData = u;
    })
  }

  setData() {
    this.userInfo = this._storageService.localGet('userInfo');
    this.tokenInfo = this._storageService.localGet('tokenInfo');
    const self = this;
    self._clubService.setLoading(true);
    self.verifyNotifications(800);
    self.events.on("event.notification").subscribe(() => {
      self.verifyNotifications(10);
    });
    self.usersService.obtenerUsuario(this.tokenInfo.userId).subscribe(u => {
      this.userData = u;
    })
  }

  private verifyNotifications(timeout) {
    const self = this;
    const idNoti = self.notificationService.getNotificationId();
    if (idNoti) {
      self.notificationRedirectUrl = `/menu/notificaciones/notificacion/${idNoti}`;
      console.log('notificationRedirectUrl', self.notificationRedirectUrl);
      setTimeout(() => {
        //self.router.navigate(['/menu/notificaciones/notificacion/', id]);
        $("#btnGotoNotification").click();
      }, timeout);
    } else {
      const idEncuesta = self.encuestasService.getEncuestaId();
      if (idEncuesta) {
        self.notificationRedirectUrl = `/menu/notificaciones/encuesta/${idEncuesta}?editable=true`;
        console.log('notificationRedirectUrl', self.notificationRedirectUrl);
        setTimeout(() => {
          //self.router.navigate(['/menu/notificaciones/notificacion/', id]);
          $("#btnGotoNotification").click();
        }, timeout);
      }
    }
  }

  ionViewWillEnter() {
    this.setClubesAndMenu();
  }

  setClubesAndMenu() {
    const self = this;
    this.obtenerMenu();
    this.obtenerClubes();
    this.obtenerCLubesDeUsuario();
  }

  obtenerCLubesDeUsuario() {
    let clubFromLocalData = this._storageService.localGet('clubInfo-' + this.tokenInfo.userId);
    console.warn('Club del usuario: ', clubFromLocalData);
    this.clubCambiar = clubFromLocalData;
    this.presentLoading().then((loadRes: any) => {
      this.loading = loadRes;
      this._clubService.obtenerClubesxUsuario(this.tokenInfo.userId).subscribe(
        (data) => {
          if(data) {
            this.listClubesCambiar = data.map(p => p.club);
            this.cantidadClubes = data.length;
            if(clubFromLocalData) {
              this.setearColores(clubFromLocalData);
            } else {
              this.setearColores(data[0].club);
              this._storageService.localSet('clubInfo-' + this.tokenInfo.userId, data[0].club );
            }
            
            
            this.dismissLoading();
          }
          this._clubService.setLoading(false);
        },
        (error) => {
          console.error(error);
          this.dismissLoading();
          this.mensaje('Opss.. ocurrio un error');
        }
      );
    });
  }

  setearColores(data) {
    document.querySelector('body').style.setProperty('--ion-color-primary', data.colorPrimario);
    document.querySelector('body').style.setProperty('--ion-color-secondary', data.colorSecundario);
    document.querySelector('body').style.setProperty('--ion-color-primary-contrast', data.colorTextoPrimario);
    document.querySelector('body').style.setProperty('--ion-color-secondary-contrast', data.colorTextoSecundario);
  }

  cerrarSesion() {
    this.presentLoading().then(async (loadRes: any) => {
      this.loading = loadRes;
      this.loading.present();
      this.usersService.cerrarSession().toPromise().then(() => {
        this.dismissLoading();
        localStorage.removeItem('token');
        localStorage.removeItem('tokenInfo');
        localStorage.removeItem('userInfo');
        this.router.navigate(['login']);
      }).catch((reason) => {
        console.log(reason);
        this.dismissLoading();
        this.mensaje('Opss.. ocurrio un error');
      });
    });

  }

  obtenerMenu() {
    this._menuService.getMenuSocio().subscribe((data) => {
      this.menu = data;
    });
  }

  clubChangeCambiar(event: {
    component: IonicSelectableComponent;
    value: any;
  }) {
    this.clubSeleccionadoCambiar(event.value.id);
  }

  clubChangeAgregar(event: {
    component: IonicSelectableComponent;
    value: any;
  }) {
    this.clubSeleccionadoAgregar(event.value.id);
  }

  clubSeleccionadoCambiar(idClub) {
    let tok = this._storageService.localGet('tokenInfo');
    this.presentLoading().then((loadRes: any) => {
      this.loading = loadRes;
      this.loading.present();
      this._clubService.obtenerClubesxId(idClub).subscribe(club => {
        tok.clubId = club.id;
        this._storageService.localSet('tokenInfo', tok);
        this._storageService.localSet('clubInfo-' + this.tokenInfo.userId, club);
        this.sendClubChanged(club.id);
        this.setearColores(club);
        this.dismissLoading();
      },
        (error) => {
          console.log(error);
          this.dismissLoading();
          this.mensaje('Opss.. ocurrio un error');
        }
      )
    });
  }

  sendClubChanged(id) {
    this._clubService.changeClub(id);
  }

  clubSeleccionadoAgregar(idClub) {
    this.presentLoading().then((loadRes: any) => {
      this.loading = loadRes;
      this.loading.present();

      this._clubService.obtenerDisciplinasByIdClub(idClub).subscribe(
        (data) => {
          // this.listDisciplina = data;
          this.dismissLoading();
          this.router.navigateByUrl('/menu/agregar-club/' + idClub);
        },
        (error) => {
          console.log(error);
          this.dismissLoading();
          this.mensaje('Opss.. ocurrio un error');
        }
      );
    });
  }

  agregarClub() {
    this.selectAgregar.open();
  }

  obtenerClubes() {
    this.presentLoading().then((loadRes: any) => {
      this.loading = loadRes;
      //this.loading.present()

      this._clubService.obtenerClubesXuser().subscribe(
        (data) => {
          this.dismissLoading();
          this.listClubesAgregar = data;
        },
        (error) => {
          this.dismissLoading();
          this.mensaje('Opss.. ocurrio un error');
        }
      );
    });
  }

  presentLoading() {
    if (this.loading) {
      this.loading.dismiss();
    }
    return new Promise((resolve) => {
      resolve(
        this.loadingController.create({
          message: 'Espere...',
        })
      );
    });
  }

  async dismissLoading(): Promise<void> {
    if (this.loading) {
      this.loading.dismiss();
    }
  }

  async mensaje(mensaje: string) {
    const toast = await this.toastController.create({
      message: mensaje,
      color: 'dark',
      duration: 3000,
    });
    toast.present();
  }
}
