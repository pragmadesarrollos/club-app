import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DeportesPageRoutingModule } from './deportes-routing.module';

import { DeportesPage } from './deportes.page';
import { HeaderComponent } from 'src/app/components/header/header.component';
import { IonicSelectableModule } from 'ionic-selectable';
import { ModalDeportesModule } from 'src/app/components/modal-deportes/modal-deportes.module';
import { DeportePage } from './deporte.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DeportesPageRoutingModule,
    IonicSelectableModule,
    ModalDeportesModule
  ],
  declarations: [
    DeportesPage,
    HeaderComponent,
    DeportePage
  ]
})
export class DeportesPageModule {}
