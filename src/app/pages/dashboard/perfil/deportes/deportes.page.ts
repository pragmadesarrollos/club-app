import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { ModalDeportesComponent } from 'src/app/components/modal-deportes/modal-deportes.component';
import { ClubService } from 'src/app/services/club.service';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-deportes',
  templateUrl: './deportes.page.html',
  styleUrls: ['./deportes.page.scss'],
})
export class DeportesPage implements OnInit {
  titulo = 'Deportes';
  form: FormGroup;
  listDeportes: any[] = [];
  lisDeportesDelClub: any[];

  constructor(
    private modalController: ModalController,
    private _clubService: ClubService,
    private _storageService: StorageService,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.setData();
  }

  setData() {
    const tokenInfo = this._storageService.localGet('tokenInfo');
    const clubInfo = this._storageService.localGet('clubInfo-' + tokenInfo.userId);
    this._clubService.obtenerDisciplinasByUserIdAndClubId(tokenInfo.userId, clubInfo.id).subscribe(
      (data: any[]) => {
        //this.dismissLoading();
        this.listDeportes = data;
      },
      (error) => {
        //this.dismissLoading();
        console.log(error);
        //this.mensaje('Opss.. ocurrio un error');
      }
    );

    this._clubService.obtenerDisciplinasByIdClub(clubInfo.id).subscribe(disc => {
      this.lisDeportesDelClub = this.formatNames(disc);
    })
  }

  formatNames(arr): any[] {
    let r = [];
    arr.forEach(p => {
      let it = p;
      it['nombre'] = p.disciplina.nombre;
      r.push(it);
    });
    return r;
  }

  openModalDeportes() {
    this.presentModal();
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: ModalDeportesComponent,
      cssClass: 'my-custom-class',
      componentProps: { 
        listDisciplinas: this.lisDeportesDelClub
      }
    });
    await modal.present();
    const data = await modal.onDidDismiss();
    if(data) {
      this.setData()
    }
  }
}
