import { Component, Input, OnInit } from '@angular/core';
import { ClubService } from 'src/app/services/club.service';
import { DisciplinaService } from 'src/app/services/disciplina.service';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-deporte',
  templateUrl: './deporte.page.html',
  styleUrls: ['./deportes.page.scss'],
})
export class DeportePage implements OnInit {
  @Input() deporte;
  division;
  posicion;

  constructor(
    private disciplinaService: DisciplinaService,
    private _storageService: StorageService,
  ) {}

  ngOnInit() {
    const tokenInfo = this._storageService.localGet('tokenInfo');
    const clubInfo = this._storageService.localGet('clubInfo-' + tokenInfo.userId);
    this.disciplinaService.obtenerDivisionesxDisciplina(clubInfo.id, this.deporte.disciplinaxclub.disciplinaId).subscribe(
      (data: any[]) => {
        this.setDivision(data);
        
      },
      (error) => {
        //this.dismissLoading();
        console.log(error);
        //this.mensaje('Opss.. ocurrio un error');
      }
    );

    this.disciplinaService.obtenerPosicionXDisciplinaId( this.deporte.disciplinaxclub.disciplinaId).subscribe(
        (data: any[]) => {
          this.setPosicion(data);
        },
        (error) => {
          //this.dismissLoading();
          console.log(error);
          //this.mensaje('Opss.. ocurrio un error');
        }
      );

    
  }

  setDivision(arr) {
    
    this.division = this.deporte.disxclubxdivId ? arr.find(p => p.id == this.deporte.disxclubxdivId)?.nombre : 'Sin especificar';
  }

  setPosicion(arr) {
    console.error('Set posicion: ', this.deporte.disciplinaxclubxposId, arr);
    let pos = arr.find(p => p.id == this.deporte.disciplinaxclubxposId);
    this.posicion = pos ? pos.nombre : 'Sin especificar';
  }

}
