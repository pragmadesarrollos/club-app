import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { StorageService } from 'src/app/services/storage.service';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-cambiar-clave',
  templateUrl: './cambiar-clave.page.html',
  styleUrls: ['./cambiar-clave.page.scss'],
})
export class CambiarClavePage implements OnInit {
  email;

  constructor(private _storageService: StorageService, private _usuarioService: UsuarioService, private toastController: ToastController) { }

  ngOnInit() {
    let tokenInfo = this._storageService.localGet('tokenInfo');
    this._usuarioService.obtenerUsuario(tokenInfo.userId).subscribe(u => {
      this.email = u.persona.correo;
    })

  }

  async presentToast(msg, type) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000,
      color: type,
      position: 'top'
    });
    toast.present();
  }

  saveChanges() {
    this._usuarioService.resetPassword(this.email).subscribe(d => {
      this.presentToast('Se ha enviado el email con exito!', 'success')
    }, e => {
      console.error(e)
      this.presentToast('No se ha podido enviar el email: ' + e.error.message, 'danger')})
  }

}


