import { Component, OnInit, ViewChild } from '@angular/core';
import { ClubService } from 'src/app/services/club.service';
import { StorageService } from 'src/app/services/storage.service';
import { UsuarioService } from 'src/app/services/usuario.service';
import * as $ from 'jquery';
import { AlertController, IonInput } from '@ionic/angular';
import { FormBuilder, FormGroup } from '@angular/forms';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.page.html',
  styleUrls: ['./perfil.page.scss'],
})
export class PerfilPage implements OnInit {
  titulo = 'Mi Perfil';
  userInfo;
  tokenInfo;
  usuario;
  club;
  @ViewChild('inputPhoto') inputPhoto: IonInput;

  acciones: any[] = [
    { nombre: 'Deportes', redirect: '/menu/perfil/deportes' },
    { nombre: 'Datos Personales', redirect: '/menu/perfil/datos-personales' },
    { nombre: 'Cambiar Clave', redirect: '/menu/perfil/cambiar-clave' },
    { nombre: 'Mi Club', redirect: '/menu/perfil/mi-club' },
  ]

  uploadForm: FormGroup; 

  constructor(private alertController: AlertController, private loadingController: LoadingController, private formBuilder: FormBuilder, private _clubService: ClubService, private _storageService: StorageService, private _usuarioService: UsuarioService) { 
    this.tokenInfo = this._storageService.localGet('tokenInfo');
  }

  ngOnInit() {
    this.setData()
    
  }

  ionViewWillEnter() {
    this.setData()
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'Cargando imagen...',
      duration: 0,
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
  }

  setData() {
    this.userInfo = this._storageService.localGet('userInfo');
    this._usuarioService.obtenerUsuario(this.tokenInfo.userId).subscribe(data => {
      this.usuario = data;
      this.loadingController.dismiss();
    }, error => {
      console.log(error);
      this.loadingController.dismiss();
    });
  
    this._clubService.obtenerClubesxId(this.tokenInfo.clubId).subscribe(data => {
      this.club = data;
    }, error => {
      console.log(error);
    });

    this.uploadForm = this.formBuilder.group({
      profile: ['']
    });
  }

  addPhoto() {
    console.log(this.inputPhoto)
    $('#inputPhoto').click();
  }

  onFileSelect(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      if(file) {
        if(+file.size >= 500000) {
          this.presentAlert();
        } else {
          this.uploadForm.get('profile').setValue(file);
          this.onSubmit();
        }
      }

    }
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Error',
      subHeader: 'Imagen demasiado grande',
      message: 'La imagen que estas queriendo cargar es demasiado grande. (limite: 5MB)',
      buttons: ['Cerrar']
    });

    await alert.present();

    const { role } = await alert.onDidDismiss();
  }

  async onSubmit() {

    this.presentLoading();

    let token = localStorage.getItem('token');

    const formData = new FormData();
    formData.append('imagen', this.uploadForm.get('profile').value);

    var myHeaders = new Headers();
    myHeaders.append("Accept", "application/json");
    myHeaders.append("Authorization", "Bearer " + token)


   

          var requestOptions = {
                    method: 'PUT',
                    headers: myHeaders,
                    body: formData
                  };

    const result = await fetch('https://api.klubo.club/api/personas/update/image/' + this.tokenInfo.userId, requestOptions)
    
    this.setData();
    this.bindPhoto()

    
    // this._usuarioService.uploadFile(this.tokenInfo.userId, formData).subscribe(d => {
      
    // }, error => console.error(error));
  }

  bindPhoto() {
    this._usuarioService.updatePhoto(new Date().getTime())
  }

}
