import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UsuarioService } from 'src/app/services/usuario.service';
import { StorageService } from 'src/app/services/storage.service';
import { AlertController, LoadingController, NavController, ToastController } from '@ionic/angular';
import { DireccionService } from 'src/app/services/direccion.service';

@Component({
  selector: 'app-datos-personales',
  templateUrl: './datos-personales.page.html',
  styleUrls: ['./datos-personales.page.scss'],
})
export class DatosPersonalesPage implements OnInit {
  titulo = 'Datos Personales';
  form: FormGroup;
  usuario: object;
  tokenInfo;
  loading: HTMLIonLoadingElement;

 

  constructor(private _usuarioService: UsuarioService,
    private _storageService: StorageService,
    private fb: FormBuilder,
    public toastController: ToastController,
    public loadingController: LoadingController,
    public alertController: AlertController,
    private navController: NavController,
    private _direccionService: DireccionService
    ) {
      this.form = this.fb.group({
        correo: ['', Validators.required],
        nombre: ['', Validators.required],
        apellido: ['', Validators.required],
        documento: ['', Validators.required],
        fechaNacimiento: ['', Validators.required],
        calle: ['', Validators.required],
        numero: ['', Validators.required],
        localidad: ['', Validators.required],
        cp: ['', Validators.required],
        telefono: ['', Validators.required],
      })

      this.tokenInfo = this._storageService.localGet('tokenInfo');
     }

   presentLoading() {
      if (this.loading) {
        this.loading.dismiss();
      }
      return new Promise((resolve) => {
        resolve(this.loadingController.create({
          message: 'Espere...'
        }));
      })
    }

    async dismissLoading(): Promise<void> {
      if (this.loading) {
        this.loading.dismiss();
      }
    }

    async mensaje(mensaje: string, type?: string) {
      const toast = await this.toastController.create({
        message: mensaje,
        duration: 3000,
        position: 'top',
        color: type || 'dark',
        translucent: true,
        buttons: [
          {
            side: 'end',
            icon: 'close',
            handler: () => {
              toast.dismiss();
            }
          }
        ]
      });
      toast.present();
    }

    ngOnInit() {
    
      this.presentLoading().then((loadRes: any) => {
        this.loading = loadRes
        this.loading.present()
        const self = this;
    
          this._usuarioService.obtenerUsuario(this.tokenInfo.userId).subscribe(data => {
            console.warn(data)
            this.dismissLoading();
            this.usuario = data;
            
            self.form.setValue({
              correo: self.usuario['persona'].correo,
              nombre: self.usuario['persona'].nombre,
              apellido: self.usuario['persona'].apellido,
              documento: self.usuario['persona'].documento,
              fechaNacimiento: self.usuario['persona'].fechaNacimiento,
              calle: self.usuario['persona'].direccionPersona.calle,
              localidad: self.usuario['persona'].direccionPersona.localidad,
              cp: self.usuario['persona'].direccionPersona.cp,
              numero: self.usuario['persona'].direccionPersona.numero,
              telefono: self.usuario['persona'].telefono
            });
          }, error => {
            self.dismissLoading();
            console.log(error);
            self.mensaje('Opss.. ocurrio un error');
          })

      })

  }

  async guardarDatos() {
    const datosUsuario = this.form.value;

    let ob = {
      nombre: datosUsuario.nombre,
      apellido: datosUsuario.apellido,
      documento: datosUsuario.documento,
      telefono: datosUsuario.telefono,
      fechaNacimiento: datosUsuario.fechaNacimiento,
      correo: datosUsuario.correo,
      direccion: {
        calle: datosUsuario.calle,
        numero: datosUsuario.numero,
        cp: datosUsuario.cp,
        localidad: datosUsuario.localidad,
        provinciaID: 1
      }
    }

    if(this.form.valid) {
      console.log('form valido')
    } else {
      console.error('form invalido');
      this.mensaje('Por favor, complete todos los datos obligatorios', 'danger')
      return;
    }

    this._usuarioService.modificarUsuario( this.tokenInfo.clubId,  this.tokenInfo.userId ,ob).subscribe(async data => {
      this.mensaje('Tus datos fueron modificados correctamente', 'success');
      this.setLocalStorageName(ob.nombre);
      this.navController.back();
    }, error => {
      //this.dismissLoading();
      console.error(error);
      //this.mensaje('Opss.. ocurrio un error');
    })
  }

  setLocalStorageName(name) {
    let ti = JSON.parse(localStorage.getItem('userInfo'));
    ti.nombre = name;
    localStorage.setItem('userInfo', JSON.stringify(ti));
  }

  
  

  setDireccion(dir) {
    this.usuario['calle'] = dir.calle;
    this.usuario['localidad'] = dir.localidad;
    this.usuario['cp'] = dir.cp;
    this.usuario['numero'] = dir.numero;
  }


}
