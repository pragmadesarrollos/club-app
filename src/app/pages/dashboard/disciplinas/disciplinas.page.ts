import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { Disciplina } from 'src/app/class/disciplina.model';
import { ModalEspaciosComponent } from 'src/app/components/modal-espacios/modal-espacios.component';
import { DisciplinaService } from 'src/app/services/disciplina.service';
import { EspacioService } from 'src/app/services/espacio.service';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-disciplinas',
  templateUrl: './disciplinas.page.html',
  styleUrls: ['./disciplinas.page.scss'],
})
export class DisciplinasPage implements OnInit {
  titulo = 'Disciplinas';
  disciplinas: Disciplina[];
  disciplinasAux: any[];
  tokenInfo;

  constructor(
    private disciplinaService: DisciplinaService,
    private router: Router,
    private storageService: StorageService,
    private espacioService: EspacioService,
    public _storageService: StorageService,
    public modalController: ModalController,
  ) {}

  ngOnInit() {
    this.tokenInfo = this.storageService.localGet('tokenInfo');
    this.disciplinaService.obtenerDisciplinasXClub(this.tokenInfo.clubId).subscribe(d => {
      this.disciplinas = d;
      this.disciplinasAux = d;
    })
    
  }

  filterList(e) {
    this.disciplinas = this.disciplinasAux.filter(p => (p.disciplina.nombre.toLowerCase()).indexOf( (e.detail.value).toLowerCase() ) > -1)
  }

  mostrarPop(disciplina) {
    this._storageService.set('disciplinaSelected', disciplina);
    this.espacioService.getEspaciosPorDisciplina(disciplina.id).subscribe( esps => {
      console.error(esps);
      this.presentarEspaciosModal(esps);
    })
  }

  async presentarEspaciosModal(esps) {
    const modal = await this.modalController.create({
      component: ModalEspaciosComponent,
      cssClass: 'my-custom-class',
      swipeToClose: true,
      componentProps: {
        espacios: esps,
        titulo: 'Selecciona un espacio'
      }
    });
    return await modal.present();
  }

  
}
