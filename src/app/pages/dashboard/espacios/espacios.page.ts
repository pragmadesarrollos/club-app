import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { Espacio } from 'src/app/models/ingresos.model';
import { DisciplinaService } from 'src/app/services/disciplina.service';
import { EspacioService } from 'src/app/services/espacio.service';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-espacios',
  templateUrl: './espacios.page.html',
  styleUrls: ['./espacios.page.scss'],
})
export class EspaciosPage implements OnInit {
  titulo = 'Espacios';
  espacios: Espacio[];
  espaciosAux = [];
  tokenInfo;

  constructor(
    private disciplinaService: DisciplinaService,
    private router: Router,
    private storageService: StorageService,
    private espacioService: EspacioService,
    public modalController: ModalController,
    private route: ActivatedRoute,
  ) {}

  ngOnInit() {
    this.tokenInfo = this.storageService.localGet('tokenInfo');
    this.espacioService.getEspaciosDelClub(this.tokenInfo.clubId).subscribe((d: Espacio[]) => {
      console.log('espacios: ', d);
      this.espacios = d;
      this.espaciosAux = d;
    })
   
    
  }

  filterList(e) {
    this.espacios = this.espaciosAux.filter(p => (p.nombre.toLowerCase()).indexOf( (e.detail.value).toLowerCase() ) > -1)
  }

  chooseEspacio(espacio) {
    console.log(espacio, JSON.parse(this.route.snapshot.params.qr) );
    let gotoQrReader = JSON.parse(this.route.snapshot.params.qr);
    this.router.navigate(['/menu/ingresos/'+ (gotoQrReader ? 'escanea-codigo' : 'listado-ingresos') +'/', espacio.id]);
  }

 
  
}
