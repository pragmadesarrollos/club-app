import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { TurnoService } from 'src/app/services/turno.service';
import { Router } from '@angular/router';
import { MasterService } from 'src/app/helpers/master.service';
import { EspacioService } from 'src/app/services/espacio.service';
import { ReservaService } from 'src/app/services/reserva.service';
import { Reserva } from 'src/app/models/ingresos.model';
import * as moment from 'moment';


interface Turno {
  activo: number;
  id: number;
  cupo: string;
  fecha: string;
  horaDesde: string;
  horaHasta: string;
}

@Component({
  selector: 'app-confirmado',
  templateUrl: './confirmado.page.html',
  styleUrls: ['./confirmado.page.scss'],
})
export class ConfirmadoPage implements OnInit {
  titulo = 'Turno confirmado';
  turno: Reserva;
  esp: any;

  constructor(
    private _turnoService: TurnoService,
    private route: ActivatedRoute,
    public alertController: AlertController,  
    private router: Router,
    private _reservaService: ReservaService,
    private espaciosService: EspacioService,
    public masterService: MasterService) { 
    }

  ngOnInit() {
  
    const self = this;
    self._reservaService.getReservaById(this.route.snapshot.params.id).subscribe((r: Reserva) => {
      self.turno = r;
      console.error(r);
    }, error => {
      //this.dismissLoading();
      console.log(error);
      //this.mensaje('Opss.. ocurrio un error');
    });
    
    this.espaciosService.getEspacio(this.route.snapshot.params.espacioId).subscribe(function(e: any) {
      if(e) {
        self.esp = e;
        console.warn(self.esp);
      }
      
    }, error => {
      //this.dismissLoading();
      console.log(error);
      //this.mensaje('Opss.. ocurrio un error');
    })

  }

  getFecha(f) {
    return moment(f).locale('es').format('DD [de] MMMM');
  }


  confirmarTurno() {
    this.presentAlertConfirm();
  }

  cancelarTurno() {
    this.presentAlertCancel();
  }

  async presentAlertCancel() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Cancelar turno',
      message: 'Estás por cancelar el turno',
      buttons: [
        {
          text: 'VOLVER',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {

            this.masterService.setSplash({
              img: 'assets/img/splash/error.png',
              redirecTo: '/menu',
              titulo: 'No se pudo cancelar tu turno',
              subtitulo: '',
            });

            this.router.navigateByUrl('/menu/splash-generico');
            console.log('Operacion cancelada');
          }
        }, {
          text: 'SI CANCELAR',
          handler: () => {

            this._turnoService.cancelarTurno(this.turno.id).subscribe(d => {
              this.masterService.setSplash({
                img: 'assets/img/splash/correcto.png',
                redirecTo: '/menu',
                titulo: '¡Turno cancelado con éxito!',
                subtitulo: '',
              });
              this.router.navigateByUrl('/menu/splash-generico');
            })
            
            //this.recuperar();
          }
        }
      ]
    });

    await alert.present();
  }

  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Cancelar turno',
      message: 'Estás por confirmar el turno',
      buttons: [
        {
          text: 'VOLVER',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {

            this.masterService.setSplash({
              img: 'assets/img/splash/error.png',
              redirecTo: '/menu',
              titulo: 'No se pudo reservar tu turno',
              subtitulo: '',
            });

            this.router.navigateByUrl('/menu/splash-generico');
            console.log('Operacion cancelada');
          }
        }, {
          text: 'SI CONFIRMAR',
          handler: () => {


            this.masterService.setSplash({
              img: 'assets/img/splash/correcto.png',
              redirecTo: '/menu',
              titulo: '¡Turno confirmado con éxito!',
              subtitulo: '',
            });
            this.router.navigateByUrl('/menu/splash-generico');

            console.log('Operacion aceptada');
            //this.recuperar();
          }
        }
      ]
    });

    await alert.present();
  }

  splash(){
    this.masterService.setSplash({
      img: 'assets/img/splash/correcto.png',
      redirecTo: '/menu',
      titulo: 'Titulo',
      subtitulo: 'Le enviamos una notificación al socio',
    });
    this.router.navigateByUrl('/menu/splash-generico');
  }

  getHourDiff(turno) {
    let fDesde = moment(turno.fechaInicio).format('YYYY-MM-DD') + ' ' + turno.desde;
    let fHasta = moment(turno.fechaInicio).format('YYYY-MM-DD') + ' ' + turno.hasta;
    let d = moment(fHasta).diff(moment(fDesde), 'minutes');
    return d;
  }


}
