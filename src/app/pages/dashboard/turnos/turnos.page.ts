import { Component, OnInit } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';
import { Router } from '@angular/router';
import { EspacioService } from 'src/app/services/espacio.service';
import { StorageService } from 'src/app/services/storage.service';
import { Espacio } from 'src/app/models/ingresos.model';

@Component({
  selector: 'app-turnos',
  templateUrl: './turnos.page.html',
  styleUrls: ['./turnos.page.scss'],
})
export class TurnosPage implements OnInit {
  titulo = 'Turnos';
  espacios: Espacio[];
  tokenInfo;

  constructor(
    public actionSheetController: ActionSheetController,
    private _storageService: StorageService,
    private router: Router,   private espacioService: EspacioService) { 
      this.espacioService.getEspacios().subscribe(data => {
        this.espacios = data
      });
      
      
    }

  ngOnInit() {
    
    this.tokenInfo = this._storageService.localGet('tokenInfo');

  }

  async mostrarPop(event) {

    const actionSheet = await this.actionSheetController.create({
      header: 'Selecciona un espacio',
      buttons: this.returnButtons()
    });
    await actionSheet.present();

  }

  gotoDisciplinas() {
    this.router.navigate(['/menu/turnos/disciplinas' ]);
  }

  returnButtons()  {
    const buttons = [];
    if (this.espacios.length > 0) {
      this.espacios.forEach((data) => {
        buttons.push({
          text: data.nombre,
          icon: 'football-outline',
          handler: () => {
            console.log('ESPACIO SELECCIONADO', data);
            this.router.navigate(['/menu/turnos/sacar-turno/', data.id]);
          },
        });
      });
    }
    return buttons;
  }


}
