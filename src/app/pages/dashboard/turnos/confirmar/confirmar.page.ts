import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { TurnoService } from 'src/app/services/turno.service';
import { Router } from '@angular/router';
import { MasterService } from 'src/app/helpers/master.service';
import { EspacioService } from 'src/app/services/espacio.service';
import { StorageService } from 'src/app/services/storage.service';
import * as moment from 'moment';


interface Turno {
  activo: number;
  id: number;
  cupo: string;
  fecha: string;
  horaDesde: string;
  horaHasta: string;
}

@Component({
  selector: 'app-confirmar',
  templateUrl: './confirmar.page.html',
  styleUrls: ['./confirmar.page.scss'],
})
export class ConfirmarPage implements OnInit {
  titulo = 'Confirmar turno';
  turno: any;
  esp: any;
  reserva; 
  tokenInfo;
  disciplina;

  constructor(
    private _turnoService: TurnoService,
    private route: ActivatedRoute,
    public alertController: AlertController,  
    private router: Router,
    private espaciosService: EspacioService,
    private _storageService: StorageService,
    public masterService: MasterService) { 
      this.tokenInfo = this._storageService.localGet('tokenInfo');
    }

  ngOnInit() {
    this._storageService.get('reservaActual').then(t => {
      this.turno = t;
      console.error(this.turno)
    });
    this._storageService.get('disciplinaSelected').then(d => {
      this.disciplina = d;
      console.warn(d);
    })

    
  }



  confirmarTurno() {
    this.presentAlertConfirm();
  }

  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Confirmar',
      message: '¿Estás seguro de confirmar el turno?',
      buttons: [
        {
          text: 'Volver',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {

            console.log('Operacion cancelada');
          }
        }, {
          text: 'Sí, confirmar',
          handler: () => {

            this.crearTurno().subscribe(d => {
              this.masterService.setSplash({
                img: 'assets/img/splash/correcto.png',
                redirecTo: '/menu',
                titulo: '¡Turno confirmado con éxito!',
                subtitulo: '',
              });
              this.router.navigateByUrl('/menu/splash-generico');
  
              console.log('Operacion aceptada');
            })
         
            
            //this.recuperar();
          }
        }
      ]
    });

    await alert.present();
  }

  splash(){
    this.masterService.setSplash({
      img: 'assets/img/splash/correcto.png',
      redirecTo: '/menu',
      titulo: 'Titulo',
      subtitulo: 'Le enviamos una notificación al socio',
    });
    this.router.navigateByUrl('/menu/splash-generico');
  }

  getHourDiff(turno) {
    let fDesde = moment(turno.fechaInicio).format('YYYY-MM-DD') + ' ' + turno.desde;
    let fHasta = moment(turno.fechaInicio).format('YYYY-MM-DD') + ' ' + turno.hasta;
    let d = moment(fHasta).diff(moment(fDesde), 'minutes');
    return d;
  }

  crearTurno() {
  
    let obj = {
      "fechaInicio": this.turno.fecha,
      "fechaFin": this.turno.fecha,
      "desde": this.turno.desde,
      "hasta": this.turno.hasta,
      "activo": 1,
      "espacioId": this.turno.espacio.id,
      "usuarioId": this.tokenInfo.userId,
      "estadoreservaId": 1,
      "disciplinaxclubId": this.disciplina.id
    }
    return this._turnoService.crearNuevaReserva(obj);
  }


}
