import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { TurnoService } from 'src/app/services/turno.service';
import { Router } from '@angular/router';
import { StorageService } from 'src/app/services/storage.service';
import { EspacioService } from 'src/app/services/espacio.service';
//import { Espacio } from 'src/app/models/ingresos.model';
import * as moment from 'moment';
import { List } from 'linqts';
import { ConfiguracionDiasHs, Espacio } from 'src/app/models/ingresos.model';
import { ClubService } from 'src/app/services/club.service';
import { ReservaService } from 'src/app/services/reserva.service';

class Turno {
  activo: number;
  id: number;
  cupo: string;
  fecha: string;
  horaDesde: string;
  horaHasta: string;
  espacioId: number;
  estadoturnoId: number;
  precio: number;

  constructor() {
    this.activo = 0;
    this.fecha = new Date().toDateString();
  }

}

@Component({
  selector: 'app-sacar-turno',
  templateUrl: './sacar-turno.page.html',
  styleUrls: ['./sacar-turno.page.scss'],
})
export class SacarTurnoPage implements OnInit {
  titulo = 'Elegir turno';
  turno: Turno;
  tokenInfo;
  turnos;
  idTurno;
  espacio: Espacio;
  configEspacio: ConfiguracionDiasHs[];
  fecha: string;
  currentDate = moment();
  currentMonth = moment(this.currentDate).locale('es').format('MMMM');
  thisMonthConstant = moment().locale('es').format('MMMM');
  days = [];
  daySelected = false;
  daysEnabledConfig = {
    1: false,
    2: false,
    3: false,
    4: false,
    5: false,
    6: false,
    7: false
  };
  club;

  times = []

  constructor(private _turnoService: TurnoService, private route: ActivatedRoute
    , public alertController: AlertController, private router: Router,
    private clubService: ClubService, private reservasService: ReservaService,
    private _storageService: StorageService, private espaciosService: EspacioService
  ) {
    this.turno = new Turno();
    this.fecha = this.fecha = moment().locale('es').format('DD [de] MMMM');
    this.tokenInfo = this._storageService.localGet('tokenInfo');
  }

  ngOnInit() {

    this.clubService.obtenerClubesxId(this.tokenInfo.clubId).subscribe(c => {
      this.club = c;
      console.error(c);
    })

    const pTurnosInactivos = this._turnoService.obtenerTurnosInactivos().toPromise().then(data => {
      this.turnos = data;
      console.log('turnosInactivos', data);
      return data;
    });

    const pEspacio = this.espaciosService.getEspacio(this.route.snapshot.params.espacioId).toPromise().then((data: Espacio) => {
      this.espacio = data;
      console.log('espacio', data);
      return data;
    });

    const pConfigEspacio = this.espaciosService.getConfiguracionPorEspacio(this.route.snapshot.params.espacioId).toPromise().then((data: ConfiguracionDiasHs[]) => {
      this.configEspacio = data;
      console.log('configEspacio', data);
      return data;
    });


    Promise.all([pEspacio, pConfigEspacio, pTurnosInactivos]).then((data) => {
      this.configureScheduleRestrictions();
      this.createDaysOfMonth();
    }).catch((reason) => {
      console.error('turnos-espacio-config', reason);
    })

  }

  selectDay(num) {
    let d = this.days.find(p => p.number == num);
    if (d && !d.disabled) {
      this.deselectAllDays();
      this.deselectAllTimes();
      this.turno.horaDesde = null;
      this.turno.horaHasta = null;
      d.selected = true;
    }
    const syearmonth = moment(this.currentDate).locale('es').format('YYYY-MM');
    this.currentDate = moment(`${syearmonth}-${num}`, 'YYYY-MM-DD');
    this.daySelected = true;
    this.fecha = moment(this.currentDate).locale('es').format('DD [de] MMMM');

    // Recurrir al endpoint: pasarle el espacioId currentDate(forma)

    this.createTurnsOfDay(d);
  }

  selectTime(time) {
    let d = this.times.find(p => p.inicio == time.inicio && p.final == time.final);
    if (d) {
      this.deselectAllTimes();

      d.selected = true;
      this.turno.horaDesde = d.inicio;
      this.turno.horaHasta = d.final;
    }
    let obj = {
      fecha: moment(this.currentDate).format('YYYY-MM-DD'),
      desde: this.turno.horaDesde,
      hasta: this.turno.horaHasta,
      espacio: this.espacio,
      club: this.club
    }

    this._storageService.set('reservaActual', obj)
  }

  deselectAllDays() {
    this.days.forEach(d => {
      d.selected = false;
    })
  }

  deselectAllTimes() {
    this.times.forEach(t => {
      t.selected = false;
    })
  }

  prevMonth() {
    this.currentDate.subtract(1, 'month');
    this.currentMonth = moment(this.currentDate).locale('es').format('MMMM');
    this.createDaysOfMonth();
  }

  nextMonth() {
    this.currentDate.add(1, 'month');
    this.currentMonth = moment(this.currentDate).locale('es').format('MMMM');
    this.createDaysOfMonth();
  }

  cancelarTurno() {
    this.presentAlertConfirm();
  }

  getDate(f) {
    return moment(f).locale('es').format("DD [de] MMMM");
  }

  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Cancelar turno',
      message: 'Estás por cancelar el turno',
      buttons: [
        {
          text: 'VOLVER',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Operacion cancelada');
          }
        }, {
          text: 'SI CANCELAR',
          handler: () => {
            console.log('Operacion aceptada');
            //this.recuperar();
          }
        }
      ]
    });

    await alert.present();
  }

  seleccionaTurno(turno) {
    console.log(turno);
    this.turno = turno;
    this.turno.fecha = this.fecha;
  }

  turnoConfirmado() {

    this.router.navigate(['/menu/turnos/confirmar']);

  }

  changeTime(e) {
    console.log(this.turno, e);
  }

  private createTurnsOfDay(d) {

    const sdurationTurn = this.espacio.DuracionDeTurnos; 
    const sintervalTurn = this.espacio.intervaloEntreTurnos;


    const dtDurationTurn = moment(sdurationTurn, 'HH:mm:ss');
    const durationTurn = dtDurationTurn.hours() * 60 + dtDurationTurn.minutes();
    const dtIntervalTurn = moment(sintervalTurn, 'HH:mm:ss');
    const intervalTurn = dtIntervalTurn.hours() * 60 + dtIntervalTurn.minutes();


    if (durationTurn <= 0) {
      this.presentAlertErrorConfigurationaAgenda();
      return;
    }

    const iday = moment(this.currentDate).isoWeekday();

    const configEspacio = this.configEspacio.filter(p =>
      iday == 1 ? p.lunes === 1 : iday == 2 ? p.martes === 1 :
        iday == 3 ? p.miercoles === 1 : iday == 4 ? p.jueves === 1 :
          iday == 5 ? p.viernes === 1 : iday == 6 ? p.sabado === 1 :
            iday == 7 ? p.domingo === 1 : false);

    const durations = new List(configEspacio).Select(p => {
      const dti = moment(p.desde, 'HH:mm:ss');
      const durationi = dti.hours() * 60 + dti.minutes();
      const dtf = moment(p.hasta, 'HH:mm:ss');
      const durationf = dtf.hours() * 60 + dtf.minutes();
      return {
        desde: durationi,
        hasta: durationf
      };
    });

    this.times = [];

    this.getReservationsOfDay().then((reservations: any[]) => {
      const reservs = new List(reservations.map(p => {
        const pdti = p.desde.split(':');
        const pdtf = p.hasta.split(':');
        const mdti = (parseInt(pdti[0]) * 60 + parseInt(pdti[1]));
        const mdtf = (parseInt(pdtf[0]) * 60 + parseInt(pdtf[1]));
        return {
          from: mdti,
          to: mdtf
        }
      }));
      console.log('reservs', reservs);
      durations.ForEach(duration => {
        while ((duration.desde + durationTurn) <= duration.hasta) {

          if (!reservs.Any(p => p.from == duration.desde)) {
            console.log(reservs);
            const from = moment.utc(intervalTurn + duration.desde * 60 * 1000).format('HH:mm');
            const to = moment.utc((duration.desde + durationTurn) * 60 * 1000).format('HH:mm');
            const turn = {
              inicio: from,
              final: to,
              selected: false
            };
            this.times.push(turn);
          }

          duration.desde += durationTurn + intervalTurn;
        }
      });
    }).catch((err) => {
      console.error('getReservationsOfDay', err);
    });
  }

  private configureScheduleRestrictions() {
    if (!this.configEspacio) { return; }
    this.daysEnabledConfig[1] = this.configEspacio.find(p => p.lunes == 1) ? true : false;
    this.daysEnabledConfig[2] = this.configEspacio.find(p => p.martes == 1) ? true : false;
    this.daysEnabledConfig[3] = this.configEspacio.find(p => p.miercoles == 1) ? true : false;
    this.daysEnabledConfig[4] = this.configEspacio.find(p => p.jueves == 1) ? true : false;
    this.daysEnabledConfig[5] = this.configEspacio.find(p => p.viernes == 1) ? true : false;
    this.daysEnabledConfig[6] = this.configEspacio.find(p => p.sabado == 1) ? true : false;
    this.daysEnabledConfig[7] = this.configEspacio.find(p => p.domingo == 1) ? true : false;

  }

  private createDaysOfMonth() {
    const syearmonth = moment(this.currentDate).locale('es').format('YYYY-MM');
    const begin = moment(`${syearmonth}-01`, 'YYYY-MM-DD');
    const end = moment(`${begin.format("YYYY-MM-")}${begin.daysInMonth()}`, 'YYYY-MM-DD');
    this.days = [];
    while (begin.isBefore(end) || begin.isSame(end)) {
      const d = begin.format('DD');
      const ds = begin.locale('es').format('dddd');
      const dso = `${ds[0].toUpperCase()}${ds[1]}`;
      const enabled = this.dayIsEnabled(begin);
      const day = {
        shortName: dso,
        number: d,
        selected: false,
        disabled: !enabled,
        fullName: ds
      };
      if (this.thisMonthConstant == this.currentMonth) {
        if (this.isAfterToday(day)) {
          this.days.push(day)
        }
      } else {
        this.days.push(day)
      }

      begin.add(1, 'days');
    }
    console.log('finish');
  }

  isAfterToday(day) {
    let dOfMoth = moment().date();
    return dOfMoth <= +day.number;
  }

  private dayIsEnabled(mday: moment.Moment) {
    const iday = mday.isoWeekday();
    return this.daysEnabledConfig[iday];
  }

  private getReservationsOfDay() {
    const sday = this.currentDate.format('YYYY-MM-DD');
    return this.reservasService.getReservaByEspacioByFecha(this.route.snapshot.params.espacioId, sday).toPromise().then((data) => {
      console.log('reservas', data);
      return data;
    })
  }




  async presentAlertErrorConfigurationaAgenda() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Configuración de espacio incorrecta',
      message: 'Error en la configuración del espacio, contacte con el administrador del club',
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Operacion cancelada');
          }
        }
      ]
    });

    await alert.present();
  }


}
