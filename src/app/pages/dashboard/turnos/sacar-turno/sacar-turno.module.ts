import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SacarTurnoPageRoutingModule } from './sacar-turno-routing.module';

import { SacarTurnoPage } from './sacar-turno.page';
import { HeaderModule } from 'src/app/components/header/header.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SacarTurnoPageRoutingModule,
    HeaderModule
  ],
  declarations: [
    SacarTurnoPage
  ]
})
export class SacarTurnoPageModule {}
