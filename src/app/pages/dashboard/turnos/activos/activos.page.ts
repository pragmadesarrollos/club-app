import { Component, OnInit } from '@angular/core';
import { TurnoService } from 'src/app/services/turno.service';
import { Router } from '@angular/router';
import { EspacioService } from 'src/app/services/espacio.service';
import { Espacio } from 'src/app/models/ingresos.model';
import { AlertController, ToastController } from '@ionic/angular';
import { ReservaService } from 'src/app/services/reserva.service';
import { DisciplinaService } from 'src/app/services/disciplina.service';
import { StorageService } from 'src/app/services/storage.service';
import * as moment from 'moment';



@Component({
  selector: 'app-activos',
  templateUrl: './activos.page.html',
  styleUrls: ['./activos.page.scss'],
})
export class ActivosPage implements OnInit {
  titulo = 'Activos';
  activos;
  espacios: Espacio[];
  tokenInfo;

  constructor(
    public alertController: AlertController,
    private espacioService: EspacioService,
    private reservaService: ReservaService,
    private _turnoService: TurnoService,
    private _storageService: StorageService,
    public toastController: ToastController,
    private router: Router) { 
      this.tokenInfo = this._storageService.localGet('tokenInfo');
    }

  ngOnInit() {
    this.getReservas();
    
    this.espacioService.getEspacios().subscribe((esps: Espacio[]) => {
      this.espacios = esps;
    });

  }

  getReservas() {
    this.reservaService.getReservasActivasByUsuario(this.tokenInfo.clubId, this.tokenInfo.userId).subscribe(d => {
      console.error(d);
      this.activos = d;
    })
  }

  turnoConfirmado(id, espacioId){
    this.router.navigate(['/menu/turnos/confirmado/', id, espacioId ])
  }

  getEspacioById(id) {
    return this.espacios ? this.espacios.find(e => e.id == id) : {nombre: 'Cargando espacio..'};
  }

  cancelarTurno(turno) {
    this.presentAlertConfirm(turno);
  }

  async presentAlertConfirm(turno) {
    const alert = await this.alertController.create({
      header: 'Cancelar turno',
      message: '¿Estás seguro de cancelar este turno?',
      buttons: [
        {
          text: 'Volver',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            
          }
        }, {
          text: 'Sí, cancelar',
          handler: () => {
            this.sendCancelTurno(turno);
          }
        }
      ]
    });

    await alert.present();
  }

  sendCancelTurno(turno) {
    console.warn(turno);
    this._turnoService.cancelarTurno(turno.id).subscribe(d => {
      this.getReservas();
      this.mensaje('Turno cancelado con éxito', 'success');
    }, e => console.error(e));
  }

  async mensaje(mensaje: string, type?: string) {
    const toast = await this.toastController.create({
      message: mensaje,
      duration: 3000,
      position: 'top',
      color: type || 'dark',
      translucent: true,
      buttons: [
        {
          side: 'end',
          icon: 'close',
          handler: () => {
            toast.dismiss();
          }
        }
      ]
    });
    toast.present();
  }

  getFecha(f) {
    return moment(f).locale('es').format('DD [de] MMMM')
  }

  getHour(h) {
    return ('' + h).slice(0, h.length - 3);
  }

}
