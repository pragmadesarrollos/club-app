import { Component, OnInit } from '@angular/core';
import { Espacio } from 'src/app/models/ingresos.model';
import { EspacioService } from 'src/app/services/espacio.service';
import { ReservaService } from 'src/app/services/reserva.service';
import { StorageService } from 'src/app/services/storage.service';
import * as moment from 'moment';

@Component({
  selector: 'app-inactivos',
  templateUrl: './inactivos.page.html',
  styleUrls: ['./inactivos.page.scss'],
})
export class InactivosPage implements OnInit {
  titulo = 'Inactivos';
  inactivos;
  espacios: Espacio[];
  tokenInfo;

  constructor(private reservaService: ReservaService, private espacioService: EspacioService, private _storageService: StorageService) { 
    this.tokenInfo = this._storageService.localGet('tokenInfo');
  }

  ngOnInit() {
    this.getReservas();
    
    this.espacioService.getEspacios().subscribe((esps: Espacio[]) => {
      this.espacios = esps;
    });

  }

  getReservas() {
    this.reservaService.getReservasInactivasByUsuario(this.tokenInfo.userId).subscribe(d => {
      console.error(d);
      this.inactivos = d;
    })
  }

  turnoCancelado(id, espacioId){
    
  }

  getEspacioById(id) {
    return this.espacios ? this.espacios.find(e => e.id == id) : {nombre: 'Cargando espacio..'};
  }

  getFecha(f) {
    return moment(f).locale('es').format('DD [de] MMMM')
  }

  getHour(h) {
    return ('' + h).slice(0, h.length - 3);
  }


}
