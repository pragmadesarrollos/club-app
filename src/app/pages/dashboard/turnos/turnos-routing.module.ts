import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TurnosPage } from './turnos.page';

const routes: Routes = [
  {
    path: '',
    component: TurnosPage,
    children: [
      {
        path: 'activos',
        children: [
          {
            path: '',
            loadChildren: () => import('../turnos/activos/activos.module').then( m => m.ActivosPageModule)
          }
        ]
      },
      {
        path: 'inactivos',
        children: [
          {
            path: '',
            loadChildren: () => import('../turnos/inactivos/inactivos.module').then( m => m.InactivosPageModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: 'activos',
        pathMatch: 'full'
      }
     
    ],
  },
  {
    path: 'disciplinas',
    loadChildren: () => import('../disciplinas/disciplinas.module').then( m => m.DisciplinasPageModule)
  },
  {
    path: 'sacar-turno/:id/:espacioId',
    loadChildren: () => import('./sacar-turno/sacar-turno.module').then( m => m.SacarTurnoPageModule)
  },
  {
    path: 'confirmado/:id/:espacioId',
    loadChildren: () => import('./confirmado/confirmado.module').then( m => m.ConfirmadoPageModule)
  },
  {
    path: 'confirmar',
    loadChildren: () => import('./confirmar/confirmar.module').then( m => m.ConfirmarPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TurnosPageRoutingModule {}
