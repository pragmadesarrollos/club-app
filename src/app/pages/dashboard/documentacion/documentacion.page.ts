import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DocumentacionService} from 'src/app/services/documentacion.service';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-documentacion',
  templateUrl: './documentacion.page.html',
  styleUrls: ['./documentacion.page.scss'],
})
export class DocumentacionPage implements OnInit {
  titulo = 'Documentación';
  segmentSelected = 'sinaprobar';

  documentations = {
    aprobadas: [
      {
        "id": 6,
        "solicituddocumentoId": 6,
        "clubId": 135,
        "usuarioId": 1227,
        "documentacionId": 11,
        "estadodocumentacionId": 3,
        "solicituddocumento": {
          "id": 6,
          "titulo": "copia del dni",
          "descripcion": "copia del dni",
          "fecha": "2021-12-13",
          "hora": "11:59:55",
          "activo": 3,
          "enviadoporId": 945,
          "enviadopor": {
            "id": 945,
            "idFirebase": "lSvQKv7phXONvo2obCUIf48r1PM2",
            "idDevice": null,
            "ultimoIngreso": "135",
            "activo": 1,
            "personaId": 1585,
            "persona": {
              "id": 1585,
              "nombre": "Tito",
              "apellido": "Forns",
              "documento": "3205854632",
              "sexo": "masculino",
              "avatar": "https://api.klubo.club/api/image/z_NDdNieY.png",
              "correo": "titoforns@gmail.com",
              "telefono": "12345656562",
              "fechaNacimiento": "1987-05-10T22:43:11.908-00:00",
              "direccionPersonaId": 1535,
              "tipoDocumentId": 1
            }
          }
        }
      }
    ],
    pendientes: [
      {
        "id": 6,
        "solicituddocumentoId": 6,
        "clubId": 135,
        "usuarioId": 1227,
        "documentacionId": 11,
        "estadodocumentacionId": 1,
        "solicituddocumento": {
          "id": 6,
          "titulo": "copia del dni",
          "descripcion": "copia del dni",
          "fecha": "2021-12-13",
          "hora": "11:59:55",
          "activo": 1,
          "enviadoporId": 945,
          "enviadopor": {
            "id": 945,
            "idFirebase": "lSvQKv7phXONvo2obCUIf48r1PM2",
            "idDevice": null,
            "ultimoIngreso": "135",
            "activo": 1,
            "personaId": 1585,
            "persona": {
              "id": 1585,
              "nombre": "Tito",
              "apellido": "Forns",
              "documento": "3205854632",
              "sexo": "masculino",
              "avatar": "https://api.klubo.club/api/image/z_NDdNieY.png",
              "correo": "titoforns@gmail.com",
              "telefono": "12345656562",
              "fechaNacimiento": "1987-05-10T22:43:11.908-00:00",
              "direccionPersonaId": 1535,
              "tipoDocumentId": 1
            }
          }
        }
      },
      {
        "id": 6,
        "solicituddocumentoId": 6,
        "clubId": 135,
        "usuarioId": 1227,
        "documentacionId": 11,
        "estadodocumentacionId": 1,
        "solicituddocumento": {
          "id": 6,
          "titulo": "copia del dni",
          "descripcion": "copia del dni",
          "fecha": "2021-12-13",
          "hora": "11:59:55",
          "activo": 1,
          "enviadoporId": 945,
          "enviadopor": {
            "id": 945,
            "idFirebase": "lSvQKv7phXONvo2obCUIf48r1PM2",
            "idDevice": null,
            "ultimoIngreso": "135",
            "activo": 1,
            "personaId": 1585,
            "persona": {
              "id": 1585,
              "nombre": "Tito",
              "apellido": "Forns",
              "documento": "3205854632",
              "sexo": "masculino",
              "avatar": "https://api.klubo.club/api/image/z_NDdNieY.png",
              "correo": "titoforns@gmail.com",
              "telefono": "12345656562",
              "fechaNacimiento": "1987-05-10T22:43:11.908-00:00",
              "direccionPersonaId": 1535,
              "tipoDocumentId": 1
            }
          }
        }
      },
      {
        "id": 6,
        "solicituddocumentoId": 6,
        "clubId": 135,
        "usuarioId": 1227,
        "documentacionId": 11,
        "estadodocumentacionId": 1,
        "solicituddocumento": {
          "id": 6,
          "titulo": "copia del dni",
          "descripcion": "copia del dni",
          "fecha": "2021-12-13",
          "hora": "11:59:55",
          "activo": 1,
          "enviadoporId": 945,
          "enviadopor": {
            "id": 945,
            "idFirebase": "lSvQKv7phXONvo2obCUIf48r1PM2",
            "idDevice": null,
            "ultimoIngreso": "135",
            "activo": 1,
            "personaId": 1585,
            "persona": {
              "id": 1585,
              "nombre": "Tito",
              "apellido": "Forns",
              "documento": "3205854632",
              "sexo": "masculino",
              "avatar": "https://api.klubo.club/api/image/z_NDdNieY.png",
              "correo": "titoforns@gmail.com",
              "telefono": "12345656562",
              "fechaNacimiento": "1987-05-10T22:43:11.908-00:00",
              "direccionPersonaId": 1535,
              "tipoDocumentId": 1
            }
          }
        }
      },
      {
        "id": 6,
        "solicituddocumentoId": 6,
        "clubId": 135,
        "usuarioId": 1227,
        "documentacionId": 11,
        "estadodocumentacionId": 1,
        "solicituddocumento": {
          "id": 6,
          "titulo": "copia del dni",
          "descripcion": "copia del dni",
          "fecha": "2021-12-13",
          "hora": "11:59:55",
          "activo": 1,
          "enviadoporId": 945,
          "enviadopor": {
            "id": 945,
            "idFirebase": "lSvQKv7phXONvo2obCUIf48r1PM2",
            "idDevice": null,
            "ultimoIngreso": "135",
            "activo": 1,
            "personaId": 1585,
            "persona": {
              "id": 1585,
              "nombre": "Tito",
              "apellido": "Forns",
              "documento": "3205854632",
              "sexo": "masculino",
              "avatar": "https://api.klubo.club/api/image/z_NDdNieY.png",
              "correo": "titoforns@gmail.com",
              "telefono": "12345656562",
              "fechaNacimiento": "1987-05-10T22:43:11.908-00:00",
              "direccionPersonaId": 1535,
              "tipoDocumentId": 1
            }
          }
        }
      }
    ],
    revision: [
      {
        "id": 6,
        "solicituddocumentoId": 6,
        "clubId": 135,
        "usuarioId": 1227,
        "documentacionId": 2,
        "estadodocumentacionId": 2,
        "solicituddocumento": {
          "id": 6,
          "titulo": "copia del dni",
          "descripcion": "copia del dni",
          "fecha": "2021-12-13",
          "hora": "11:59:55",
          "activo": 2,
          "enviadoporId": 945,
          "enviadopor": {
            "id": 945,
            "idFirebase": "lSvQKv7phXONvo2obCUIf48r1PM2",
            "idDevice": null,
            "ultimoIngreso": "135",
            "activo": 1,
            "personaId": 1585,
            "persona": {
              "id": 1585,
              "nombre": "Tito",
              "apellido": "Forns",
              "documento": "3205854632",
              "sexo": "masculino",
              "avatar": "https://api.klubo.club/api/image/z_NDdNieY.png",
              "correo": "titoforns@gmail.com",
              "telefono": "12345656562",
              "fechaNacimiento": "1987-05-10T22:43:11.908-00:00",
              "direccionPersonaId": 1535,
              "tipoDocumentId": 1
            }
          }
        }
      },
      {
        "id": 6,
        "solicituddocumentoId": 6,
        "clubId": 135,
        "usuarioId": 1227,
        "documentacionId": 2,
        "estadodocumentacionId": 2,
        "solicituddocumento": {
          "id": 6,
          "titulo": "copia del dni",
          "descripcion": "copia del dni",
          "fecha": "2021-12-13",
          "hora": "11:59:55",
          "activo": 2,
          "enviadoporId": 945,
          "enviadopor": {
            "id": 945,
            "idFirebase": "lSvQKv7phXONvo2obCUIf48r1PM2",
            "idDevice": null,
            "ultimoIngreso": "135",
            "activo": 1,
            "personaId": 1585,
            "persona": {
              "id": 1585,
              "nombre": "Tito",
              "apellido": "Forns",
              "documento": "3205854632",
              "sexo": "masculino",
              "avatar": "https://api.klubo.club/api/image/z_NDdNieY.png",
              "correo": "titoforns@gmail.com",
              "telefono": "12345656562",
              "fechaNacimiento": "1987-05-10T22:43:11.908-00:00",
              "direccionPersonaId": 1535,
              "tipoDocumentId": 1
            }
          }
        }
      },
      {
        "id": 6,
        "solicituddocumentoId": 6,
        "clubId": 135,
        "usuarioId": 1227,
        "documentacionId": 2,
        "estadodocumentacionId": 2,
        "solicituddocumento": {
          "id": 6,
          "titulo": "copia del dni",
          "descripcion": "copia del dni",
          "fecha": "2021-12-13",
          "hora": "11:59:55",
          "activo": 2,
          "enviadoporId": 945,
          "enviadopor": {
            "id": 945,
            "idFirebase": "lSvQKv7phXONvo2obCUIf48r1PM2",
            "idDevice": null,
            "ultimoIngreso": "135",
            "activo": 1,
            "personaId": 1585,
            "persona": {
              "id": 1585,
              "nombre": "Tito",
              "apellido": "Forns",
              "documento": "3205854632",
              "sexo": "masculino",
              "avatar": "https://api.klubo.club/api/image/z_NDdNieY.png",
              "correo": "titoforns@gmail.com",
              "telefono": "12345656562",
              "fechaNacimiento": "1987-05-10T22:43:11.908-00:00",
              "direccionPersonaId": 1535,
              "tipoDocumentId": 1
            }
          }
        }
      },
      {
        "id": 6,
        "solicituddocumentoId": 6,
        "clubId": 135,
        "usuarioId": 1227,
        "documentacionId": 2,
        "estadodocumentacionId": 2,
        "solicituddocumento": {
          "id": 6,
          "titulo": "copia del dni",
          "descripcion": "copia del dni",
          "fecha": "2021-12-13",
          "hora": "11:59:55",
          "activo": 2,
          "enviadoporId": 945,
          "enviadopor": {
            "id": 945,
            "idFirebase": "lSvQKv7phXONvo2obCUIf48r1PM2",
            "idDevice": null,
            "ultimoIngreso": "135",
            "activo": 1,
            "personaId": 1585,
            "persona": {
              "id": 1585,
              "nombre": "Tito",
              "apellido": "Forns",
              "documento": "3205854632",
              "sexo": "masculino",
              "avatar": "https://api.klubo.club/api/image/z_NDdNieY.png",
              "correo": "titoforns@gmail.com",
              "telefono": "12345656562",
              "fechaNacimiento": "1987-05-10T22:43:11.908-00:00",
              "direccionPersonaId": 1535,
              "tipoDocumentId": 1
            }
          }
        }
      },
      {
        "id": 6,
        "solicituddocumentoId": 6,
        "clubId": 135,
        "usuarioId": 1227,
        "documentacionId": 2,
        "estadodocumentacionId": 2,
        "solicituddocumento": {
          "id": 6,
          "titulo": "copia del dni",
          "descripcion": "copia del dni",
          "fecha": "2021-12-13",
          "hora": "11:59:55",
          "activo": 2,
          "enviadoporId": 945,
          "enviadopor": {
            "id": 945,
            "idFirebase": "lSvQKv7phXONvo2obCUIf48r1PM2",
            "idDevice": null,
            "ultimoIngreso": "135",
            "activo": 1,
            "personaId": 1585,
            "persona": {
              "id": 1585,
              "nombre": "Tito",
              "apellido": "Forns",
              "documento": "3205854632",
              "sexo": "masculino",
              "avatar": "https://api.klubo.club/api/image/z_NDdNieY.png",
              "correo": "titoforns@gmail.com",
              "telefono": "12345656562",
              "fechaNacimiento": "1987-05-10T22:43:11.908-00:00",
              "direccionPersonaId": 1535,
              "tipoDocumentId": 1
            }
          }
        }
      },
      {
        "id": 6,
        "solicituddocumentoId": 6,
        "clubId": 135,
        "usuarioId": 1227,
        "documentacionId": 2,
        "estadodocumentacionId": 2,
        "solicituddocumento": {
          "id": 6,
          "titulo": "copia del dni",
          "descripcion": "copia del dni",
          "fecha": "2021-12-13",
          "hora": "11:59:55",
          "activo": 2,
          "enviadoporId": 945,
          "enviadopor": {
            "id": 945,
            "idFirebase": "lSvQKv7phXONvo2obCUIf48r1PM2",
            "idDevice": null,
            "ultimoIngreso": "135",
            "activo": 1,
            "personaId": 1585,
            "persona": {
              "id": 1585,
              "nombre": "Tito",
              "apellido": "Forns",
              "documento": "3205854632",
              "sexo": "masculino",
              "avatar": "https://api.klubo.club/api/image/z_NDdNieY.png",
              "correo": "titoforns@gmail.com",
              "telefono": "12345656562",
              "fechaNacimiento": "1987-05-10T22:43:11.908-00:00",
              "direccionPersonaId": 1535,
              "tipoDocumentId": 1
            }
          }
        }
      }
    ]
  }

  documentacionPendiente;
  documentacionEnRevision;
  documentacionAprobada;
  documentacionEnviada;
  documentacionEnviadaAux;
  documentacionPendienteAux;
  documentacionEnRevisionAux;
  documentacionAprobadaAux;
  isManager = false;
  pendingSearchStr = '';

  constructor(private _documentacionService: DocumentacionService,
    private router: Router,
    private _storageService: StorageService) { }

  ngOnInit() {
    

  
  }

  ionViewWillEnter() {
    this.loadData();
  }

  loadData() {
    if(this.segmentSelected == 'sinaprobar') {
      let userInfo = this._storageService.localGet('userInfo');
      this.isManager = userInfo.rol == 'manager';

      const tokenInfo = this._storageService.localGet('tokenInfo');
    
      this._documentacionService.obtenerDocumentacionPendiente(this.isManager).subscribe(data => {
        this.documentacionPendiente = data;
        this.documentacionPendienteAux = data;
        console.error('Pendientes: ', data)
        //this.dismissLoading();
  
      }, error => {
        //this.dismissLoading();
        console.log(error);
        //this.mensaje('Opss.. ocurrio un error');
      })

      this._documentacionService.obtenerDocumentacionEnRevision(this.isManager).subscribe(data => {
        this.documentacionEnRevision = data;
        this.documentacionEnRevisionAux = data;
        console.error('Revision: ', data)
        //this.dismissLoading();
  
      }, error => {
        //this.dismissLoading();
        console.log(error);
        //this.mensaje('Opss.. ocurrio un error');
      });
    } else if(this.segmentSelected == 'aprobados') {
      if(this.isManager) {
        this._documentacionService.obtenerDocumentacionEnviada().subscribe(data => {
          this.documentacionAprobada = data;
          console.error('Aprobados: ', data)
          this.documentacionAprobadaAux = data;
          //this.dismissLoading();
     
        }, error => {
          //this.dismissLoading();
          console.log(error);
          //this.mensaje('Opss.. ocurrio un error');
        });
      } else {
        this._documentacionService.obtenerDocumentacionAprobada(false).subscribe(data => {
          this.documentacionAprobada = data;
          console.error('Aprobados: ', data)
          this.documentacionAprobadaAux = data;
          //this.dismissLoading();
     
        }, error => {
          //this.dismissLoading();
          console.log(error);
          //this.mensaje('Opss.. ocurrio un error');
        });
        
      }
    } else if(this.segmentSelected == 'enviadas') {
      if(this.isManager) {
        this._documentacionService.obtenerDocumentacionEnviada().subscribe(data => {
          this.documentacionEnviada = data;
          this.documentacionEnviadaAux = data;
          console.error('Enviados: ', data)
          //this.dismissLoading();
     
        }, error => {
          //this.dismissLoading();
          console.log(error);
          //this.mensaje('Opss.. ocurrio un error');
        })
      }
    }
  }

  filterApproveds(e) {
    let strSearch = e.detail.value;
    this.documentacionAprobada = this.documentacionAprobadaAux.filter(p => this.filterApprovedItem(p, strSearch));
  }

  filterApprovedItem(p, strSearch) {
    let s = strSearch.toLowerCase();
    let title = p.titulo ? p.titulo.toLowerCase() : '';
    let desc = p.descripcion ? p.descripcion.toLowerCase() : '';
    let nombre = this.isManager ? (p.enviadoA ? p.enviadoA.toLowerCase() : '') : (p.enviadopor ? p.enviadopor.toLowerCase() : '');
    return title.indexOf(s) > -1 || 
    desc.indexOf(s) > -1 || 
    nombre.indexOf(s) > -1 
  }

  filterSendeds(e) {
    let strSearch = e.detail.value;
    this.documentacionEnviada = this.documentacionEnviadaAux.filter(p => this.filterSendedItem(p, strSearch));
  }

  filterSendedItem(p, strSearch) {
    let s = strSearch.toLowerCase();
    let title = p.titulo ? p.titulo.toLowerCase() : '';
    let desc = p.descripcion ? p.descripcion.toLowerCase() : '';
    let nombre = p.enviadoA ? p.enviadoA.toLowerCase() : '';
   
    return title.indexOf(s) > -1 || 
    desc.indexOf(s) > -1 || 
    nombre.indexOf(s) > -1 
  }

  filterPendings(e) {
    let strSearch = e.detail.value;
    this.documentacionEnRevision = this.documentacionEnRevisionAux.filter(p => this.isManager ? this.filterItemOfManager(p, strSearch) : this.filterItem(p, strSearch));
    this.documentacionPendiente = this.documentacionPendienteAux.filter(p => this.isManager ? this.filterItemOfManager(p, strSearch) : this.filterItem(p, strSearch));
  }

  filterItem(p, strSearch) {
    let s = strSearch.toLowerCase();
    let title = p.solicituddocumento.titulo ? p.solicituddocumento.titulo.toLowerCase() : '';
    let desc = p.solicituddocumento.descripcion ? p.solicituddocumento.descripcion.toLowerCase() : '';
    let nombre = p.solicituddocumento.enviadopor.persona.nombre ? p.solicituddocumento.enviadopor.persona.nombre.toLowerCase() : '';
    let apellido = p.solicituddocumento.enviadopor.persona.apellido ? p.solicituddocumento.enviadopor.persona.apellido.toLowerCase() : '';
    let nya = (nombre + ' ' + apellido).toLowerCase();
    return title.indexOf(s) > -1 || 
    desc.indexOf(s) > -1 || 
    nombre.indexOf(s) > -1 || 
    apellido.indexOf(strSearch) > -1 ||
    nya.indexOf(strSearch) > -1
  }

  filterItemOfManager(p, strSearch) {
    let s = strSearch.toLowerCase();
    let title = p.titulo ? p.titulo.toLowerCase() : '';
    let desc = p.descripcion ? p.descripcion.toLowerCase() : '';
    let nombre = p.enviadoA ? p.enviadoA.toLowerCase() : '';
    return title.indexOf(s) > -1 || 
    desc.indexOf(s) > -1 || 
    nombre.indexOf(s) > -1 
  }

  segmentChanged(e) {  
    this.segmentSelected = e.detail.value;
    this.loadData();
  }

  gotoPendiente(item) {
    this.router.navigate(['/menu/documentacion/sinaprobar', item.usuarioId, item.solicituddocumentoId ]);
  }

  gotoRevision(item) {
    this.router.navigate(['/menu/documentacion/sinaprobar', item.usuarioId, item.solicituddocumentoId]);
  }

  addDocumentacion(e) {
    this.router.navigate(['/menu/documentacion/crear-documentacion'])
  }

  gotoAprobado(item) {
    if(this.isManager) {
      this.router.navigate(['/menu/documentacion/aprobados', item.solicituddocumentoId])
    } else {
      let tokenInfo = JSON.parse(localStorage.getItem('tokenInfo'));
      this.router.navigate(['/menu/documentacion/aprobados/aprobado', tokenInfo.userId, item.solicituddocumentoId])
    }
    
  }

  gotoEnviada(item) {
    this.router.navigate(['/menu/documentacion/documentacion', item.solicituddocumentoId]);
  }
  

}
