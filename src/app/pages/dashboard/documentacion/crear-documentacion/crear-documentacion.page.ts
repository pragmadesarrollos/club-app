import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { DocumentacionService } from 'src/app/services/documentacion.service';
import { NotificacionService } from 'src/app/services/notificacion.service';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-crear-documentacion',
  templateUrl: './crear-documentacion.page.html',
  styleUrls: ['./crear-documentacion.page.scss'],
})
export class CrearDocumentacionPage implements OnInit {
  titulo = 'Crear documentacion';
  form: FormGroup;
  loading: HTMLIonLoadingElement;
  esSocio = false;
  categorias;


  constructor(private router: Router, private fb: FormBuilder,
    public loadingController: LoadingController,
    public toastController: ToastController,
    public usuarioService: UsuarioService,
    public documentacionService: DocumentacionService) {

    this.esSocio = usuarioService.rol === 'socio';

    let groups = {
      titulo: ['', Validators.required],
      categoria: ['', Validators.required],
      descripcionL: ['', Validators.required]
    }

    if (this.esSocio) {
      groups['contenido'] = [''];
    }

    this.form = this.fb.group(groups);
  }

  ngOnInit() {
    this.documentacionService.obtenerCategorias().subscribe(cs => {
      console.warn(cs);
      this.categorias = cs;
    })
  }

  siguiente() {

    this.documentacionService.titulo = this.form.value.titulo;
    this.documentacionService.descripcionL = this.form.value.descripcionL;
    this.documentacionService.categoria = this.form.value.categoria;
    this.router.navigate(['/menu/documentacion/crear-documentacion/destinatarios']);

  }

  presentLoading() {
    if (this.loading) {
      this.loading.dismiss();
    }
    return new Promise((resolve) => {
      resolve(this.loadingController.create({
        message: 'Espere...'
      }));
    })
  }

  async dismissLoading(): Promise<void> {
    if (this.loading) {
      this.loading.dismiss();
    }
  }

  async mensaje(mensaje: string) {
    const toast = await this.toastController.create({
      message: mensaje,
      color: 'dark',
      duration: 3000
    });
    toast.present();
  }

}
