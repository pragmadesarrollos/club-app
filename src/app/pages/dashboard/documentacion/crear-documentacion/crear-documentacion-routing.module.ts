import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CrearDocumentacionPage } from './crear-documentacion.page';

const routes: Routes = [
  {
    path: '',
    component: CrearDocumentacionPage
  },
  
  {
    path: 'destinatarios',
    loadChildren: () => import('./destinatarios/destinatarios.module').then( m => m.DestinatariosPageModule)
  },

  
  {
    path: 'enviar',
    loadChildren: () => import('./enviar/enviar.module').then( m => m.EnviarPageModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CrearDocumentacionPageRoutingModule {}
