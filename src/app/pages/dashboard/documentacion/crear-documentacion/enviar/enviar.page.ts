import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, LoadingController, ToastController } from '@ionic/angular';
import { DocumentacionService } from 'src/app/services/documentacion.service';
import { EncuestaService } from 'src/app/services/encuesta.service';
import { NotificacionService } from 'src/app/services/notificacion.service';

@Component({
  selector: 'app-enviar',
  templateUrl: './enviar.page.html',
  styleUrls: ['./enviar.page.scss'],
})
export class EnviarPage implements OnInit {
  titulo = 'Enviar'
  loading: HTMLIonLoadingElement;
  destinatarios = [];
  categoriaSelected;

  newDocumentation = {
    "solicitud": {
      "categoria": 1,
      "titulo": "copia del dni",
      "descripcion": "copia del dni"
    },
    "usuarios": []
  }

  constructor(private router: Router,
    public loadingController: LoadingController,
    public toastController: ToastController,
    public documentacionService: DocumentacionService,
    public alertController: AlertController) {

    this.newDocumentation.solicitud.titulo = this.documentacionService.titulo;
    this.newDocumentation.solicitud.descripcion = this.documentacionService.descripcionL;
    this.newDocumentation.solicitud.categoria = this.documentacionService.categoria.id;
    this.destinatarios = this.documentacionService.destinatarios;
    this.categoriaSelected = this.documentacionService.categoria;
  }

  ngOnInit() {


  }


  siguiente() {
    
    this.newDocumentation.usuarios = this.destinatarios;
    console.error(this.destinatarios);
    
    this.presentLoading().then((loadRes: any) => {
      this.loading = loadRes
      this.loading.present();
      this.documentacionService.enviarSolicitud(this.newDocumentation).subscribe(async data => {
        console.log(data);
        this.dismissLoading();
        this.router.navigate(['/menu']);

        const alert = await this.alertController.create({
          header: 'Solicitud de documentacion',
          message: 'Enviada correctamente.',
          buttons: ['OK']
        });
        await alert.present();
      }, error => {
        console.log(error);
        this.dismissLoading();
        this.mensaje('Opss.. ocurrio un error');
      });
    });
  }



  presentLoading() {
    if (this.loading) {
      this.loading.dismiss();
    }
    return new Promise((resolve) => {
      resolve(this.loadingController.create({
        message: 'Espere...'
      }));
    })
  }

  async dismissLoading(): Promise<void> {
    if (this.loading) {
      this.loading.dismiss();
    }
  }

  async mensaje(mensaje: string) {
    const toast = await this.toastController.create({
      message: mensaje,
      color: 'dark',
      duration: 3000
    });
    toast.present();
  }


}
