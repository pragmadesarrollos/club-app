import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CrearDocumentacionPageRoutingModule } from './crear-documentacion-routing.module';

import { CrearDocumentacionPage } from './crear-documentacion.page';
import { HeaderComponent } from 'src/app/components/header/header.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CrearDocumentacionPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [
    CrearDocumentacionPage,
    HeaderComponent
  ]
})
export class CrearDocumentacionPageModule { }
