import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DocumentacionService } from 'src/app/services/documentacion.service';
import * as moment from 'moment';
import { IonInput, LoadingController, NavController } from '@ionic/angular';
import { FormGroup, FormBuilder } from '@angular/forms';
import * as $ from 'jquery';
import { AlertController } from '@ionic/angular';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-aprobado',
  templateUrl: './aprobado.page.html',
  styleUrls: ['./aprobado.page.scss'],
})
export class AprobadoPage implements OnInit {
  titulo = 'SIN APROBAR';
  solicitud;
  @ViewChild('inputDoc') inputDoc: IonInput;
  uploadForm: FormGroup; 
  idSolicitud;
  isManager = false;
  idUser;

  constructor(private _storageService: StorageService, private navCtrl: NavController, public loadingController: LoadingController, public alertController: AlertController, private route: ActivatedRoute, private formBuilder: FormBuilder, private docService: DocumentacionService) { 
    
  }

  ngOnInit() {
    let userInfo = this._storageService.localGet('userInfo');
    this.isManager = userInfo.rol == 'manager';
    this.idSolicitud = this.route.snapshot.params.id;
    this.idUser = this.route.snapshot.params.userId;
    console.log(this.idSolicitud)
    this.loadData();
    this.uploadForm = this.formBuilder.group({
      documentos: ['']
    });
  }

  loadData() {
    this.docService.obtenerDocumentacionxId(this.idSolicitud, this.idUser).subscribe(d => {
      console.error(d);
      this.solicitud = d;
    })
  }

  getFecha(f) {
    return moment(f).locale('es').format('DD [de] MMMM')
  }

  addDoc() {
    console.log(this.inputDoc)
    $('#inputDoc').click();
  }

  onFileSelect(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      console.error(file);
      if(file) {
        if(+file.size >= 5000000) {
          this.presentAlert('Error', 'Archivo demasiado grande', 'El documento que estas queriendo cargar es demasiado grande. (limite: 5MB)', null);
        } else {
          this.uploadForm.get('documentos').setValue(file);
          this.onSubmit();
        }
      }
    }
  }

  async presentAlert(h, s, m, func) {
    const alert = await this.alertController.create({
      header: h || '',
      subHeader: s || '',
      message: m || '',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            if(func) {
              func();
            }
          }
        }
      ]
    });

    await alert.present();

    const { role } = await alert.onDidDismiss();
    
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      message: 'Cargando archivo...'
    });
    await loading.present();
  }

  hideLoading() {
    this.loadingController.dismiss();
  }

  async onSubmit() {

    this.presentLoading();
    let token = localStorage.getItem('token');
    let tokenInfo = JSON.parse(localStorage.getItem('tokenInfo'));

    const formData = new FormData();
    formData.append('documento', this.uploadForm.get('documentos').value);

    var myHeaders = new Headers();
    myHeaders.append("Accept", "application/json");
    myHeaders.append("Authorization", "Bearer " + token)


   

          var requestOptions = {
                    method: 'POST',
                    headers: myHeaders,
                    body: formData
                  };
    const result = await fetch('https://api.klubo.club/api/documentacion/upload/' + this.idSolicitud + '/' + tokenInfo.userId, requestOptions)
    this.docService.obtenerDocumentacionxId(this.idSolicitud, this.idUser).subscribe(d => {
      console.error(d);
      this.solicitud = d;
      this.hideLoading();
    })
  }

  typeOfDoc(item) {
    let end = item.split('.')[1];
    if(end == 'pdf') {
      return 'pdf';
    } else if (end == 'jpg' || end == 'jpeg' || end == 'svg' || end == 'png') {
      return 'image';
    } else {
      return 'text';
    }
    
  }

  downloadFile(url) {
    window.open(url, "_blank");
  }

  deleteFile(doc) {
    console.warn(doc);
    this.docService.eliminarDocumento(doc.id).subscribe(d => {
      this.presentAlert(null, 'Documento eliminado', 'El documento ' + doc.titulo + ' ha sido eliminado correctamente', this.loadData());
    }, e => {
      this.presentAlert('Error!', null, 'El documento ' + doc.titulo + ' no ha podido ser elimiminado: ' + e, null);
    })
  }

  confirmDeleteFile(doc) {
    this.presentAlertConfirm(doc)
  }

  async presentAlertConfirm(doc) {
    const self = this;
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Eliminar',
      message: 'Confirma que quieres eliminar el documento: ' + doc.titulo + '?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            
          }
        }, {
          text: 'Si, eliminar',
          handler: () => {
            self.deleteFile(doc)
          }
        }
      ]
    });

    await alert.present();
  }

  siguiente() {
    this.navCtrl.back();
  }

  refuse() {
    this.docService.rechazarDocumentacion(this.idSolicitud, this.solicitud.usuarioID).subscribe(d => {
      this.presentAlert(null, 'Documentacion rechazada', 'La solicitud de documentacion ha sido rechazada correctamente', this.navCtrl.back());
    }, e => {
      this.presentAlert('Error!', null, 'La solicitud de documentacion no ha podido ser rechazada: ' + e, null);
    })
  }

  accept() {
    this.docService.aprobarDocumentacion(this.idSolicitud, this.solicitud.usuarioID).subscribe(d => {
      this.presentAlert(null, 'Documentacion aprobada!', 'La solicitud de documentacion ha sido aprobada correctamente', this.navCtrl.back());
    }, e => {
      this.presentAlert('Error!', null, 'La solicitud de documentacion no ha podido ser aprobada: ' + e, null);
    })
  }

}
