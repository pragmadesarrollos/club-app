import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AprobadoPage } from './aprobado.page';
import { HeaderComponent } from 'src/app/components/header/header.component';
import { AprobadoPageRoutingModule } from './aprobado-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AprobadoPageRoutingModule
  ],
  declarations: [
    AprobadoPage,
    HeaderComponent
  ]
})
export class AprobadoPageModule {}
