import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DocumentacionService} from 'src/app/services/documentacion.service';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-aprobados',
  templateUrl: './aprobados.page.html',
  styleUrls: ['./aprobados.page.scss'],
})
export class AprobadosPage implements OnInit {
  titulo = 'Aprobados';
  isManager = false;
  idSolicitud;
  documentacionAprobada;
  documentacionAprobadaAux;

  constructor(private router: Router, private docService: DocumentacionService, private _storageService: StorageService, private route: ActivatedRoute) { }

  ngOnInit() {
    let userInfo = this._storageService.localGet('userInfo');
    this.isManager = userInfo.rol == 'manager';
    this.idSolicitud = this.route.snapshot.params.id;
    this.loadData();
   
  }

  loadData() {
    this.docService.obtenerDocumentacionEnviadaAprobada(this.idSolicitud).subscribe(d => {
      console.warn(d);
      this.documentacionAprobada = d;
      this.documentacionAprobadaAux = d;
    });

  }

  gotoSolicitud(item) {
    this.router.navigate(['/menu/documentacion/aprobados/aprobado', item.usuarioId, item.solicituddocumentoId]);
  }

  filterApproveds(e) {
    let strSearch = e.detail.value;
    this.documentacionAprobada = this.documentacionAprobadaAux.filter(p => this.filterApprovedItem(p, strSearch));
  }

  filterApprovedItem(p, strSearch) {
    if(p.estadodocumentacionId != 3 ) {
      return false;
    }
    let s = strSearch.toLowerCase();
    let title = p.solicituddocumento.titulo ? p.solicituddocumento.titulo.toLowerCase() : '';
    let desc = p.solicituddocumento.descripcion ? p.solicituddocumento.descripcion.toLowerCase() : '';
    let nombre = p.usuario.persona.nombre ? p.usuario.persona.nombre.toLowerCase() : '';
    let apellido = p.usuario.persona.apellido ? p.usuario.persona.apellido.toLowerCase() : '';
    let nya = (nombre + ' ' + apellido).toLowerCase();
    return title.indexOf(s) > -1 || 
    desc.indexOf(s) > -1 || 
    nombre.indexOf(s) > -1 || 
    apellido.indexOf(strSearch) > -1 ||
    nya.indexOf(strSearch) > -1
  }

}
