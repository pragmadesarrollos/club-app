import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AprobadosPage } from './aprobados.page';

const routes: Routes = [
  {
    path: '',
    component: AprobadosPage,
  },
  {
    path: 'aprobado/:id',
    loadChildren: () => import('./aprobado/aprobado.module').then( m => m.AprobadoPageModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AprobadosPageRoutingModule {}
