import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DocumentacionService } from 'src/app/services/documentacion.service';


@Component({
  selector: 'app-documentacino',
  templateUrl: './documentacion.page.html',
  styleUrls: ['./documentacion.page.scss'],
})
export class DocumentacionPage implements OnInit {
  titulo = 'Documentacion';
  idSolicitud;
  solicitudes;
  solicitudesAux;

  constructor(private route: ActivatedRoute, private router: Router, private docService: DocumentacionService) { }

  ngOnInit() {
    this.idSolicitud = this.route.snapshot.params.id;
    this.docService.obtenerUsuariosPorSolicitud(this.idSolicitud).subscribe(d => {
      console.warn(d);
      this.solicitudes = d;
      this.solicitudesAux = d;
    }, e => console.error(e))

  }

  filterItems(e) {
    let strSearch = e.detail.value;
    this.solicitudes = this.solicitudesAux.filter(p => this.filterItem(p, strSearch));
  }

  filterItem(p, strSearch) {
    let s = strSearch.toLowerCase();
    let dni = p.usuario.persona.documento ? p.usuario.persona.documento.toLowerCase() : '';
    let nombre = p.usuario.persona.nombre ? p.usuario.persona.nombre.toLowerCase() : '';
    let apellido = p.usuario.persona.apellido ? p.usuario.persona.apellido.toLowerCase() : '';
    let nya = (nombre + ' ' + apellido).toLowerCase();
    let estado = p.estadodocumentacionId == 3 ? 'aprobado' : (p.estadodocumentacionId == 1 ? 'pendiente' : 'en revision');
    return dni.indexOf(s) > -1 || 
    estado.indexOf(s) > -1 || 
    nombre.indexOf(s) > -1 || 
    apellido.indexOf(strSearch) > -1 ||
    nya.indexOf(strSearch) > -1
  }

  gotoSolicitud(item) {
    if(item.estadodocumentacionId == 3) {
      this.gotoAprobado(item);
    } else {
      this.gotoNoAprobado(item);
    }
  }

  gotoNoAprobado(item) {
    this.router.navigate(['/menu/documentacion/sinaprobar', item.usuarioId, item.solicituddocumentoId]);
  }

  gotoAprobado(item) {
    this.router.navigate(['/menu/documentacion/aprobados/aprobado', item.usuarioId, item.solicituddocumentoId])
  }

}
