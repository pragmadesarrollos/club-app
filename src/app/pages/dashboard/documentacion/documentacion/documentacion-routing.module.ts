import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DocumentacionPage } from './documentacion.page';
import { DocumentoPage } from './documento/documento.page';

const routes: Routes = [
  {
    path: '',
    component: DocumentacionPage,
  },
  {
    path: '/documento',
    component: DocumentoPage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DocumentacionPageRoutingModule {}
