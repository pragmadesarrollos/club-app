import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DocumentacionPage } from './documentacion.page';

const routes: Routes = [
  {
    path: '',
    component: DocumentacionPage
  }
  ,
  {
    path: 'documentacion/:id',
    loadChildren: () => import('./documentacion/documentacion.module').then( m => m.DocumentacionPageModule)
  },
  {
    path: 'sinaprobar/:userId/:id',
    loadChildren: () => import('./sinaprobar/sinaprobar.module').then( m => m.SinAprobarPageModule)
  },
  {
    path: 'crear-documentacion',
    loadChildren: () => import('./crear-documentacion/crear-documentacion.module').then( m => m.CrearDocumentacionPageModule)
  },
  {
    path: 'crear-documentacion/destinatarios',
    loadChildren: () => import('./crear-documentacion/destinatarios/destinatarios.module').then( m => m.DestinatariosPageModule)
  },
  {
    path: 'aprobados/:id',
    loadChildren: () => import('./aprobados/aprobados.module').then( m => m.AprobadosPageModule)
  },
  {
    path: 'aprobados/aprobado/:userId/:id',
    loadChildren: () => import('./aprobados/aprobado/aprobado.module').then( m => m.AprobadoPageModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DocumentacionPageRoutingModule {}
