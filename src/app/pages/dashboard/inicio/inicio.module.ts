import { ToggleInicioComponent } from './../../../components/toggle-inicio/toggle-inicio.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InicioPageRoutingModule } from './inicio-routing.module';

import { InicioPage } from './inicio.page';
import { ToolbarInicioComponent } from 'src/app/components/toolbar-inicio/toolbar-inicio.component';
import { ModalEspaciosModule } from 'src/app/components/modal-espacios/modal-espacios.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InicioPageRoutingModule,
    ModalEspaciosModule
  ],
  declarations: [InicioPage, ToolbarInicioComponent, ToggleInicioComponent]
})
export class InicioPageModule { }
