import { Component, OnInit, ViewChild } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { StorageService } from 'src/app/services/storage.service';
import {
  AlertController,
  LoadingController,
  ModalController,
  NavController,
  ToastController,
} from '@ionic/angular';
import { Beneficio } from 'src/app/models/beneficio.model';
import { DashboardService } from 'src/app/services/dashboard.service';
import { EspacioService } from 'src/app/services/espacio.service';
import { Router } from '@angular/router';
import { ModalEspaciosComponent } from 'src/app/components/modal-espacios/modal-espacios.component';
import { ClubService } from 'src/app/services/club.service';
import * as moment from 'moment';
import { ToggleInicioComponent } from 'src/app/components/toggle-inicio/toggle-inicio.component';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit {
  @ViewChild('toggleInicio') toggleInicio: ToggleInicioComponent;

  listNotificaciones: any[] = [];
  listDeportes: any[] = [];
  listTurnos: any[] = [];
  beneficios: Beneficio[];
  format = '';
  text = '';
  redes = false;
  loading: HTMLIonLoadingElement;
  userInfo;
  tokenInfo;

  slideOptsBeneficios = {
    initialSlide: 1,
    speed: 400,
  };

  slideOptsNotificaciones = {
    initialSlide: 1,
    speed: 400,
  };

  constructor(
    private barcodeScanner: BarcodeScanner,
    public toastController: ToastController,
    public loadingController: LoadingController,
    public alertController: AlertController,
    public _storageService: StorageService,
    private _dashboardService: DashboardService,
    private espacioService: EspacioService,
    public modalController: ModalController,
    private router: Router,
    private clubService: ClubService
  ) {
    this.userInfo = this._storageService.localGet('userInfo');
    clubService.bindChangeClub().subscribe(d => {
      this.setDashboard();
    })
    
  }

  ngOnInit() {
    const self = this;
    self.setDashboard();
    
    
  }

  setDashboard() {
    this.clubService.dashboardChanges();
    this.toggleInicio && this.toggleInicio.setData();
    this.tokenInfo = this._storageService.localGet('tokenInfo');
    this._dashboardService
      .obtenerDashboard(this.tokenInfo.clubId, this.tokenInfo.userId)
      .subscribe(
        (data) => {
          this.listNotificaciones = data.notificaciones;
          this.listDeportes = data.disciplinaxclub;
          this.listTurnos = data.turnos;
          this.beneficios = data.beneficios;
          console.warn(data)
        },
        (error) => {
          console.log(error);
        }
      );
  }

  mostrarPop(id) {
    this.espacioService.getEspaciosPorDisciplina(id).subscribe(esps => {
      this.presentarEspaciosModal(esps);
    })



  }

  ionViewWillEnter() {
    this.setDashboard();
  }

  async presentarEspaciosModal(esps) {
    const modal = await this.modalController.create({
      component: ModalEspaciosComponent,
      cssClass: 'my-custom-class',
      swipeToClose: true,
      componentProps: {
        espacios: esps,
        titulo: 'Selecciona un espacio'
      }
    });
    return await modal.present();
  }

  redesTogle() {
    this.redes = !this.redes;
  }

  scan() {
    this.router.navigate(['menu/ingresos']);
    /*this.barcodeScanner
      .scan({
        prompt: 'Apunta con tu cámara al código Qr del espacio al cual estás ingresando'
      })
      .then((barcodeData) => {

        if (!barcodeData.cancelled) {
          this.format = barcodeData.format;
          this.text = barcodeData.text;
        }
      })
      .catch((err) => {
        console.log('Error', err);
      });*/
  }

  goTurn(turno) {
    this.router.navigate(['/menu/turnos/confirmado/' + turno.id + '/' + turno.espacioId]);
  }

  irATurnos() {
    this.router.navigate(['/menu/turnos']);
  }

  gotoNotification(item) {
    this.router.navigate(['/menu/notificaciones/notificacion/', item.club.notificacionId]);
  }

  gotoNotifications() {
    this.router.navigate(['/menu/notificaciones/']);
  }

  irABeneficios() {
    this.router.navigate(['/menu/beneficios/']);
  }

  getFecha(f) {
    return moment(f).locale('es').format('dddd DD [de] MMMM')
  }

  getHora(d, h) {
    return this.cutZeros(d) + ' - ' + this.cutZeros(h) + 'hs';
  }

  cutZeros(h) {
    let arr = h.split(':');
    return arr[0] + (arr[1] ? ':' + arr[1] : '');
  }

  goToBeneficio(data) {
    this.router.navigate(['/menu/beneficios/detalle-beneficio', data.beneficioId]);
  }
}

