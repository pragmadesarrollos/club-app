import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InicioPage } from './inicio.page';

const routes: Routes = [
  {
    path: '',
    component: InicioPage
  },
  {
    path: 'disciplinas',
    loadChildren: () => import('../disciplinas/disciplinas.module').then( m => m.DisciplinasPageModule)
  },
  {
    path: 'notificaciones',
    loadChildren: () => import('../notificaciones/notificaciones.module').then( m => m.NotificacionesPageModule)
  },
  {
    path: 'espacios',
    loadChildren: () => import('../espacios/espacios.module').then( m => m.EspaciosPageModule)
  },
  {
    path: 'detalle-beneficio/:idBeneficio',
    loadChildren: () => import('../beneficios/detalle-beneficio/detalle-beneficio.module').then( m => m.DetalleBeneficioPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InicioPageRoutingModule {}
