import { Component, OnInit } from '@angular/core';
import { IngresoService } from 'src/app/services/ingreso.service';
import { Router } from '@angular/router';
import { ActionSheetController } from '@ionic/angular';
import { EspacioService } from 'src/app/services/espacio.service';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { ReservaService } from 'src/app/services/reserva.service';
import { StorageService } from 'src/app/services/storage.service';
import { Espacio, Ingresos } from 'src/app/models/ingresos.model';
import { MasterService } from 'src/app/helpers/master.service';
import * as moment from 'moment';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-ingresos',
  templateUrl: './ingresos.page.html',
  styleUrls: ['./ingresos.page.scss'],
})
export class IngresosPage implements OnInit {
  titulo = 'Ingresos';
  espacios: Espacio[];
  ingresos: Ingresos[] = [];
  tokenInfo;
  isManager = false;

  constructor(
    private router: Router,
    private ingresoService: IngresoService,
    public actionSheetController: ActionSheetController,
    private espacioService: EspacioService,
    private barcodeScanner: BarcodeScanner,
    private reservaService: ReservaService,
    private storageService: StorageService,
    private userService: UsuarioService,
    private masterService: MasterService
  ) {
    this.tokenInfo = this.storageService.localGet('tokenInfo');
  }

  ngOnInit() {
    // Ver la forma de validar si el usuario es manager o usuario normal, y segun eso poner un id o el otro 
    this.isManager = this.tokenInfo.rol == 'manager';
    this.ingresoService.obtenerIngresosPorIdDeUsuarioOManager(null, (this.isManager ? null : this.tokenInfo.userId), (this.isManager ? this.tokenInfo.userId : null), moment().subtract(1, 'week').format('YYYY-MM-DD'), moment().format('YYYY-MM-DD')).subscribe(d => {
      console.warn(d);
      this.ingresos = d;
      this.setEspacios();
      this.setUsuarios();
    }, e => {
      console.error(e);
    })
  }

  setEspacios() {
    const self = this;
    this.ingresos.forEach( (i: any) => {
      self.getEspacioById(i.espacioId).subscribe(d => {
        i['espacio'] = d;
      });
    })
  }

  setUsuarios() {
    const self = this;
    this.ingresos.forEach( (u: any) => {
      self.getUserById(u.usuarioId).subscribe(d => {
        u['usuario'] = d;
      });
    })
  }

  getFecha(f) {
    return moment(f).locale('es').format('DD [de] MMMM')
  }

  getEmptyImage() {
    return './assets/img/no-image-available.png';
  }

  getEspacioById(id) {
    return this.espacioService.getEspacio(id);
  }

  getUserById(id) {
    return this.userService.obtenerUsuario(id);
  }

  async presentActionSheetQR() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Selecciona un espacio',
      buttons: this.returnButtons(),
    });
    await actionSheet.present();
  }

  returnButtons() {
    const buttons = [];
    if (this.ingresos.length > 0) {
      console.warn(this.ingresos);
      this.ingresos.forEach((data) => {
        buttons.push({
          text: data.reserva.turno.espacio.nombre,
          icon: 'football-outline',
          handler: () => {
            console.log('ESPACIO SELECCIONADO', data);
            this.router.navigate(['/menu/ingresos/escanea-codigo/', data.id]);
          },
        });
      });
    }
    return buttons;
  }

  goToScan() {
    this.barcodeScanner
      .scan({
        prompt: 'Apunta con tu cámara al código Qr del espacio al cual estás ingresando'
      })
      .then((barcodeData) => {
        console.log('Barcode data', barcodeData.text);
        let arr = (barcodeData.text).split('/');
        this.ingresoService.crearIngreso(arr[0], arr[1]).subscribe(data => {
          console.log('INGRESO REALIZADO: ', data);
          this.masterService.setSplash({
            img: 'assets/img/splash/correcto.png',
            redirecTo: '/menu',
            titulo: '¡Ingreso registrado con éxito!',
            subtitulo: '',
          });

          this.router.navigateByUrl('/menu/splash-generico');
        }, error => {
          console.log('RESERVA ERROR: ', error);

          this.masterService.setSplash({
            img: 'assets/img/splash/error.png',
            redirecTo: '/menu',
            titulo: 'Ocurrio un error al registrar tu ingreso',
            subtitulo: 'Contactese con el Administrador',
          });

          this.router.navigateByUrl('/menu/splash-generico');
        });
      })
      .catch((err) => {
        console.log('Error', err);
        /* this._dataLocal.guardarRegistro('QRCode', 'http://www.google.com'); */
      });
  }

  goToScanCodigoEspacio(data) {
    console.log('INGRESO SELECCIONADO', data);
    this.ingresoService.ingresoSeleccionado = data;
    this.router.navigate(['/menu/ingresos/escanea-codigo/', data.id]);
  }

  gotoEspacios(b) {
    this.router.navigate(['menu/ingresos/espacios', b]);
  }
}
