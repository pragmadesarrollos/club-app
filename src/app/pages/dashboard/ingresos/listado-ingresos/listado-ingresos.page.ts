import { EspacioService } from 'src/app/services/espacio.service';
import { IngresoService } from './../../../../services/ingreso.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Ingresos } from 'src/app/models/ingresos.model';
import * as moment from 'moment';
import { UsuarioService } from 'src/app/services/usuario.service';
import { PopoverController, NavController } from '@ionic/angular';
import { StorageService } from 'src/app/services/storage.service';


@Component({
  selector: 'app-listado-ingresos',
  templateUrl: './listado-ingresos.page.html',
  styleUrls: ['./listado-ingresos.page.scss'],
})
export class ListadoIngresosPage implements OnInit {
  titulo = '';
  ingresos: Ingresos[];
  searchbar: string;
  espacio;
  ingresosAux = [];
  tokenInfo;
  isManager = false;

  constructor(
    private route: ActivatedRoute,
    private ingresoService: IngresoService,
    private espacioService: EspacioService,
    private usuarioService: UsuarioService,
    public popoverController: PopoverController,
    private storageService: StorageService,
  ) {
    this.tokenInfo = this.storageService.localGet('tokenInfo');
    this.isManager = this.tokenInfo.rol == 'manager';
  }

  ngOnInit() {
    this.setIngresos();
  }
  
  setIngresos(desde?, hasta?) {
    let d = desde || moment().subtract(1, 'week').format('YYYY-MM-DD');
    let h = hasta || moment().format('YYYY-MM-DD')
    this.ingresoService.obtenerIngresosPorIdDeUsuarioOManager(this.route.snapshot.params.idEspacio, (this.isManager ? null : this.tokenInfo.userId), (this.isManager ? this.tokenInfo.userId : null), d, h).subscribe((d: Ingresos[]) => {
      this.ingresos = this.separateEntries(d);
      this.ingresosAux = this.separateEntries(d);
      this.setUsuarios();
    }, e => {
      console.error(e);
    });
    this.espacioService.getEspacio(this.route.snapshot.params.idEspacio).subscribe(e => {
      this.espacio = e;
    })
  }

  separateEntries(a: any[]): Ingresos[] {
    let obj = [];
    a =  a.sort(function(a,b) {
      if (a.fecha == b.fecha) { return 0; }
      else if (a.fecha > b.fecha) { return -1; }
      else { return 1; }
    });
    a.forEach( e => {
      let ele = obj.find(p => p.title == e.fecha);
      if(ele) {
        ele.items.push(e);
      } else {
        obj.push({
          title: e.fecha,
          formatedDate: this.getFormatedDate(e.fecha),
          items: [ e ]
        });
      }
    });
    console.error(obj);
    return obj;
  }

  orderArray(a,b) {
    
  }

  getFormatedDate(f) {
    return moment().format('YYYY-MM-DD') == f ? 'Hoy' : moment(f).locale('es').format('DD [de] MMMM');
  }

  filterList(evt) {
    this.ingresos = this.ingresosAux.filter(i => (i.usuario.persona.nombre.toLowerCase().indexOf(evt.detail.value.toLowerCase()) > -1) || (i.usuario.persona.apellido.toLowerCase().indexOf(evt.detail.value.toLowerCase()) > -1));
  }

  setUsuarios() {
    const self = this;
    self.ingresos.forEach( (i: any, index) => {
      i.items.forEach(j => {
        self.usuarioService.obtenerUsuario(j.usuarioId).subscribe(u => {
          j['usuario'] = u;
          this.ingresosAux[index]['usuario'] = u;
        });
      })
    });
  }

  getFecha(f) {
    return moment(f).locale('es').format('DD [de] MMMM')
  }

  async popFilter(ev: any) {
    const popover = await this.popoverController.create({
      component: PopoverFiltersComponent,
      event: ev
    });
    await popover.present();

    const { data } = await popover.onDidDismiss();
    if(data) {
      this.setIngresos(data.desde, data.hasta);
    }
  }
}

@Component({
  selector: 'popover-filters',
  templateUrl: './filters/filters.html',
  styleUrls: ['./filters/filters.scss']
})

export class PopoverFiltersComponent {

  desde = new Date().toISOString();;
  hasta = moment().format('YYYY-MM-DD');

  constructor(private popoverCtrl: PopoverController) {
   
  }

  onSelect(e) {
    let ob = {
      desde: this.desde,
      hasta: this.hasta
    }
    this.popoverCtrl.dismiss(ob)
  }

  setHasta(e) {
    this.hasta = moment(e.detail.value).format('YYYY-MM-DD');
  }

  setDesde(e) {
    this.desde = moment(e.detail.value).format('YYYY-MM-DD');
  }
  
}
