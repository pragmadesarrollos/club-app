import { Component, OnInit } from '@angular/core';
import { IngresoService } from 'src/app/services/ingreso.service';
import { ActivatedRoute, Router } from '@angular/router';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';

@Component({
  selector: 'app-escanea-codigo',
  templateUrl: './escanea-codigo.page.html',
  styleUrls: ['./escanea-codigo.page.scss'],
})
export class EscaneaCodigoPage implements OnInit {
  titulo = 'Escanea Código';
  code = '';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
  ) {
    let tokenInfo = JSON.parse(localStorage.getItem('tokenInfo'));
    this.code = this.route.snapshot.params.idEspacio + '/' + tokenInfo.userId;
  }

  ngOnInit() {
  }

  mostrarAsistencias() {
    this.router.navigate([
      '/menu/ingresos/listado-ingresos/',
      this.route.snapshot.params.idEspacio,
    ]);
  }
}
