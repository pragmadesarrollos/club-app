import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { Espacio } from 'src/app/models/ingresos.model';
import { DisciplinaService } from 'src/app/services/disciplina.service';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-modal-deportes',
  templateUrl: './modal-deportes.component.html',
  styleUrls: ['./modal-deportes.component.scss'],
})
export class ModalDeportesComponent implements OnInit {
  listDisciplinas;
  disciplina;
  posiciones;
  divisiones;
  division;
  posicion;

  constructor(private mdlCtrl: ModalController, private disciplinasService: DisciplinaService, private storage: StorageService) { }

  ngOnInit() {
    console.error(this.listDisciplinas)
  }

  dismiss() {
    this.mdlCtrl.dismiss();
  }

  

  cambioDisciplina() {
    console.log(this.disciplina);
    this.posiciones = null;
    this.posicion = null;
    this.division = null;
    let tokenInfo = this.storage.localGet('tokenInfo');
    let clubInfo = this.storage.localGet('clubInfo-' + tokenInfo.userId);
    this.divisiones = [];
    this.disciplinasService.obtenerDivisionesxDisciplina(clubInfo.id, this.disciplina.disciplinaId).subscribe(pos => {
      this.divisiones.push({id: -1, nombre: 'Todas'});
      pos.forEach(p => {
        this.divisiones.push(p);
      })
      console.error(pos)
    })
  }

  cambioDivision() {
    console.log(this.division);
    this.posiciones = null;
    if(!this.division) {
      return;
    }
    if(this.division && this.division.id == -1) {
      this.disciplinasService.obtenerPosicionXDisciplinaId(this.disciplina.disciplinaId).subscribe(pos => {
        if(pos) {
          this.posiciones = pos;
        } else {
          this.posiciones = [];
        }
        console.error('entro al primer if: ', pos)
      });
    } else {
      this.disciplinasService.obtenerPosicionXDivisionId(this.division.id).subscribe((pos: any) => {
        console.error('entro al segundoi if: ', pos)
        if(pos && pos.posiciones) {
          this.posiciones = pos.posiciones;
        } else {
          this.posiciones = [];
        }
      });
    }
    
  }

  cambioPosicion() {
    console.log(this.disciplina, this.division, this.posicion);
  }

  addDeporte() {
    let tokenInfo = this.storage.localGet('tokenInfo');
    console.log(this.disciplina, this.division, this.posicion);
    this.disciplinasService.agregarDeporteAlUsuario(tokenInfo.userId, tokenInfo.clubId, this.disciplina.disciplinaId, this.division.id, this.posicion.id).subscribe(d => {
      console.warn(d);
      this.dismiss();
    }, e => console.error(e))
  }


}
