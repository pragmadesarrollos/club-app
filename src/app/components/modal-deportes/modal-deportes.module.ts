import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ModalDeportesComponent } from './modal-deportes.component';
import { ModalDeportesPageRoutingModule } from './modal-deportes-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    ModalDeportesPageRoutingModule
  ],
  declarations: [
    ModalDeportesComponent
  ],
  exports: [ModalDeportesComponent]
})
export class ModalDeportesModule {}
