import { Component, OnInit } from '@angular/core';
import { ClubService } from 'src/app/services/club.service';
import { DashboardService } from 'src/app/services/dashboard.service';
import { EncuestaService } from 'src/app/services/encuesta.service';
import { NotificacionService } from 'src/app/services/notificacion.service';
import { StorageService } from 'src/app/services/storage.service';


@Component({
  selector: 'app-toolbar-inicio',
  templateUrl: './toolbar-inicio.component.html',
  styleUrls: ['./toolbar-inicio.component.scss'],
})
export class ToolbarInicioComponent implements OnInit {
  userInfo;
  notificacionesRecibidas;
  encuestasRecibidas;
  tokenInfo;
  subs;

  constructor(private _dashboardService: DashboardService, public notificationsService: NotificacionService, private _storageService: StorageService, private clubService: ClubService) { 
    this.clubService.bindDashboardChanges().subscribe(d => {
      this.tokenInfo = this._storageService.localGet('tokenInfo');
      this.userInfo = this._storageService.localGet('userInfo');
      this.setBell();
    });
  }

  ngOnInit() {  
    this.bindNotifications();
  }

  bindNotifications() {
    this.notificationsService.bindUpdateNotifications().subscribe(d => {
      this.setBell();
    });
  }

  ionViewWillEnter() {
    console.error('sdf dsf dsf fds f fd dsf sd sdds dfs sdf')
  }

  getUserInfo() {
    return this._storageService.localGet('userInfo');
  }

  setBell() {
    this.notificacionesRecibidas = [];
    this.subs && this.subs.unsubscribe();
    this.subs = null;
    this.subs = this._dashboardService
      .obtenerDashboard(this.tokenInfo.clubId, this.tokenInfo.userId)
      .subscribe(
        (data) => {
          this.notificacionesRecibidas = data.notificaciones;
        },
        (error) => {
          console.log(error);
        }
      );
  }




}
