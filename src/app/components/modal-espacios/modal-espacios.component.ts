import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { Espacio } from 'src/app/models/ingresos.model';

@Component({
  selector: 'app-modal-espacios',
  templateUrl: './modal-espacios.component.html',
  styleUrls: ['./modal-espacios.component.scss'],
})
export class ModalEspaciosComponent implements OnInit {
  @Input() titulo: string;
  @Input() espacios: Espacio[]; 

  constructor(private mdlCtrl: ModalController, private router: Router) { }

  ngOnInit() {
    console.error(this.espacios)
  }

  dismissModal() {
    this.mdlCtrl.dismiss();
  }

  gotoTurn(id, espacioId) {
    
    this.router.navigate(['menu/turnos/sacar-turno/', id, espacioId]);
    this.mdlCtrl.dismiss();
  }

  getImageEspacio() {
    return './assets/img/espacio-empty.jpeg'
  }

}
