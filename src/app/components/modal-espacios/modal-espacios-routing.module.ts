import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'turnos',
    loadChildren: () =>
      import('../../pages/dashboard/turnos/turnos.module').then((m) => m.TurnosPageModule),
  },
  {
    path: 'turnos/sacar-turno/:id',
    loadChildren: () => import('../../pages/dashboard/turnos/sacar-turno/sacar-turno.module').then( m => m.SacarTurnoPageModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModalEspaciosPageRoutingModule {}
