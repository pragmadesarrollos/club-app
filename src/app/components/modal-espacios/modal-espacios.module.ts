import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ModalEspaciosComponent } from './modal-espacios.component';
import { ModalEspaciosPageRoutingModule } from './modal-espacios-routing.module';
import { TurnosPageModule } from 'src/app/pages/dashboard/turnos/turnos.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    ModalEspaciosPageRoutingModule
  ],
  declarations: [
    ModalEspaciosComponent
  ],
  exports: [ModalEspaciosComponent]
})
export class ModalEspaciosModule {}
