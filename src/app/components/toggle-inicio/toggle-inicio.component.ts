import { ClubInfo } from './../../models/club.model';
import { ClubService } from 'src/app/services/club.service';
import { Component, Input, OnInit } from '@angular/core';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-toggle-inicio',
  templateUrl: './toggle-inicio.component.html',
  styleUrls: ['./toggle-inicio.component.scss'],
})
export class ToggleInicioComponent implements OnInit {
  redes = false;
  club: ClubInfo;

  constructor(private clubService: ClubService, public storageService: StorageService,) {
    
  }

  ngOnInit() { }

  redesTogle() {
    this.redes = !this.redes;
  }
  
  ionViewWillEnter() {
    
  }

  setData() {
    if (this.storageService.localGet('tokenInfo')) {
      let usrID = this.storageService.localGet('tokenInfo').userId;
      this.club = this.storageService.localGet('clubInfo-' + usrID);
      console.error(this.club)
    }
  }
}
