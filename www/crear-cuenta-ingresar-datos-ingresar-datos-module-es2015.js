(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["crear-cuenta-ingresar-datos-ingresar-datos-module"],{

/***/ "6ynD":
/*!*******************************************************************************************!*\
  !*** ./src/app/pages/acceso/crear-cuenta/ingresar-datos/ingresar-datos-routing.module.ts ***!
  \*******************************************************************************************/
/*! exports provided: IngresarDatosPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IngresarDatosPageRoutingModule", function() { return IngresarDatosPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _ingresar_datos_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./ingresar-datos.page */ "bngM");




const routes = [
    {
        path: '',
        component: _ingresar_datos_page__WEBPACK_IMPORTED_MODULE_3__["IngresarDatosPage"]
    }
];
let IngresarDatosPageRoutingModule = class IngresarDatosPageRoutingModule {
};
IngresarDatosPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], IngresarDatosPageRoutingModule);



/***/ }),

/***/ "D9N1":
/*!***********************************************************************************!*\
  !*** ./src/app/pages/acceso/crear-cuenta/ingresar-datos/ingresar-datos.page.scss ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-toolbar {\n  --background: #514cbd;\n}\n\nion-back-button,\nion-title {\n  --color: white !important;\n}\n\nion-content {\n  --height: 100%;\n  --background: #6b64f8;\n}\n\n.stepper {\n  height: 100px;\n  background-color: #514cbd;\n  border-radius: 0px 0px 32px 32px;\n  display: flex;\n  justify-content: center;\n}\n\n.title {\n  font-family: \"Founders Grotesk\";\n  font-style: normal;\n  font-weight: normal;\n  font-size: 20px;\n  line-height: 24px;\n  display: flex;\n  align-items: center;\n  color: #ffffff;\n}\n\n.img {\n  width: 330px;\n  max-height: 75px;\n}\n\n.textoAcceso {\n  margin-top: 20px;\n  font-family: \"Roboto\", sans-serif;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 16px;\n  line-height: 19px;\n  text-align: center;\n  color: #ffffff;\n}\n\nion-button {\n  background: white !important;\n  color: #514cbd !important;\n  border-radius: 8px;\n}\n\nion-card {\n  margin-left: 15px;\n  margin-right: 15px;\n  margin-top: 20px;\n}\n\nion-item {\n  --highlight-color-valid: #514cbd;\n}\n\nimg {\n  width: 150px;\n}\n\nion-footer ion-toolbar {\n  --background: #6b64f8;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL2luZ3Jlc2FyLWRhdG9zLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHFCQUFBO0FBQ0o7O0FBRUE7O0VBRUkseUJBQUE7QUFDSjs7QUFFQTtFQUNJLGNBQUE7RUFDQSxxQkFBQTtBQUNKOztBQUVBO0VBQ0ksYUFBQTtFQUNBLHlCQUFBO0VBQ0EsZ0NBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7QUFDSjs7QUFFQTtFQUNJLCtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLGNBQUE7QUFDSjs7QUFFQTtFQUNJLFlBQUE7RUFDQSxnQkFBQTtBQUNKOztBQUVBO0VBQ0ksZ0JBQUE7RUFDQSxpQ0FBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7QUFDSjs7QUFFQTtFQUNJLDRCQUFBO0VBQ0EseUJBQUE7RUFDQSxrQkFBQTtBQUNKOztBQUVBO0VBQ0ksaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0FBQ0o7O0FBRUE7RUFDSSxnQ0FBQTtBQUNKOztBQUVBO0VBQ0ksWUFBQTtBQUNKOztBQUdJO0VBQ0kscUJBQUE7QUFBUiIsImZpbGUiOiJpbmdyZXNhci1kYXRvcy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tdG9vbGJhciB7XG4gICAgLS1iYWNrZ3JvdW5kOiAjNTE0Y2JkO1xufVxuXG5pb24tYmFjay1idXR0b24sXG5pb24tdGl0bGUge1xuICAgIC0tY29sb3I6IHdoaXRlICFpbXBvcnRhbnQ7XG59XG5cbmlvbi1jb250ZW50IHtcbiAgICAtLWhlaWdodDogMTAwJTtcbiAgICAtLWJhY2tncm91bmQ6ICM2YjY0Zjg7XG59XG5cbi5zdGVwcGVyIHtcbiAgICBoZWlnaHQ6IDEwMHB4O1xuICAgIGJhY2tncm91bmQtY29sb3I6ICM1MTRjYmQ7XG4gICAgYm9yZGVyLXJhZGl1czogMHB4IDBweCAzMnB4IDMycHg7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cblxuLnRpdGxlIHtcbiAgICBmb250LWZhbWlseTogXCJGb3VuZGVycyBHcm90ZXNrXCI7XG4gICAgZm9udC1zdHlsZTogbm9ybWFsO1xuICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gICAgZm9udC1zaXplOiAyMHB4O1xuICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBjb2xvcjogI2ZmZmZmZjtcbn1cblxuLmltZyB7XG4gICAgd2lkdGg6IDMzMHB4O1xuICAgIG1heC1oZWlnaHQ6IDc1cHg7XG59XG5cbi50ZXh0b0FjY2VzbyB7XG4gICAgbWFyZ2luLXRvcDogMjBweDtcbiAgICBmb250LWZhbWlseTogXCJSb2JvdG9cIiwgc2Fucy1zZXJpZjtcbiAgICBmb250LXN0eWxlOiBub3JtYWw7XG4gICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgICBmb250LXNpemU6IDE2cHg7XG4gICAgbGluZS1oZWlnaHQ6IDE5cHg7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGNvbG9yOiAjZmZmZmZmO1xufVxuXG5pb24tYnV0dG9uIHtcbiAgICBiYWNrZ3JvdW5kOiB3aGl0ZSAhaW1wb3J0YW50O1xuICAgIGNvbG9yOiAjNTE0Y2JkICFpbXBvcnRhbnQ7XG4gICAgYm9yZGVyLXJhZGl1czogOHB4O1xufVxuXG5pb24tY2FyZCB7XG4gICAgbWFyZ2luLWxlZnQ6IDE1cHg7XG4gICAgbWFyZ2luLXJpZ2h0OiAxNXB4O1xuICAgIG1hcmdpbi10b3A6IDIwcHg7XG59XG5cbmlvbi1pdGVtIHtcbiAgICAtLWhpZ2hsaWdodC1jb2xvci12YWxpZDogIzUxNGNiZDsgLy8gdmFsaWQgdW5kZXJsaW5lIGNvbG9yXG59XG5cbmltZyB7XG4gICAgd2lkdGg6IDE1MHB4O1xufVxuXG5pb24tZm9vdGVyIHtcbiAgICBpb24tdG9vbGJhciB7XG4gICAgICAgIC0tYmFja2dyb3VuZDogIzZiNjRmODtcbiAgICB9XG59XG4iXX0= */");

/***/ }),

/***/ "YRyR":
/*!***********************************************************************************!*\
  !*** ./src/app/pages/acceso/crear-cuenta/ingresar-datos/ingresar-datos.module.ts ***!
  \***********************************************************************************/
/*! exports provided: IngresarDatosPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IngresarDatosPageModule", function() { return IngresarDatosPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _ingresar_datos_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./ingresar-datos-routing.module */ "6ynD");
/* harmony import */ var _ingresar_datos_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./ingresar-datos.page */ "bngM");







let IngresarDatosPageModule = class IngresarDatosPageModule {
};
IngresarDatosPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _ingresar_datos_routing_module__WEBPACK_IMPORTED_MODULE_5__["IngresarDatosPageRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"]
        ],
        declarations: [_ingresar_datos_page__WEBPACK_IMPORTED_MODULE_6__["IngresarDatosPage"]]
    })
], IngresarDatosPageModule);



/***/ }),

/***/ "bngM":
/*!*********************************************************************************!*\
  !*** ./src/app/pages/acceso/crear-cuenta/ingresar-datos/ingresar-datos.page.ts ***!
  \*********************************************************************************/
/*! exports provided: IngresarDatosPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IngresarDatosPage", function() { return IngresarDatosPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_ingresar_datos_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./ingresar-datos.page.html */ "lOfF");
/* harmony import */ var _ingresar_datos_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ingresar-datos.page.scss */ "D9N1");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var src_app_services_usuario_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/usuario.service */ "on2l");







let IngresarDatosPage = class IngresarDatosPage {
    constructor(router, fb, _usuarioService) {
        this.router = router;
        this.fb = fb;
        this._usuarioService = _usuarioService;
        this.form = this.fb.group({
            nombre: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            apellido: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            documento: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
        });
    }
    ngOnInit() {
    }
    siguiente() {
        this._usuarioService.nombre = this.form.value.nombre;
        this._usuarioService.apellido = this.form.value.apellido;
        this._usuarioService.documento = this.form.value.documento;
        this.router.navigateByUrl('/login/ingresar-password');
    }
};
IngresarDatosPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"] },
    { type: src_app_services_usuario_service__WEBPACK_IMPORTED_MODULE_6__["UsuarioService"] }
];
IngresarDatosPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-ingresar-datos',
        template: _raw_loader_ingresar_datos_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_ingresar_datos_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], IngresarDatosPage);



/***/ }),

/***/ "lOfF":
/*!*************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/acceso/crear-cuenta/ingresar-datos/ingresar-datos.page.html ***!
  \*************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n    <ion-back-button defaultHref=\"/\" text=\"\" color>\n      </ion-back-button> \n  \n    </ion-buttons>\n    <ion-title class=\"title\">\n      Crear Cuenta\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"stepper\">\n    <img class=\"img\" src=\"/assets/img/crear-cuenta/crearCuentaDatos.png\">\n  </div>\n\n  <div class=\"textoAcceso\">\n    Completá con tus datos personales\n  </div>\n  <form [formGroup]=\"form\" (ngSubmit)=\"siguiente()\">\n    <ion-card>\n      <ion-card-content>\n        <ion-item>\n          <ion-label position=\"stacked\">Nombre</ion-label>\n          <ion-input formControlName=\"nombre\" ></ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-label position=\"stacked\" required>Apellido</ion-label>\n          <ion-input formControlName=\"apellido\"></ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-label position=\"stacked\" required>Documento de Identidad</ion-label>\n          <ion-input formControlName=\"documento\"></ion-input>\n        </ion-item>\n      </ion-card-content>\n    </ion-card>\n    <ion-button type=\"submit\" class=\"ion-margin btnSiguiente\" [disabled]=\"form.invalid\" color=\"ligth\" size=\"large\" expand=\"block\">SIGUIENTE</ion-button>\n        \n  </form>\n \n</ion-content>\n\n<ion-footer class=\"ion-no-border\">\n  <ion-toolbar class=\"ion-text-center\">\n    <img routerLink=\"/login\" src=\"/assets/img/splash/logoProClubFondoV.png\">\n  </ion-toolbar>\n</ion-footer>");

/***/ })

}]);
//# sourceMappingURL=crear-cuenta-ingresar-datos-ingresar-datos-module-es2015.js.map