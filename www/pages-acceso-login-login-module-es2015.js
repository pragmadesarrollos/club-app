(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-acceso-login-login-module"],{

/***/ "EFyh":
/*!*******************************************!*\
  !*** ./src/app/services/login.service.ts ***!
  \*******************************************/
/*! exports provided: LoginService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginService", function() { return LoginService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/environments/environment */ "AytR");




let LoginService = class LoginService {
    constructor(http) {
        this.http = http;
        this.myAppUrl = src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].endpoint;
    }
    validate(correo) {
        return this.http.post(this.myAppUrl + '/api/validate', correo);
    }
    login(usuario) {
        return this.http.post(this.myAppUrl + '/api/signin', usuario);
    }
};
LoginService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }
];
LoginService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
        providedIn: 'root'
    })
], LoginService);



/***/ }),

/***/ "HTA3":
/*!**************************************************!*\
  !*** ./src/app/pages/acceso/login/login.page.ts ***!
  \**************************************************/
/*! exports provided: LoginPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPage", function() { return LoginPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_login_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./login.page.html */ "NlLU");
/* harmony import */ var _login_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./login.page.scss */ "NV/p");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var src_app_services_login_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/login.service */ "EFyh");
/* harmony import */ var src_app_services_usuario_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/usuario.service */ "on2l");









let LoginPage = class LoginPage {
    constructor(fb, router, _usuarioService, _loginService, loadingController, toastController) {
        this.fb = fb;
        this.router = router;
        this._usuarioService = _usuarioService;
        this._loginService = _loginService;
        this.loadingController = loadingController;
        this.toastController = toastController;
        this.spinner = false;
        this.checked = false;
        this.form = this.fb.group({
            correo: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].email]],
        });
    }
    ngOnInit() {
    }
    ionViewWillEnter() {
        this.form.patchValue({
            correo: ''
        });
    }
    validar() {
        const usuario = {
            correo: this.form.value.correo
        };
        this.presentLoading().then((loadRes) => {
            this.loading = loadRes;
            // this.loading.present()
            this.spinner = true;
            this._loginService.validate(usuario).subscribe(data => {
                this.dismissLoading();
                this.redirect(data);
            }, error => {
                this.mensaje('Opss.. ocurrio un error');
                this.dismissLoading();
                console.log(error);
            });
        });
    }
    redirect(data) {
        this._usuarioService.nombreUsuario = this.form.value.correo;
        console.log('data: ', data);
        switch (data.codigo) {
            case 1:
                this.router.navigateByUrl('/login/usuario-no-registrado');
                break;
            case 2:
                this.router.navigateByUrl('/login/crear-password');
                break;
            case 3:
                this.router.navigateByUrl('/login/login-verificado');
                break;
            default:
                this.mensaje('Opss.. ocurrio un error!');
                break;
        }
    }
    presentLoading() {
        if (this.loading) {
            this.loading.dismiss();
        }
        return new Promise((resolve) => {
            resolve(this.loadingController.create({
                message: 'Espere...'
            }));
        });
    }
    dismissLoading() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            // if (this.loading) {
            //   this.loading.dismiss();
            // }
            if (this.spinner) {
                this.spinner = false;
                this.checked = true;
            }
        });
    }
    mensaje(mensaje) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: mensaje,
                color: 'dark',
                duration: 3000
            });
            toast.present();
        });
    }
};
LoginPage.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
    { type: src_app_services_usuario_service__WEBPACK_IMPORTED_MODULE_8__["UsuarioService"] },
    { type: src_app_services_login_service__WEBPACK_IMPORTED_MODULE_7__["LoginService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ToastController"] }
];
LoginPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-login',
        template: _raw_loader_login_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_login_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], LoginPage);



/***/ }),

/***/ "NV/p":
/*!****************************************************!*\
  !*** ./src/app/pages/acceso/login/login.page.scss ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-content {\n  --height: 100%;\n  --background: linear-gradient(0deg, #F4F3F3 50%, #6b64f8 50%);\n}\n\nion-toolbar {\n  --background: #F4F3F3;\n}\n\nimg {\n  width: 150px;\n}\n\n#container {\n  text-align: center;\n  position: absolute;\n  left: 0;\n  right: 0;\n  top: 50%;\n  transform: translateY(-50%);\n}\n\n.img {\n  position: absolute;\n  z-index: -2;\n  bottom: 80px;\n  margin-left: -155px;\n}\n\n.img img {\n  margin-right: 200px;\n  width: 400px;\n  height: 400px;\n  transform: rotate(15deg);\n}\n\n.inputCorreo {\n  margin-left: 10px;\n  margin-right: 20px;\n}\n\n.mail-icon {\n  font-size: 25px;\n}\n\n.spinner-container {\n  position: absolute;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  opacity: 50%;\n  right: 30px;\n  top: 15px;\n  border-radius: 50px;\n}\n\n.spinner-container ion-spiner {\n  font-size: 25px;\n  padding: 0px;\n}\n\n.spinner-container ion-icon {\n  font-size: 25px;\n  color: green;\n}\n\n.btnIngresar {\n  margin-left: 10px;\n  margin-right: 10px;\n  font-size: 15px;\n  font-family: \"Roboto\", sans-serif;\n}\n\n.btnIngresar[disabled] {\n  opacity: 1;\n  --ion-color-base: grey;\n}\n\n.input-group-text {\n  border-style: none;\n  background-color: white;\n}\n\n.form-control {\n  padding-left: 0;\n  padding-right: 50px;\n  border-style: none;\n}\n\n.form-control:focus {\n  border-color: rgba(255, 255, 255, 0.9);\n  box-shadow: inset 0 1px 1px rgba(255, 255, 255, 0.3), 0 0 8px rgba(255, 255, 255, 0.3);\n}\n\n.hola {\n  /* Title */\n  margin-top: -60px;\n  font-family: \"Roboto\", sans-serif;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 50px;\n  line-height: 31px;\n  color: white;\n  z-index: 1;\n}\n\n.textoAcceso {\n  margin-top: 20px;\n  font-family: \"Roboto\", sans-serif;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 16px;\n  line-height: 19px;\n  text-align: center;\n  color: #FFFFFF;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL2xvZ2luLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGNBQUE7RUFDQSw2REFBQTtBQUNGOztBQUVBO0VBQ0UscUJBQUE7QUFDRjs7QUFFQTtFQUNFLFlBQUE7QUFDRjs7QUFFQTtFQUNFLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxPQUFBO0VBQ0EsUUFBQTtFQUNBLFFBQUE7RUFDQSwyQkFBQTtBQUNGOztBQUVBO0VBQ0Usa0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0FBQ0Y7O0FBQ0U7RUFDRSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0VBQ0Esd0JBQUE7QUFDSjs7QUFHQTtFQUNFLGlCQUFBO0VBQ0Esa0JBQUE7QUFBRjs7QUFHQTtFQUNFLGVBQUE7QUFBRjs7QUFJQTtFQUNFLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFFQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLFNBQUE7RUFDQSxtQkFBQTtBQUZGOztBQUlFO0VBQ0UsZUFBQTtFQUNBLFlBQUE7QUFGSjs7QUFLRTtFQUNFLGVBQUE7RUFDQSxZQUFBO0FBSEo7O0FBU0E7RUFDRSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLGlDQUFBO0FBTkY7O0FBU0U7RUFDRSxVQUFBO0VBQ0Esc0JBQUE7QUFQSjs7QUFZQTtFQUNFLGtCQUFBO0VBQ0EsdUJBQUE7QUFURjs7QUFZQTtFQUNFLGVBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0FBVEY7O0FBWUE7RUFDRSxzQ0FBQTtFQUNBLHNGQUFBO0FBVEY7O0FBWUE7RUFDRSxVQUFBO0VBQ0EsaUJBQUE7RUFDQSxpQ0FBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtBQVRGOztBQVlBO0VBQ0UsZ0JBQUE7RUFDQSxpQ0FBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7QUFURiIsImZpbGUiOiJsb2dpbi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudCB7XG4gIC0taGVpZ2h0OiAxMDAlO1xuICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgwZGVnLCAjRjRGM0YzIDUwJSwgIzZiNjRmOCA1MCUpO1xufVxuXG5pb24tdG9vbGJhciB7XG4gIC0tYmFja2dyb3VuZDogI0Y0RjNGMztcbn1cblxuaW1nIHtcbiAgd2lkdGg6IDE1MHB4O1xufVxuXG4jY29udGFpbmVyIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IDA7XG4gIHJpZ2h0OiAwO1xuICB0b3A6IDUwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01MCUpO1xufVxuXG4uaW1nIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB6LWluZGV4OiAtMjtcbiAgYm90dG9tOiA4MHB4O1xuICBtYXJnaW4tbGVmdDogLTE1NXB4O1xuXG4gIGltZyB7XG4gICAgbWFyZ2luLXJpZ2h0OiAyMDBweDtcbiAgICB3aWR0aDogNDAwcHg7XG4gICAgaGVpZ2h0OiA0MDBweDtcbiAgICB0cmFuc2Zvcm06IHJvdGF0ZSgxNWRlZyk7XG4gIH1cbn1cblxuLmlucHV0Q29ycmVvIHtcbiAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gIG1hcmdpbi1yaWdodDogMjBweDtcbn1cblxuLm1haWwtaWNvbiB7XG4gIGZvbnQtc2l6ZTogMjVweDtcbn1cblxuXG4uc3Bpbm5lci1jb250YWluZXIge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuXG4gIG9wYWNpdHk6IDUwJTtcbiAgcmlnaHQ6IDMwcHg7XG4gIHRvcDogMTVweDtcbiAgYm9yZGVyLXJhZGl1czogNTBweDtcblxuICBpb24tc3BpbmVyIHtcbiAgICBmb250LXNpemU6IDI1cHg7XG4gICAgcGFkZGluZzogMHB4O1xuICB9XG5cbiAgaW9uLWljb24ge1xuICAgIGZvbnQtc2l6ZTogMjVweDtcbiAgICBjb2xvcjogZ3JlZW47XG4gIH1cbn1cblxuXG5cbi5idG5JbmdyZXNhciB7XG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgZm9udC1mYW1pbHk6ICdSb2JvdG8nLFxuICAgIHNhbnMtc2VyaWY7XG5cbiAgJltkaXNhYmxlZF0ge1xuICAgIG9wYWNpdHk6IDE7XG4gICAgLS1pb24tY29sb3ItYmFzZTogZ3JleTtcbiAgfVxuXG59XG5cbi5pbnB1dC1ncm91cC10ZXh0IHtcbiAgYm9yZGVyLXN0eWxlOiBub25lO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbn1cblxuLmZvcm0tY29udHJvbCB7XG4gIHBhZGRpbmctbGVmdDogMDtcbiAgcGFkZGluZy1yaWdodDogNTBweDtcbiAgYm9yZGVyLXN0eWxlOiBub25lO1xufVxuXG4uZm9ybS1jb250cm9sOmZvY3VzIHtcbiAgYm9yZGVyLWNvbG9yOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuOSk7XG4gIGJveC1zaGFkb3c6IGluc2V0IDAgMXB4IDFweCByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuMyksIDAgMCA4cHggcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjMpO1xufVxuXG4uaG9sYSB7XG4gIC8qIFRpdGxlICovXG4gIG1hcmdpbi10b3A6IC02MHB4O1xuICBmb250LWZhbWlseTogJ1JvYm90bycsIHNhbnMtc2VyaWY7XG4gIGZvbnQtc3R5bGU6IG5vcm1hbDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGZvbnQtc2l6ZTogNTBweDtcbiAgbGluZS1oZWlnaHQ6IDMxcHg7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgei1pbmRleDogMTtcbn1cblxuLnRleHRvQWNjZXNvIHtcbiAgbWFyZ2luLXRvcDogMjBweDtcbiAgZm9udC1mYW1pbHk6ICdSb2JvdG8nLCBzYW5zLXNlcmlmO1xuICBmb250LXN0eWxlOiBub3JtYWw7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgbGluZS1oZWlnaHQ6IDE5cHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgY29sb3I6ICNGRkZGRkY7XG59XG4iXX0= */");

/***/ }),

/***/ "NlLU":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/acceso/login/login.page.html ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n  <div id=\"container\">\n\n    <div class=\"img\">\n      <img src=\"../../../../assets/img/splash/ilustracion_celular.png\" alt=\"\">\n    </div>\n    <div class=\"hola\">\n      ¡Hola!\n    </div>\n    <div class=\"textoAcceso\">\n      Para ingresar accedé\n      <br>\n      con tu correo electronico\n    </div>\n    <form [formGroup]=\"form\" (ngSubmit)=\"validar()\">\n      <div class=\"input-group input-group-lg inputCorreo mt-4\">\n        <span class=\"input-group-text\" id=\"inputGroup-sizing-lg\">\n          <ion-icon name=\"mail-outline\" class=\"mail-icon\"></ion-icon>\n          <!-- <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"20\" height=\"20\" fill=\"currentColor\" class=\"bi bi-briefcase\"\n            viewBox=\"0 0 16 16\">\n            <path\n              d=\"M6.5 1A1.5 1.5 0 0 0 5 2.5V3H1.5A1.5 1.5 0 0 0 0 4.5v8A1.5 1.5 0 0 0 1.5 14h13a1.5 1.5 0 0 0 1.5-1.5v-8A1.5 1.5 0 0 0 14.5 3H11v-.5A1.5 1.5 0 0 0 9.5 1h-3zm0 1h3a.5.5 0 0 1 .5.5V3H6v-.5a.5.5 0 0 1 .5-.5zm1.886 6.914L15 7.151V12.5a.5.5 0 0 1-.5.5h-13a.5.5 0 0 1-.5-.5V7.15l6.614 1.764a1.5 1.5 0 0 0 .772 0zM1.5 4h13a.5.5 0 0 1 .5.5v1.616L8.129 7.948a.5.5 0 0 1-.258 0L1 6.116V4.5a.5.5 0 0 1 .5-.5z\" />\n          </svg> -->\n        </span>\n        <input type=\"text\" class=\"form-control\" formControlName=\"correo\" placeholder=\"Correo electronico\"\n          style=\"margin-right: 20px; border-top-right-radius: 5px; height: 4rem;\">\n\n        <div class=\"spinner-container\">\n          <ion-spinner name=\"bubbles\" *ngIf=\"spinner\" class=\"spinner ion-no-padding\"></ion-spinner>\n          <ion-icon name=\"checkmark-outline\" *ngIf=\"checked\" class=\"checked ion-no-padding\"></ion-icon>\n        </div>\n      </div>\n\n      <ion-button type=\"submit\" size=\"large\" class=\"btnIngresar\" color=\"proclub\" [disabled]=\"form.invalid\"\n        expand=\"block\">INGRESAR</ion-button>\n    </form>\n  </div>\n</ion-content>\n<ion-footer class=\"ion-no-border\">\n  <ion-toolbar class=\"ion-text-center\">\n    <img src=\"/assets/img/splash/ProClubLogo.png\">\n  </ion-toolbar>\n</ion-footer>");

/***/ }),

/***/ "RCpi":
/*!************************************************************!*\
  !*** ./src/app/pages/acceso/login/login-routing.module.ts ***!
  \************************************************************/
/*! exports provided: LoginPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageRoutingModule", function() { return LoginPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./login.page */ "HTA3");




const routes = [
    {
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_3__["LoginPage"],
    },
    {
        path: 'crear-password',
        loadChildren: () => __webpack_require__.e(/*! import() | crear-password-crear-password-module */ "crear-password-crear-password-module").then(__webpack_require__.bind(null, /*! ../crear-password/crear-password.module */ "hy4E")).then(m => m.CrearPasswordPageModule)
    },
    {
        path: 'login-verificado',
        loadChildren: () => Promise.all(/*! import() | login-verificado-login-verificado-module */[__webpack_require__.e("common"), __webpack_require__.e("login-verificado-login-verificado-module")]).then(__webpack_require__.bind(null, /*! ../login-verificado/login-verificado.module */ "84wE")).then(m => m.LoginVerificadoPageModule)
    },
    {
        path: 'usuario-no-registrado',
        loadChildren: () => __webpack_require__.e(/*! import() | usuario-no-registrado-usuario-no-registrado-module */ "usuario-no-registrado-usuario-no-registrado-module").then(__webpack_require__.bind(null, /*! ../usuario-no-registrado/usuario-no-registrado.module */ "49on")).then(m => m.UsuarioNoRegistradoPageModule)
    },
    {
        path: 'ingresar-cuenta',
        loadChildren: () => __webpack_require__.e(/*! import() | crear-cuenta-ingresar-datos-ingresar-datos-module */ "crear-cuenta-ingresar-datos-ingresar-datos-module").then(__webpack_require__.bind(null, /*! ../crear-cuenta/ingresar-datos/ingresar-datos.module */ "YRyR")).then(m => m.IngresarDatosPageModule)
    }, {
        path: 'ingresar-password',
        loadChildren: () => __webpack_require__.e(/*! import() | crear-cuenta-ingresar-password-ingresar-password-module */ "crear-cuenta-ingresar-password-ingresar-password-module").then(__webpack_require__.bind(null, /*! ../crear-cuenta/ingresar-password/ingresar-password.module */ "kjL5")).then(m => m.IngresarPasswordPageModule)
    },
    {
        path: 'ingresar-club',
        loadChildren: () => Promise.all(/*! import() | crear-cuenta-ingresar-club-ingresar-club-module */[__webpack_require__.e("default~acceso-crear-cuenta-ingresar-club-ingresar-club-module~crear-cuenta-ingresar-club-ingresar-c~677df094"), __webpack_require__.e("common")]).then(__webpack_require__.bind(null, /*! ../crear-cuenta/ingresar-club/ingresar-club.module */ "sbZs")).then(m => m.IngresarClubPageModule)
    },
    {
        path: 'verificar-cuenta',
        loadChildren: () => __webpack_require__.e(/*! import() | crear-cuenta-verificar-cuenta-verificar-cuenta-module */ "crear-cuenta-verificar-cuenta-verificar-cuenta-module").then(__webpack_require__.bind(null, /*! ../crear-cuenta/verificar-cuenta/verificar-cuenta.module */ "WZB3")).then(m => m.VerificarCuentaPageModule)
    },
    {
        path: 'olvidaste-tu-clave',
        loadChildren: () => __webpack_require__.e(/*! import() | olvidaste-tu-clave-olvidaste-tu-clave-module */ "olvidaste-tu-clave-olvidaste-tu-clave-module").then(__webpack_require__.bind(null, /*! ../olvidaste-tu-clave/olvidaste-tu-clave.module */ "ZRyC")).then(m => m.OlvidasteTuClavePageModule)
    }
];
let LoginPageRoutingModule = class LoginPageRoutingModule {
};
LoginPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], LoginPageRoutingModule);



/***/ }),

/***/ "VGEl":
/*!****************************************************!*\
  !*** ./src/app/pages/acceso/login/login.module.ts ***!
  \****************************************************/
/*! exports provided: LoginPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _login_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./login-routing.module */ "RCpi");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login.page */ "HTA3");








let LoginPageModule = class LoginPageModule {
};
LoginPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _login_routing_module__WEBPACK_IMPORTED_MODULE_5__["LoginPageRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"]
        ],
        declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]]
    })
], LoginPageModule);



/***/ })

}]);
//# sourceMappingURL=pages-acceso-login-login-module-es2015.js.map