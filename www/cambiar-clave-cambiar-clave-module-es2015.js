(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["cambiar-clave-cambiar-clave-module"],{

/***/ "HTJu":
/*!******************************************************************************!*\
  !*** ./src/app/pages/dashboard/perfil/cambiar-clave/cambiar-clave.page.scss ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".container {\n  padding: 20px;\n}\n\n.form-label {\n  font-size: 20px;\n  font-weight: 600;\n  font-family: Arial, Helvetica, sans-serif;\n}\n\n.icon {\n  font-size: 25px;\n}\n\n.icon-eye {\n  margin-top: 36px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL2NhbWJpYXItY2xhdmUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsYUFBQTtBQUNGOztBQUVBO0VBQ0UsZUFBQTtFQUNBLGdCQUFBO0VBQ0EseUNBQUE7QUFDRjs7QUFFQTtFQUNFLGVBQUE7QUFDRjs7QUFFQTtFQUNFLGdCQUFBO0FBQ0YiLCJmaWxlIjoiY2FtYmlhci1jbGF2ZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY29udGFpbmVyIHtcbiAgcGFkZGluZzogMjBweDtcbn1cblxuLmZvcm0tbGFiZWwge1xuICBmb250LXNpemU6IDIwcHg7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGZvbnQtZmFtaWx5OiBBcmlhbCwgSGVsdmV0aWNhLCBzYW5zLXNlcmlmO1xufVxuXG4uaWNvbiB7XG4gIGZvbnQtc2l6ZTogMjVweDtcbn1cblxuLmljb24tZXllIHtcbiAgbWFyZ2luLXRvcDogMzZweDtcbn1cbiJdfQ== */");

/***/ }),

/***/ "Mc1n":
/*!******************************************************************************!*\
  !*** ./src/app/pages/dashboard/perfil/cambiar-clave/cambiar-clave.module.ts ***!
  \******************************************************************************/
/*! exports provided: CambiarClavePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CambiarClavePageModule", function() { return CambiarClavePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _cambiar_clave_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./cambiar-clave-routing.module */ "RMoF");
/* harmony import */ var _cambiar_clave_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./cambiar-clave.page */ "Nb5c");
/* harmony import */ var src_app_components_header_header_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/header/header.component */ "2MiI");








let CambiarClavePageModule = class CambiarClavePageModule {
};
CambiarClavePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _cambiar_clave_routing_module__WEBPACK_IMPORTED_MODULE_5__["CambiarClavePageRoutingModule"]
        ],
        declarations: [
            _cambiar_clave_page__WEBPACK_IMPORTED_MODULE_6__["CambiarClavePage"],
            src_app_components_header_header_component__WEBPACK_IMPORTED_MODULE_7__["HeaderComponent"]
        ]
    })
], CambiarClavePageModule);



/***/ }),

/***/ "Nb5c":
/*!****************************************************************************!*\
  !*** ./src/app/pages/dashboard/perfil/cambiar-clave/cambiar-clave.page.ts ***!
  \****************************************************************************/
/*! exports provided: CambiarClavePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CambiarClavePage", function() { return CambiarClavePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_cambiar_clave_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./cambiar-clave.page.html */ "eWSN");
/* harmony import */ var _cambiar_clave_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./cambiar-clave.page.scss */ "HTJu");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_services_usuario_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/usuario.service */ "on2l");





let CambiarClavePage = class CambiarClavePage {
    /*camposClave: object = {
      nuevaClave: 'pasare1234',
      repetirClave: '111111',
      claveActual: '111111'
    };*/
    constructor(_usuarioService) {
        this._usuarioService = _usuarioService;
        this.titulo = 'Cambiar Clave';
        this.activarClave = true;
        this.hide = true;
        this.hide1 = true;
        this.hide2 = true;
    }
    ngOnInit() {
    }
    cambiarClave(camposClave) {
        //console.log(this.camposClave)
        this._usuarioService.nuevaClave(camposClave.subscribe(data => {
            console.log(data);
            //this.dismissLoading();
        }, error => {
            console.log(error);
            //this.dismissLoading();
            //this.mensaje('Opss.. ocurrio un error');
        }));
    }
    mostrarPassword() { }
    saveChanges() {
    }
};
CambiarClavePage.ctorParameters = () => [
    { type: src_app_services_usuario_service__WEBPACK_IMPORTED_MODULE_4__["UsuarioService"] }
];
CambiarClavePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-cambiar-clave',
        template: _raw_loader_cambiar_clave_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_cambiar_clave_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], CambiarClavePage);



/***/ }),

/***/ "RMoF":
/*!**************************************************************************************!*\
  !*** ./src/app/pages/dashboard/perfil/cambiar-clave/cambiar-clave-routing.module.ts ***!
  \**************************************************************************************/
/*! exports provided: CambiarClavePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CambiarClavePageRoutingModule", function() { return CambiarClavePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _cambiar_clave_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./cambiar-clave.page */ "Nb5c");




const routes = [
    {
        path: '',
        component: _cambiar_clave_page__WEBPACK_IMPORTED_MODULE_3__["CambiarClavePage"]
    }
];
let CambiarClavePageRoutingModule = class CambiarClavePageRoutingModule {
};
CambiarClavePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], CambiarClavePageRoutingModule);



/***/ }),

/***/ "eWSN":
/*!********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/dashboard/perfil/cambiar-clave/cambiar-clave.page.html ***!
  \********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\n  <app-header [titulo]=\"titulo\"></app-header>\n</ion-header>\n\n<ion-content>\n  <div class=\"container\">\n    <form>\n      <ion-item class=\"ion-no-padding\">\n        <ion-label position=\"stacked\" class=\"form-label\">Clave actual<b style=\"color:red;\">*</b></ion-label>\n        <ion-input #input1 type=\"password\" [type]=\"hide ? 'password' : 'text' \"></ion-input>\n        <ion-fab horizontal=\"end\" class=\"icon-eye\" *ngIf=\"input1.value\">\n          <ion-icon [name]=\" hide ? 'eye-outline' : 'eye-off-outline'\" class=\"icon\" color=\"medium\"\n            (click)=\"hide = !hide\"></ion-icon>\n        </ion-fab>\n      </ion-item>\n\n      <ion-item class=\"ion-no-padding\">\n        <ion-label position=\"stacked\" class=\"form-label\">Nueva clave<b style=\"color:red;\">*</b></ion-label>\n        <ion-input #input2 [type]=\"hide1 ? 'password' : 'text' \"></ion-input>\n        <ion-fab horizontal=\"end\" class=\"icon-eye\" *ngIf=\"input2.value\">\n          <ion-icon [name]=\"hide1 ? 'eye-outline' : 'eye-off-outline'\" class=\"icon\" color=\"medium\"\n            (click)=\"hide1 = !hide1\"></ion-icon>\n        </ion-fab>\n      </ion-item>\n\n      <ion-item class=\"ion-no-padding\">\n        <ion-label position=\"stacked\" class=\"form-label\">Repetir clave<b style=\"color:red;\">*</b></ion-label>\n        <ion-input #input3 [type]=\"hide2 ? 'password' : 'text' \"></ion-input>\n        <ion-fab horizontal=\"end\" class=\"icon-eye\" *ngIf=\"input3.value\">\n          <ion-icon [name]=\"hide2 ? 'eye-outline' : 'eye-off-outline'\" class=\"icon\" color=\"medium\"\n            (click)=\"hide2 = !hide2\">\n          </ion-icon>\n        </ion-fab>\n      </ion-item>\n    </form>\n  </div>\n</ion-content>\n<ion-footer class=\"ion-padding-start ion-padding-end\">\n  <ion-button (click)=\"saveChanges()\" expand=\"block\"  color=\"primary\">\n    Guardar cambios\n  </ion-button>\n</ion-footer>");

/***/ })

}]);
//# sourceMappingURL=cambiar-clave-cambiar-clave-module-es2015.js.map