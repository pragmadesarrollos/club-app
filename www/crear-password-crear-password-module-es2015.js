(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["crear-password-crear-password-module"],{

/***/ "4UjQ":
/*!******************************************************************************!*\
  !*** ./src/app/pages/acceso/crear-password/crear-password-routing.module.ts ***!
  \******************************************************************************/
/*! exports provided: CrearPasswordPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CrearPasswordPageRoutingModule", function() { return CrearPasswordPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _crear_password_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./crear-password.page */ "aPE7");




const routes = [
    {
        path: '',
        component: _crear_password_page__WEBPACK_IMPORTED_MODULE_3__["CrearPasswordPage"]
    }
];
let CrearPasswordPageRoutingModule = class CrearPasswordPageRoutingModule {
};
CrearPasswordPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], CrearPasswordPageRoutingModule);



/***/ }),

/***/ "ZETh":
/*!**********************************************************************!*\
  !*** ./src/app/pages/acceso/crear-password/crear-password.page.scss ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-toolbar {\n  --background: #6b64f8;\n}\n\nion-back-button {\n  --color: white !important;\n}\n\nion-content {\n  --height: 100%;\n  --background: linear-gradient(0deg, #f4f3f3 50%, #6b64f8 50%);\n}\n\n#container {\n  text-align: center;\n  position: absolute;\n  left: 0;\n  right: 0;\n  top: 50%;\n  transform: translateY(-50%);\n}\n\nimg {\n  width: 150px;\n}\n\n.toolbar {\n  --background: #f4f3f3;\n}\n\n.estasCargado {\n  font-family: \"Roboto\", sans-serif;\n  font-style: normal;\n  font-weight: 500;\n  font-size: 26px;\n  line-height: 31px;\n  text-align: center;\n  color: #FFFFFF;\n}\n\n.textoAcceso {\n  margin-top: 10px;\n  font-family: \"Roboto\", sans-serif;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 16px;\n  line-height: 19px;\n  text-align: center;\n  color: #ffffff;\n}\n\n.inputCorreo {\n  margin-left: 10px;\n  margin-right: 10px;\n}\n\n.btnIngresar {\n  margin-left: 10px;\n  margin-right: 10px;\n}\n\n.input-group-text {\n  border-style: none;\n  background-color: white;\n}\n\n.form-control:focus {\n  border-color: rgba(255, 255, 255, 0.9);\n  box-shadow: inset 0 1px 1px rgba(255, 255, 255, 0.3), 0 0 8px rgba(255, 255, 255, 0.3);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL2NyZWFyLXBhc3N3b3JkLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHFCQUFBO0FBQ0o7O0FBQ0E7RUFDSSx5QkFBQTtBQUVKOztBQUFBO0VBQ0ksY0FBQTtFQUNBLDZEQUFBO0FBR0o7O0FBQUE7RUFDSSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsT0FBQTtFQUNBLFFBQUE7RUFDQSxRQUFBO0VBQ0EsMkJBQUE7QUFHSjs7QUFBQTtFQUNJLFlBQUE7QUFHSjs7QUFBQTtFQUNJLHFCQUFBO0FBR0o7O0FBQUE7RUFDSSxpQ0FBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7QUFHSjs7QUFBQTtFQUNJLGdCQUFBO0VBQ0EsaUNBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0FBR0o7O0FBQUU7RUFDRSxpQkFBQTtFQUNBLGtCQUFBO0FBR0o7O0FBQUU7RUFDRSxpQkFBQTtFQUNBLGtCQUFBO0FBR0o7O0FBQUU7RUFDRSxrQkFBQTtFQUNBLHVCQUFBO0FBR0o7O0FBQUU7RUFDRSxzQ0FBQTtFQUNBLHNGQUFBO0FBR0oiLCJmaWxlIjoiY3JlYXItcGFzc3dvcmQucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLXRvb2xiYXIge1xuICAgIC0tYmFja2dyb3VuZDogIzZiNjRmODtcbn1cbmlvbi1iYWNrLWJ1dHRvbiB7XG4gICAgLS1jb2xvcjogd2hpdGUgIWltcG9ydGFudDtcbn1cbmlvbi1jb250ZW50IHtcbiAgICAtLWhlaWdodDogMTAwJTtcbiAgICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgwZGVnLCAjZjRmM2YzIDUwJSwgIzZiNjRmOCA1MCUpO1xufVxuXG4jY29udGFpbmVyIHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGxlZnQ6IDA7XG4gICAgcmlnaHQ6IDA7XG4gICAgdG9wOiA1MCU7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01MCUpO1xufVxuXG5pbWcge1xuICAgIHdpZHRoOiAxNTBweDtcbn1cblxuLnRvb2xiYXIge1xuICAgIC0tYmFja2dyb3VuZDogI2Y0ZjNmMztcbn1cblxuLmVzdGFzQ2FyZ2FkbyB7XG4gICAgZm9udC1mYW1pbHk6IFwiUm9ib3RvXCIsIHNhbnMtc2VyaWY7XG4gICAgZm9udC1zdHlsZTogbm9ybWFsO1xuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgZm9udC1zaXplOiAyNnB4O1xuICAgIGxpbmUtaGVpZ2h0OiAzMXB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBjb2xvcjogI0ZGRkZGRjtcbn1cblxuLnRleHRvQWNjZXNvIHtcbiAgICBtYXJnaW4tdG9wOiAxMHB4O1xuICAgIGZvbnQtZmFtaWx5OiBcIlJvYm90b1wiLCBzYW5zLXNlcmlmO1xuICAgIGZvbnQtc3R5bGU6IG5vcm1hbDtcbiAgICBmb250LXdlaWdodDogbm9ybWFsO1xuICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICBsaW5lLWhlaWdodDogMTlweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgY29sb3I6ICNmZmZmZmY7XG4gIH1cblxuICAuaW5wdXRDb3JyZW8ge1xuICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICAgIG1hcmdpbi1yaWdodDogMTBweDtcbiAgfVxuXG4gIC5idG5JbmdyZXNhciB7XG4gICAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gICAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xuICB9XG5cbiAgLmlucHV0LWdyb3VwLXRleHQge1xuICAgIGJvcmRlci1zdHlsZTogbm9uZTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgfVxuXG4gIC5mb3JtLWNvbnRyb2w6Zm9jdXMge1xuICAgIGJvcmRlci1jb2xvcjogcmdiYSgyNTUsMjU1LDI1NSwgMC45KTtcbiAgICBib3gtc2hhZG93OiBpbnNldCAwIDFweCAxcHggcmdiYSgyNTUsMjU1LDI1NSwgMC4zKSwgMCAwIDhweCByZ2JhKDI1NSwyNTUsMjU1LCAwLjMpO1xuICB9Il19 */");

/***/ }),

/***/ "aPE7":
/*!********************************************************************!*\
  !*** ./src/app/pages/acceso/crear-password/crear-password.page.ts ***!
  \********************************************************************/
/*! exports provided: CrearPasswordPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CrearPasswordPage", function() { return CrearPasswordPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_crear_password_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./crear-password.page.html */ "nixE");
/* harmony import */ var _crear_password_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./crear-password.page.scss */ "ZETh");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var src_app_services_usuario_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/usuario.service */ "on2l");








let CrearPasswordPage = class CrearPasswordPage {
    constructor(_usuarioService, router, fb, toastController, loadingController) {
        this._usuarioService = _usuarioService;
        this.router = router;
        this.fb = fb;
        this.toastController = toastController;
        this.loadingController = loadingController;
        this.form = this.fb.group({
            correo: [{ value: this._usuarioService.nombreUsuario, disabled: true }, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            repetirPassword: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
        });
    }
    ngOnInit() {
    }
    ingresar() {
        // Obtenemos datos
        const correo = this._usuarioService.nombreUsuario;
        const password = this.form.value.password;
        const repetirPassword = this.form.value.repetirPassword;
        // Validamos que las passwords sean iguales
        if (password !== repetirPassword) {
            this.presentToast('Las contraseñas no coinciden');
            return;
        }
        // Validamos que la password tenga al menos 6 caracteres
        if (password.length < 6) {
            this.presentToast('La contraseña debe contener al menos 6 caracteres');
            return;
        }
        const usuario = {
            email: correo,
            password: password,
        };
        // Enviamos email y password para almacenarlo en FIREBASE
        this.presentLoading();
        this._usuarioService.registrarFirebase(usuario).subscribe(data => {
            this.loadingController.dismiss();
            this.presentToast(`El usuario ${correo} fue registrado con exito!`);
            this.router.navigateByUrl('/login');
        }, error => {
            console.log(error);
            this.presentToast('Opss.. ocurrio un error!');
        });
    }
    // Si el usuario vuelve a la pantalla con el boton 'volver' (del cel, no de la app), limpiamos el password
    ionViewWillEnter() {
        this.form.patchValue({
            password: '',
            repetirPassword: '',
        });
    }
    presentToast(mensaje) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: mensaje,
                color: 'dark',
                duration: 3000
            });
            toast.present();
        });
    }
    presentLoading() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const loading = yield this.loadingController.create({
                cssClass: 'my-custom-class',
                message: 'Espere...',
            });
            yield loading.present();
        });
    }
};
CrearPasswordPage.ctorParameters = () => [
    { type: src_app_services_usuario_service__WEBPACK_IMPORTED_MODULE_7__["UsuarioService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ToastController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["LoadingController"] }
];
CrearPasswordPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-crear-password',
        template: _raw_loader_crear_password_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_crear_password_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], CrearPasswordPage);



/***/ }),

/***/ "hy4E":
/*!**********************************************************************!*\
  !*** ./src/app/pages/acceso/crear-password/crear-password.module.ts ***!
  \**********************************************************************/
/*! exports provided: CrearPasswordPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CrearPasswordPageModule", function() { return CrearPasswordPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _crear_password_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./crear-password-routing.module */ "4UjQ");
/* harmony import */ var _crear_password_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./crear-password.page */ "aPE7");








let CrearPasswordPageModule = class CrearPasswordPageModule {
};
CrearPasswordPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _crear_password_routing_module__WEBPACK_IMPORTED_MODULE_5__["CrearPasswordPageRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"]
        ],
        declarations: [_crear_password_page__WEBPACK_IMPORTED_MODULE_6__["CrearPasswordPage"]]
    })
], CrearPasswordPageModule);



/***/ }),

/***/ "nixE":
/*!************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/acceso/crear-password/crear-password.page.html ***!
  \************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/\" text=\"\" color>\n      </ion-back-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div id=\"container\">\n    <div style=\"margin-top: -50px;\">   \n      <img src=\"/assets/img/splash/imgCrearPassword.png\">\n    </div>\n    <div class=\"estasCargado\">\n      ¡Estas cargado!\n    </div>\n    <div class=\"textoAcceso\">\n      Estas registrado, CREA TU CONTRASEÑA \n    </div>\n    <form [formGroup]=\"form\">\n      <div class=\"input-group input-group-lg inputCorreo mt-4\">\n        <span class=\"input-group-text\" style=\"background-color: #e9ecef;\" id=\"inputGroup-sizing-lg\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"20\" height=\"20\" fill=\"currentColor\" class=\"bi bi-briefcase\" viewBox=\"0 0 16 16\">\n          <path d=\"M6.5 1A1.5 1.5 0 0 0 5 2.5V3H1.5A1.5 1.5 0 0 0 0 4.5v8A1.5 1.5 0 0 0 1.5 14h13a1.5 1.5 0 0 0 1.5-1.5v-8A1.5 1.5 0 0 0 14.5 3H11v-.5A1.5 1.5 0 0 0 9.5 1h-3zm0 1h3a.5.5 0 0 1 .5.5V3H6v-.5a.5.5 0 0 1 .5-.5zm1.886 6.914L15 7.151V12.5a.5.5 0 0 1-.5.5h-13a.5.5 0 0 1-.5-.5V7.15l6.614 1.764a1.5 1.5 0 0 0 .772 0zM1.5 4h13a.5.5 0 0 1 .5.5v1.616L8.129 7.948a.5.5 0 0 1-.258 0L1 6.116V4.5a.5.5 0 0 1 .5-.5z\"/>\n        </svg></span>\n        <input type=\"text\" class=\"form-control\" formControlName=\"correo\" name=\"correo\" \n        placeholder=\"Correo electronico\" style=\"border-style: none; height: 4rem; ; margin-right: 20px\">\n      </div>\n      <div class=\"input-group input-group-lg inputCorreo mt-2\">\n        <span class=\"input-group-text\" id=\"inputGroup-sizing-lg\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"20\" height=\"20\" fill=\"currentColor\" class=\"bi bi-key\" viewBox=\"0 0 16 16\">\n          <path d=\"M0 8a4 4 0 0 1 7.465-2H14a.5.5 0 0 1 .354.146l1.5 1.5a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0L13 9.207l-.646.647a.5.5 0 0 1-.708 0L11 9.207l-.646.647a.5.5 0 0 1-.708 0L9 9.207l-.646.647A.5.5 0 0 1 8 10h-.535A4 4 0 0 1 0 8zm4-3a3 3 0 1 0 2.712 4.285A.5.5 0 0 1 7.163 9h.63l.853-.854a.5.5 0 0 1 .708 0l.646.647.646-.647a.5.5 0 0 1 .708 0l.646.647.646-.647a.5.5 0 0 1 .708 0l.646.647.793-.793-1-1h-6.63a.5.5 0 0 1-.451-.285A3 3 0 0 0 4 5z\"/>\n          <path d=\"M4 8a1 1 0 1 1-2 0 1 1 0 0 1 2 0z\"/>\n        </svg></span>\n        <input type=\"password\" class=\"form-control\" formControlName=\"password\"  placeholder=\"Contraseña\" autocomplete=\"off\" style=\"margin-right: 20px;  border-style: none; height: 4rem;\">\n        \n      </div>\n      <div class=\"input-group input-group-lg inputCorreo mt-2\">\n        <span class=\"input-group-text\" id=\"inputGroup-sizing-lg\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"20\" height=\"20\" fill=\"currentColor\" class=\"bi bi-key\" viewBox=\"0 0 16 16\">\n          <path d=\"M0 8a4 4 0 0 1 7.465-2H14a.5.5 0 0 1 .354.146l1.5 1.5a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0L13 9.207l-.646.647a.5.5 0 0 1-.708 0L11 9.207l-.646.647a.5.5 0 0 1-.708 0L9 9.207l-.646.647A.5.5 0 0 1 8 10h-.535A4 4 0 0 1 0 8zm4-3a3 3 0 1 0 2.712 4.285A.5.5 0 0 1 7.163 9h.63l.853-.854a.5.5 0 0 1 .708 0l.646.647.646-.647a.5.5 0 0 1 .708 0l.646.647.646-.647a.5.5 0 0 1 .708 0l.646.647.793-.793-1-1h-6.63a.5.5 0 0 1-.451-.285A3 3 0 0 0 4 5z\"/>\n          <path d=\"M4 8a1 1 0 1 1-2 0 1 1 0 0 1 2 0z\"/>\n        </svg></span>\n        <input type=\"password\" class=\"form-control\" formControlName=\"repetirPassword\" autocomplete=\"off\" placeholder=\"Repetir contraseña\" style=\"margin-right: 20px;  border-style: none; height: 4rem;\">\n        \n      </div>\n      <ion-button type=\"submit\" (click)=\"ingresar()\" [disabled]=\"form.invalid\" size=\"large\" class=\"btnIngresar mt-2\" color=\"proclub\" expand=\"block\">INGRESAR</ion-button>\n    </form>\n  </div>\n</ion-content>\n\n<ion-footer class=\"ion-no-border\">\n  <ion-toolbar class=\"ion-text-center toolbar\">\n    <img routerLink=\"/login\" src=\"/assets/img/splash/ProClubLogo.png\">\n  </ion-toolbar>\n</ion-footer>\n");

/***/ })

}]);
//# sourceMappingURL=crear-password-crear-password-module-es2015.js.map