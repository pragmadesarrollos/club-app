(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-dashboard-mensaje-verificacion-mensaje-verificacion-module"],{

/***/ "2Yzq":
/*!*************************************************************************************!*\
  !*** ./src/app/pages/dashboard/mensaje-verificacion/mensaje-verificacion.page.scss ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("#container {\n  text-align: center;\n  position: absolute;\n  left: 0;\n  right: 0;\n  top: 50%;\n  transform: translateY(-50%);\n}\n\nion-content {\n  --height: 100%;\n  --background: var(--ion-color-primary);\n}\n\n.texto {\n  font-family: \"Roboto\", sans-serif;\n  font-style: normal;\n  font-weight: 500;\n  font-size: 24px;\n  line-height: 29px;\n  align-items: center;\n  text-align: center;\n  color: #ffffff;\n  margin-top: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL21lbnNhamUtdmVyaWZpY2FjaW9uLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxPQUFBO0VBQ0EsUUFBQTtFQUNBLFFBQUE7RUFDQSwyQkFBQTtBQUNGOztBQUVBO0VBQ0UsY0FBQTtFQUNBLHNDQUFBO0FBQ0Y7O0FBRUE7RUFDRSxpQ0FBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0FBQ0YiLCJmaWxlIjoibWVuc2FqZS12ZXJpZmljYWNpb24ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiI2NvbnRhaW5lciB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiAwO1xuICByaWdodDogMDtcbiAgdG9wOiA1MCU7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgtNTAlKTtcbn1cblxuaW9uLWNvbnRlbnQge1xuICAtLWhlaWdodDogMTAwJTtcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG59XG5cbi50ZXh0byB7XG4gIGZvbnQtZmFtaWx5OiBcIlJvYm90b1wiLCBzYW5zLXNlcmlmO1xuICBmb250LXN0eWxlOiBub3JtYWw7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIGZvbnQtc2l6ZTogMjRweDtcbiAgbGluZS1oZWlnaHQ6IDI5cHg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgY29sb3I6ICNmZmZmZmY7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG59XG4iXX0= */");

/***/ }),

/***/ "9umO":
/*!***************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/dashboard/mensaje-verificacion/mensaje-verificacion.page.html ***!
  \***************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n  <div id=\"container\">\n    <div style=\"margin-top:-60px;\">\n      <img style=\"width: 60%;\" src=\"/assets/img/splash/confirmacionRojo.png\">\n    </div>\n    <div class=\"texto\">\n      ¡Tu cuenta se verificó\n    </div>\n    <div class=\"texto\">\n      con éxito!\n    </div>\n  </div>\n</ion-content>\n");

/***/ }),

/***/ "dZQC":
/*!*************************************************************************************!*\
  !*** ./src/app/pages/dashboard/mensaje-verificacion/mensaje-verificacion.module.ts ***!
  \*************************************************************************************/
/*! exports provided: MensajeVerificacionPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MensajeVerificacionPageModule", function() { return MensajeVerificacionPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _mensaje_verificacion_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./mensaje-verificacion-routing.module */ "fcEd");
/* harmony import */ var _mensaje_verificacion_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./mensaje-verificacion.page */ "xf+c");







let MensajeVerificacionPageModule = class MensajeVerificacionPageModule {
};
MensajeVerificacionPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _mensaje_verificacion_routing_module__WEBPACK_IMPORTED_MODULE_5__["MensajeVerificacionPageRoutingModule"]
        ],
        declarations: [_mensaje_verificacion_page__WEBPACK_IMPORTED_MODULE_6__["MensajeVerificacionPage"]]
    })
], MensajeVerificacionPageModule);



/***/ }),

/***/ "fcEd":
/*!*********************************************************************************************!*\
  !*** ./src/app/pages/dashboard/mensaje-verificacion/mensaje-verificacion-routing.module.ts ***!
  \*********************************************************************************************/
/*! exports provided: MensajeVerificacionPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MensajeVerificacionPageRoutingModule", function() { return MensajeVerificacionPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _mensaje_verificacion_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./mensaje-verificacion.page */ "xf+c");




const routes = [
    {
        path: '',
        component: _mensaje_verificacion_page__WEBPACK_IMPORTED_MODULE_3__["MensajeVerificacionPage"]
    }
];
let MensajeVerificacionPageRoutingModule = class MensajeVerificacionPageRoutingModule {
};
MensajeVerificacionPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], MensajeVerificacionPageRoutingModule);



/***/ }),

/***/ "xf+c":
/*!***********************************************************************************!*\
  !*** ./src/app/pages/dashboard/mensaje-verificacion/mensaje-verificacion.page.ts ***!
  \***********************************************************************************/
/*! exports provided: MensajeVerificacionPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MensajeVerificacionPage", function() { return MensajeVerificacionPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_mensaje_verificacion_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./mensaje-verificacion.page.html */ "9umO");
/* harmony import */ var _mensaje_verificacion_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./mensaje-verificacion.page.scss */ "2Yzq");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");





let MensajeVerificacionPage = class MensajeVerificacionPage {
    constructor(router) {
        this.router = router;
    }
    ngOnInit() {
        setTimeout(() => {
            this.router.navigateByUrl('/menu');
        }, 4000);
    }
};
MensajeVerificacionPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
MensajeVerificacionPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-mensaje-verificacion',
        template: _raw_loader_mensaje_verificacion_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_mensaje_verificacion_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], MensajeVerificacionPage);



/***/ })

}]);
//# sourceMappingURL=pages-dashboard-mensaje-verificacion-mensaje-verificacion-module-es2015.js.map