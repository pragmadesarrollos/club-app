(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["notificacion-enviada-notificacion-enviada-module"],{

/***/ "/fuf":
/*!****************************************************************************************************!*\
  !*** ./src/app/pages/dashboard/notificaciones/notificacion-enviada/notificacion-enviada.module.ts ***!
  \****************************************************************************************************/
/*! exports provided: NotificacionEnviadaPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificacionEnviadaPageModule", function() { return NotificacionEnviadaPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _notificacion_enviada_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./notificacion-enviada-routing.module */ "aKUi");
/* harmony import */ var _notificacion_enviada_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./notificacion-enviada.page */ "xq2J");
/* harmony import */ var src_app_components_header_header_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/header/header.component */ "2MiI");








let NotificacionEnviadaPageModule = class NotificacionEnviadaPageModule {
};
NotificacionEnviadaPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _notificacion_enviada_routing_module__WEBPACK_IMPORTED_MODULE_5__["NotificacionEnviadaPageRoutingModule"]
        ],
        declarations: [
            _notificacion_enviada_page__WEBPACK_IMPORTED_MODULE_6__["NotificacionEnviadaPage"],
            src_app_components_header_header_component__WEBPACK_IMPORTED_MODULE_7__["HeaderComponent"]
        ]
    })
], NotificacionEnviadaPageModule);



/***/ })

}]);
//# sourceMappingURL=notificacion-enviada-notificacion-enviada-module-es2015.js.map