(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["components-splash-generico-splash-generico-module"], {
    /***/
    "I6GX":
    /*!************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/splash-generico/splash-generico.page.html ***!
      \************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function I6GX(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content class=\"primary-bg\">\n  <ion-row class=\"ion-align-items-center ion-margin-top\">\n    <br>\n    <br>\n    <ion-col size=\"12\" class=\"ion-margin-top\">\n      <ion-img class=\"img\" [src]=\"this.masterService.splash.img\"></ion-img>\n    </ion-col>\n    <br>\n    <ion-col size=\"12\" class=\"ion-text-center ion-padding\">\n      <!-- <h2>{{this.masterService.splash.titulo}}</h2> \n      <h2>¡Turno confirmado con éxito!</h2>-->\n      <h2>{{this.masterService.splash.titulo}}</h2>\n      <!-- <h6>{{this.masterService.splash.subtitulo}}</h6>-->\n    </ion-col>\n  </ion-row>\n</ion-content>\n";
      /***/
    },

    /***/
    "UBQ5":
    /*!**********************************************************************!*\
      !*** ./src/app/components/splash-generico/splash-generico.module.ts ***!
      \**********************************************************************/

    /*! exports provided: SplashGenericoPageModule */

    /***/
    function UBQ5(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SplashGenericoPageModule", function () {
        return SplashGenericoPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _splash_generico_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./splash-generico-routing.module */
      "xS7q");
      /* harmony import */


      var _splash_generico_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./splash-generico.page */
      "f9TE");

      var SplashGenericoPageModule = function SplashGenericoPageModule() {
        _classCallCheck(this, SplashGenericoPageModule);
      };

      SplashGenericoPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _splash_generico_routing_module__WEBPACK_IMPORTED_MODULE_5__["SplashGenericoPageRoutingModule"]],
        declarations: [_splash_generico_page__WEBPACK_IMPORTED_MODULE_6__["SplashGenericoPage"]]
      })], SplashGenericoPageModule);
      /***/
    },

    /***/
    "YhIc":
    /*!**********************************************************************!*\
      !*** ./src/app/components/splash-generico/splash-generico.page.scss ***!
      \**********************************************************************/

    /*! exports provided: default */

    /***/
    function YhIc(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".img {\n  width: 200px;\n  margin: 0 auto;\n  margin-top: 25%;\n  margin-bottom: 5%;\n}\n\n.primary-bg {\n  background: var(--ion-color-primary);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NwbGFzaC1nZW5lcmljby5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxZQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQUNKOztBQUVBO0VBQ0ksb0NBQUE7QUFDSiIsImZpbGUiOiJzcGxhc2gtZ2VuZXJpY28ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmltZyB7XG4gICAgd2lkdGg6IDIwMHB4O1xuICAgIG1hcmdpbjogMCBhdXRvO1xuICAgIG1hcmdpbi10b3A6IDI1JTtcbiAgICBtYXJnaW4tYm90dG9tOiA1JTtcbn1cblxuLnByaW1hcnktYmd7XG4gICAgYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xufSJdfQ== */";
      /***/
    },

    /***/
    "f9TE":
    /*!********************************************************************!*\
      !*** ./src/app/components/splash-generico/splash-generico.page.ts ***!
      \********************************************************************/

    /*! exports provided: SplashGenericoPage */

    /***/
    function f9TE(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SplashGenericoPage", function () {
        return SplashGenericoPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _raw_loader_splash_generico_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! raw-loader!./splash-generico.page.html */
      "I6GX");
      /* harmony import */


      var _splash_generico_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./splash-generico.page.scss */
      "YhIc");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var src_app_helpers_master_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! src/app/helpers/master.service */
      "4fYM");

      var SplashGenericoPage = /*#__PURE__*/function () {
        function SplashGenericoPage(masterService, router) {
          _classCallCheck(this, SplashGenericoPage);

          this.masterService = masterService;
          this.router = router;
        }

        _createClass(SplashGenericoPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this = this;

            setTimeout(function () {
              _this.router.navigate([_this.masterService.splash.redirecTo]);
            }, 3000);
          }
        }]);

        return SplashGenericoPage;
      }();

      SplashGenericoPage.ctorParameters = function () {
        return [{
          type: src_app_helpers_master_service__WEBPACK_IMPORTED_MODULE_5__["MasterService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
        }];
      };

      SplashGenericoPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-splash-generico',
        template: _raw_loader_splash_generico_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_splash_generico_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
      })], SplashGenericoPage);
      /***/
    },

    /***/
    "xS7q":
    /*!******************************************************************************!*\
      !*** ./src/app/components/splash-generico/splash-generico-routing.module.ts ***!
      \******************************************************************************/

    /*! exports provided: SplashGenericoPageRoutingModule */

    /***/
    function xS7q(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SplashGenericoPageRoutingModule", function () {
        return SplashGenericoPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _splash_generico_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./splash-generico.page */
      "f9TE");

      var routes = [{
        path: '',
        component: _splash_generico_page__WEBPACK_IMPORTED_MODULE_3__["SplashGenericoPage"]
      }];

      var SplashGenericoPageRoutingModule = function SplashGenericoPageRoutingModule() {
        _classCallCheck(this, SplashGenericoPageRoutingModule);
      };

      SplashGenericoPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], SplashGenericoPageRoutingModule);
      /***/
    }
  }]);
})();
//# sourceMappingURL=components-splash-generico-splash-generico-module-es5.js.map