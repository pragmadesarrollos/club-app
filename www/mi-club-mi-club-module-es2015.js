(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["mi-club-mi-club-module"],{

/***/ "8bwn":
/*!**************************************************************************!*\
  !*** ./src/app/pages/dashboard/perfil/mi-club/mi-club-routing.module.ts ***!
  \**************************************************************************/
/*! exports provided: MiClubPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MiClubPageRoutingModule", function() { return MiClubPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _mi_club_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./mi-club.page */ "zKZY");




const routes = [
    {
        path: '',
        component: _mi_club_page__WEBPACK_IMPORTED_MODULE_3__["MiClubPage"]
    }
];
let MiClubPageRoutingModule = class MiClubPageRoutingModule {
};
MiClubPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], MiClubPageRoutingModule);



/***/ }),

/***/ "Qn2l":
/*!******************************************************************!*\
  !*** ./src/app/pages/dashboard/perfil/mi-club/mi-club.page.scss ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".sup {\n  background-color: var(--ion-color-primary);\n  border-radius: 0px 0px 24px 24px;\n  height: 56px;\n  display: flex;\n  flex-wrap: wrap;\n  flex-direction: row;\n  justify-content: center;\n  align-items: stretch;\n}\n\n.img {\n  margin-top: 10px;\n}\n\n.titulo-contenedor {\n  display: flex;\n  flex-wrap: wrap;\n  flex-direction: row;\n  justify-content: center;\n  align-items: stretch;\n  margin-top: 50px;\n}\n\n.titulo {\n  font-weight: 700;\n  font-family: \"Roboto\", sans-serif;\n}\n\n.contenedor {\n  padding: 10px;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  margin-top: 30px;\n}\n\n.social-media {\n  display: flex;\n  justify-content: space-evenly;\n  margin-top: 50px;\n  padding: 80px;\n}\n\n.input-value {\n  padding: 6px 0;\n}\n\nion-item, ion-row {\n  width: 100%;\n}\n\n.capitalize-text {\n  text-transform: capitalize;\n}\n\n.social-medias {\n  width: 100%;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n\n.fab-button-social {\n  width: 30px;\n  height: 30px;\n  box-shadow: none !important;\n  border-radius: 50%;\n  --background: var(--ion-color-secondary-contrast) !important;\n  margin-right: 10px;\n}\n\n.fab-button-social ion-icon {\n  color: var(--ion-color-secondary);\n}\n\nion-fab-button {\n  --box-shadow:none;\n}\n\nion-fab-button a {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL21pLWNsdWIucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsMENBQUE7RUFDQSxnQ0FBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxvQkFBQTtBQUNGOztBQUVBO0VBQ0UsZ0JBQUE7QUFDRjs7QUFFQTtFQUNFLGFBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLG9CQUFBO0VBQ0EsZ0JBQUE7QUFDRjs7QUFFQTtFQUNFLGdCQUFBO0VBQ0EsaUNBQUE7QUFDRjs7QUFJQTtFQUNFLGFBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7QUFERjs7QUFJQTtFQUNFLGFBQUE7RUFDQSw2QkFBQTtFQUVBLGdCQUFBO0VBQ0EsYUFBQTtBQUZGOztBQUtBO0VBQ0UsY0FBQTtBQUZGOztBQUtBO0VBQ0UsV0FBQTtBQUZGOztBQUtBO0VBQ0UsMEJBQUE7QUFGRjs7QUFLQTtFQUNFLFdBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtBQUZGOztBQUtBO0VBQ0UsV0FBQTtFQUNBLFlBQUE7RUFDQSwyQkFBQTtFQUNBLGtCQUFBO0VBQ0EsNERBQUE7RUFDQSxrQkFBQTtBQUZGOztBQUtBO0VBRUUsaUNBQUE7QUFIRjs7QUFNQTtFQUNFLGlCQUFBO0FBSEY7O0FBTUE7RUFDRSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtBQUhGIiwiZmlsZSI6Im1pLWNsdWIucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnN1cCB7XG4gIGJhY2tncm91bmQtY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbiAgYm9yZGVyLXJhZGl1czogMHB4IDBweCAyNHB4IDI0cHg7XG4gIGhlaWdodDogNTZweDtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC13cmFwOiB3cmFwO1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IHN0cmV0Y2g7XG59XG5cbi5pbWcge1xuICBtYXJnaW4tdG9wOiAxMHB4O1xufVxuXG4udGl0dWxvLWNvbnRlbmVkb3Ige1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LXdyYXA6IHdyYXA7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogc3RyZXRjaDtcbiAgbWFyZ2luLXRvcDogNTBweDtcbn1cblxuLnRpdHVsbyB7XG4gIGZvbnQtd2VpZ2h0OiA3MDA7XG4gIGZvbnQtZmFtaWx5OiAnUm9ib3RvJyxcbiAgICBzYW5zLXNlcmlmO1xuICA7XG59XG5cbi5jb250ZW5lZG9yIHtcbiAgcGFkZGluZzogMTBweDtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIG1hcmdpbi10b3A6IDMwcHg7XG59XG5cbi5zb2NpYWwtbWVkaWEge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWV2ZW5seTtcblxuICBtYXJnaW4tdG9wOiA1MHB4O1xuICBwYWRkaW5nOiA4MHB4O1xufVxuXG4uaW5wdXQtdmFsdWV7XG4gIHBhZGRpbmc6IDZweCAwO1xufVxuXG5pb24taXRlbSwgaW9uLXJvd3tcbiAgd2lkdGg6IDEwMCU7XG59XG5cbi5jYXBpdGFsaXplLXRleHR7XG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xufVxuXG4uc29jaWFsLW1lZGlhc3tcbiAgd2lkdGg6IDEwMCU7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuXG4uZmFiLWJ1dHRvbi1zb2NpYWx7XG4gIHdpZHRoOiAzMHB4O1xuICBoZWlnaHQ6IDMwcHg7XG4gIGJveC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnktY29udHJhc3QpICFpbXBvcnRhbnQ7XG4gIG1hcmdpbi1yaWdodDogMTBweDtcbn1cblxuLmZhYi1idXR0b24tc29jaWFsIGlvbi1pY29ue1xuXG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3Itc2Vjb25kYXJ5KTtcbn1cblxuaW9uLWZhYi1idXR0b24ge1xuICAtLWJveC1zaGFkb3c6bm9uZTtcbn1cblxuaW9uLWZhYi1idXR0b24gYXtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59Il19 */");

/***/ }),

/***/ "UUTp":
/*!******************************************************************!*\
  !*** ./src/app/pages/dashboard/perfil/mi-club/mi-club.module.ts ***!
  \******************************************************************/
/*! exports provided: MiClubPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MiClubPageModule", function() { return MiClubPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _mi_club_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./mi-club-routing.module */ "8bwn");
/* harmony import */ var _mi_club_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./mi-club.page */ "zKZY");
/* harmony import */ var src_app_components_header_header_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/header/header.component */ "2MiI");








let MiClubPageModule = class MiClubPageModule {
};
MiClubPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _mi_club_routing_module__WEBPACK_IMPORTED_MODULE_5__["MiClubPageRoutingModule"]
        ],
        declarations: [
            _mi_club_page__WEBPACK_IMPORTED_MODULE_6__["MiClubPage"],
            src_app_components_header_header_component__WEBPACK_IMPORTED_MODULE_7__["HeaderComponent"]
        ]
    })
], MiClubPageModule);



/***/ }),

/***/ "xydf":
/*!********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/dashboard/perfil/mi-club/mi-club.page.html ***!
  \********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\n  <app-header [titulo]=\"titulo\"></app-header>\n</ion-header>\n\n<ion-content>\n  <div class=\"sup\">\n    <img class=\"img\" [src]=\"club?.logo\" width=\"90\">\n  </div>\n\n  <div class=\"titulo-contenedor\">\n    <h3 class=\"titulo\">{{club?.nombre}}</h3>\n  </div>\n\n  <div class=\"social-medias\">\n    <ion-fab-button *ngIf=\"club?.facebook\" class=\"fab-button-social\">\n        <a href=\"club?.facebook\" target=\"_blank\">\n            <ion-icon name=\"logo-facebook\"></ion-icon>\n        </a>\n    </ion-fab-button>\n    \n    <ion-fab-button *ngIf=\"club?.instagram\" class=\"fab-button-social\">\n        <a href=\"club?.instagram\" target=\"_blank\">\n            <ion-icon name=\"logo-instagram\"></ion-icon>\n        </a>\n    </ion-fab-button>\n    <ion-fab-button *ngIf=\"club?.twitter\" class=\"fab-button-social\">\n        <a href=\"club?.twitter\" target=\"_blank\">\n            <ion-icon name=\"logo-twitter\"></ion-icon>\n        </a>\n    </ion-fab-button>\n    <ion-fab-button *ngIf=\"club?.email\" class=\"fab-button-social\">\n        <a href=\"mailto: {{club?.email}}\" target=\"_blank\">\n            <ion-icon name=\"mail\"></ion-icon>\n        </a>\n    </ion-fab-button>\n    <ion-fab-button *ngIf=\"club?.telefono\" class=\"fab-button-social\">\n        <a href=\"tel: {{club?.telefono}}\" target=\"_blank\">\n            <ion-icon name=\"call\"></ion-icon>\n        </a>\n    </ion-fab-button>\n    <ion-fab-button *ngIf=\"club?.telefono\" class=\"fab-button-social\">\n        <a href=\"https://api.whatsapp.com/send?phone={{club?.telefono}}\" target=\"_blank\">\n            <ion-icon name=\"logo-whatsapp\"></ion-icon>\n        </a>\n    </ion-fab-button>\n  </div>\n\n  <div class=\"contenedor\" *ngIf=\"club\">\n   \n      <ion-item class=\"ion-no-padding\">\n        <ion-label position=\"stacked\" class=\"form-label\">Domicilio</ion-label>\n        <div class=\"input-value\">{{club.direccion.calle || 'N/D'}}</div>\n      </ion-item>\n\n      <ion-row>\n        <ion-col>\n          <ion-item class=\"ion-no-padding\">\n            <ion-label position=\"stacked\" class=\"form-label\">Numero</ion-label>\n            <div class=\"input-value\">{{club.direccion.numero || 'N/D'}}</div>\n          </ion-item>\n        </ion-col>\n        <ion-col>\n          <ion-item class=\"ion-no-padding\">\n            <ion-label position=\"stacked\" class=\"form-label\">Código postal</ion-label>\n            <div class=\"input-value\">{{club.direccion.cp || 'N/D'}}</div>\n          </ion-item>\n        </ion-col>\n    \n      </ion-row>\n\n      <ion-item class=\"ion-no-padding\">\n        <ion-label position=\"stacked\" class=\"form-label\">Localidad</ion-label>\n        <div class=\"input-value capitalize-text\">{{club.direccion.localidad || 'N/D'}}, {{club.direccion.provincia.nombre || 'N/D'}}</div>\n      </ion-item>\n\n      <ion-item class=\"ion-no-padding\">\n        <ion-label position=\"stacked\" class=\"form-label\">Correo electrónico</ion-label>\n        <div class=\"input-value\">{{club.email || 'N/D'}}</div>\n      </ion-item>\n\n      <ion-item class=\"ion-no-padding\">\n        <ion-label position=\"stacked\" class=\"form-label\">Teléfono</ion-label>\n        <div class=\"input-value\">{{club.telefono || 'N/D'}}</div>\n      </ion-item>\n\n      \n    \n  </div>\n</ion-content>");

/***/ }),

/***/ "zKZY":
/*!****************************************************************!*\
  !*** ./src/app/pages/dashboard/perfil/mi-club/mi-club.page.ts ***!
  \****************************************************************/
/*! exports provided: MiClubPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MiClubPage", function() { return MiClubPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_mi_club_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./mi-club.page.html */ "xydf");
/* harmony import */ var _mi_club_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./mi-club.page.scss */ "Qn2l");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_services_club_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/club.service */ "hXIf");
/* harmony import */ var src_app_services_storage_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/storage.service */ "n90K");






let MiClubPage = class MiClubPage {
    constructor(_clubService, _storageService) {
        this._clubService = _clubService;
        this._storageService = _storageService;
        this.titulo = 'Mi Club';
    }
    ngOnInit() {
        const tokenInfo = this._storageService.localGet('tokenInfo');
        this._clubService.obtenerClubesxId(tokenInfo.clubId).subscribe(data => {
            console.error(data);
            //this.dismissLoading();
            this.club = data;
        }, error => {
            //this.dismissLoading();
            console.log(error);
            //this.mensaje('Opss.. ocurrio un error');
        });
    }
};
MiClubPage.ctorParameters = () => [
    { type: src_app_services_club_service__WEBPACK_IMPORTED_MODULE_4__["ClubService"] },
    { type: src_app_services_storage_service__WEBPACK_IMPORTED_MODULE_5__["StorageService"] }
];
MiClubPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-mi-club',
        template: _raw_loader_mi_club_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_mi_club_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], MiClubPage);



/***/ })

}]);
//# sourceMappingURL=mi-club-mi-club-module-es2015.js.map