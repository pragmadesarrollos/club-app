(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["usuario-no-registrado-usuario-no-registrado-module"],{

/***/ "49on":
/*!************************************************************************************!*\
  !*** ./src/app/pages/acceso/usuario-no-registrado/usuario-no-registrado.module.ts ***!
  \************************************************************************************/
/*! exports provided: UsuarioNoRegistradoPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsuarioNoRegistradoPageModule", function() { return UsuarioNoRegistradoPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _usuario_no_registrado_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./usuario-no-registrado-routing.module */ "laZS");
/* harmony import */ var _usuario_no_registrado_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./usuario-no-registrado.page */ "s9gI");







let UsuarioNoRegistradoPageModule = class UsuarioNoRegistradoPageModule {
};
UsuarioNoRegistradoPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _usuario_no_registrado_routing_module__WEBPACK_IMPORTED_MODULE_5__["UsuarioNoRegistradoPageRoutingModule"]
        ],
        declarations: [_usuario_no_registrado_page__WEBPACK_IMPORTED_MODULE_6__["UsuarioNoRegistradoPage"]]
    })
], UsuarioNoRegistradoPageModule);



/***/ }),

/***/ "h8i+":
/*!**************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/acceso/usuario-no-registrado/usuario-no-registrado.page.html ***!
  \**************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/\" text=\"\" color>\n      </ion-back-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div id=\"container\">\n    <div style=\"margin-top:-190px ;\">\n      <img src=\"/assets/img/splash/imgUps.png\">\n    </div>\n    <div class=\"ups\">\n      ¡Ups!\n    </div>\n    <div class=\"textoAcceso\">\n      Parece que tu correo no está registrado,\n      <br>\n      ¿querés crear una cuenta?\n    </div>\n    <form (ngSubmit)=\"crearCuenta()\">\n      <div class=\"input-group input-group-lg inputCorreo mt-4\">\n        <span class=\"input-group-text\" style=\"background-color: #e9ecef;\" id=\"inputGroup-sizing-lg\">\n          <ion-icon name=\"mail-outline\" class=\"mail-icon\"></ion-icon>\n          <!-- <svg xmlns=\"http://www.w3.org/2000/svg\" width=\"20\" height=\"20\" fill=\"currentColor\" class=\"bi bi-briefcase\" viewBox=\"0 0 16 16\">\n          <path d=\"M6.5 1A1.5 1.5 0 0 0 5 2.5V3H1.5A1.5 1.5 0 0 0 0 4.5v8A1.5 1.5 0 0 0 1.5 14h13a1.5 1.5 0 0 0 1.5-1.5v-8A1.5 1.5 0 0 0 14.5 3H11v-.5A1.5 1.5 0 0 0 9.5 1h-3zm0 1h3a.5.5 0 0 1 .5.5V3H6v-.5a.5.5 0 0 1 .5-.5zm1.886 6.914L15 7.151V12.5a.5.5 0 0 1-.5.5h-13a.5.5 0 0 1-.5-.5V7.15l6.614 1.764a1.5 1.5 0 0 0 .772 0zM1.5 4h13a.5.5 0 0 1 .5.5v1.616L8.129 7.948a.5.5 0 0 1-.258 0L1 6.116V4.5a.5.5 0 0 1 .5-.5z\"/>\n          </svg> -->\n        </span>\n        <input type=\"text\" class=\"form-control\" [(ngModel)]=\"nombre\" disabled name=\"correo\"\n          placeholder=\"Correo electronico\" style=\"border-style: none; height: 4rem; margin-right: 20px;\">\n      </div>\n      <ion-button type=\"submit\" size=\"large\" class=\"btnIngresar mt-2\" color=\"proclub\" expand=\"block\">CREAR CUENTA\n      </ion-button>\n    </form>\n  </div>\n</ion-content>\n\n<ion-footer class=\"ion-no-border\">\n  <ion-toolbar class=\"ion-text-center toolbar\">\n    <img routerLink=\"/login\" src=\"/assets/img/splash/ProClubLogo.png\">\n  </ion-toolbar>\n</ion-footer>");

/***/ }),

/***/ "laZS":
/*!********************************************************************************************!*\
  !*** ./src/app/pages/acceso/usuario-no-registrado/usuario-no-registrado-routing.module.ts ***!
  \********************************************************************************************/
/*! exports provided: UsuarioNoRegistradoPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsuarioNoRegistradoPageRoutingModule", function() { return UsuarioNoRegistradoPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _usuario_no_registrado_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./usuario-no-registrado.page */ "s9gI");




const routes = [
    {
        path: '',
        component: _usuario_no_registrado_page__WEBPACK_IMPORTED_MODULE_3__["UsuarioNoRegistradoPage"]
    }
];
let UsuarioNoRegistradoPageRoutingModule = class UsuarioNoRegistradoPageRoutingModule {
};
UsuarioNoRegistradoPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], UsuarioNoRegistradoPageRoutingModule);



/***/ }),

/***/ "s9gI":
/*!**********************************************************************************!*\
  !*** ./src/app/pages/acceso/usuario-no-registrado/usuario-no-registrado.page.ts ***!
  \**********************************************************************************/
/*! exports provided: UsuarioNoRegistradoPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsuarioNoRegistradoPage", function() { return UsuarioNoRegistradoPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_usuario_no_registrado_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./usuario-no-registrado.page.html */ "h8i+");
/* harmony import */ var _usuario_no_registrado_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./usuario-no-registrado.page.scss */ "trjz");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var src_app_services_usuario_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/usuario.service */ "on2l");






let UsuarioNoRegistradoPage = class UsuarioNoRegistradoPage {
    constructor(_usuarioService, router) {
        this._usuarioService = _usuarioService;
        this.router = router;
        this.nombre = '';
    }
    ngOnInit() {
        this.nombre = this._usuarioService.nombreUsuario;
        console.log(this._usuarioService.nombreUsuario);
    }
    crearCuenta() {
        this.router.navigateByUrl('/login/ingresar-cuenta');
    }
};
UsuarioNoRegistradoPage.ctorParameters = () => [
    { type: src_app_services_usuario_service__WEBPACK_IMPORTED_MODULE_5__["UsuarioService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
UsuarioNoRegistradoPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-usuario-no-registrado',
        template: _raw_loader_usuario_no_registrado_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_usuario_no_registrado_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], UsuarioNoRegistradoPage);



/***/ }),

/***/ "trjz":
/*!************************************************************************************!*\
  !*** ./src/app/pages/acceso/usuario-no-registrado/usuario-no-registrado.page.scss ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-toolbar {\n  --background: #FF8338;\n}\n\nion-back-button {\n  --color: white !important;\n}\n\nion-content {\n  --height: 100%;\n  --background: linear-gradient(0deg, #F4F3F3 50%, #FF8338 50%);\n}\n\n#container {\n  text-align: center;\n  position: absolute;\n  left: 0;\n  right: 0;\n  top: 50%;\n  transform: translateY(-50%);\n}\n\n.ups {\n  font-family: \"Roboto\", sans-serif;\n  font-style: normal;\n  font-weight: 500;\n  font-size: 26px;\n  line-height: 31px;\n  text-align: center;\n  color: #ffffff;\n}\n\n.textoAcceso {\n  margin-top: 20px;\n  font-family: \"Roboto\", sans-serif;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 16px;\n  line-height: 19px;\n  text-align: center;\n  color: #ffffff;\n}\n\n.inputCorreo {\n  margin-left: 10px;\n  margin-right: 10px;\n}\n\n.mail-icon {\n  font-size: 25px;\n}\n\n.btnIngresar {\n  margin-left: 10px;\n  margin-right: 10px;\n  font-size: 15px;\n  font-family: \"Roboto\", sans-serif;\n}\n\n.input-group-text {\n  border-style: none;\n  background-color: white;\n}\n\n.form-control {\n  padding: 0px;\n}\n\n.toolbar {\n  --background: #F4F3F3;\n}\n\nimg {\n  width: 150px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3VzdWFyaW8tbm8tcmVnaXN0cmFkby5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxxQkFBQTtBQUNGOztBQUVBO0VBQ0UseUJBQUE7QUFDRjs7QUFFQTtFQUNFLGNBQUE7RUFDQSw2REFBQTtBQUNGOztBQUdBO0VBQ0Usa0JBQUE7RUFDQSxrQkFBQTtFQUNBLE9BQUE7RUFDQSxRQUFBO0VBQ0EsUUFBQTtFQUNBLDJCQUFBO0FBQUY7O0FBR0E7RUFDRSxpQ0FBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7QUFBRjs7QUFHQTtFQUNFLGdCQUFBO0VBQ0EsaUNBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0FBQUY7O0FBR0E7RUFDRSxpQkFBQTtFQUNBLGtCQUFBO0FBQUY7O0FBR0E7RUFDRSxlQUFBO0FBQUY7O0FBR0E7RUFDRSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLGlDQUFBO0FBQUY7O0FBSUE7RUFDRSxrQkFBQTtFQUNBLHVCQUFBO0FBREY7O0FBSUE7RUFDRSxZQUFBO0FBREY7O0FBSUE7RUFDRSxxQkFBQTtBQURGOztBQUlBO0VBQ0UsWUFBQTtBQURGIiwiZmlsZSI6InVzdWFyaW8tbm8tcmVnaXN0cmFkby5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tdG9vbGJhciB7XG4gIC0tYmFja2dyb3VuZDogI0ZGODMzODtcbn1cblxuaW9uLWJhY2stYnV0dG9uIHtcbiAgLS1jb2xvcjogd2hpdGUgIWltcG9ydGFudDtcbn1cblxuaW9uLWNvbnRlbnQge1xuICAtLWhlaWdodDogMTAwJTtcbiAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoMGRlZywgI0Y0RjNGMyA1MCUsICNGRjgzMzggNTAlKTtcbn1cblxuXG4jY29udGFpbmVyIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IDA7XG4gIHJpZ2h0OiAwO1xuICB0b3A6IDUwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01MCUpO1xufVxuXG4udXBzIHtcbiAgZm9udC1mYW1pbHk6IFwiUm9ib3RvXCIsIHNhbnMtc2VyaWY7XG4gIGZvbnQtc3R5bGU6IG5vcm1hbDtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgZm9udC1zaXplOiAyNnB4O1xuICBsaW5lLWhlaWdodDogMzFweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBjb2xvcjogI2ZmZmZmZjtcbn1cblxuLnRleHRvQWNjZXNvIHtcbiAgbWFyZ2luLXRvcDogMjBweDtcbiAgZm9udC1mYW1pbHk6IFwiUm9ib3RvXCIsIHNhbnMtc2VyaWY7XG4gIGZvbnQtc3R5bGU6IG5vcm1hbDtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgZm9udC1zaXplOiAxNnB4O1xuICBsaW5lLWhlaWdodDogMTlweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBjb2xvcjogI2ZmZmZmZjtcbn1cblxuLmlucHV0Q29ycmVvIHtcbiAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gIG1hcmdpbi1yaWdodDogMTBweDtcbn1cblxuLm1haWwtaWNvbiB7XG4gIGZvbnQtc2l6ZTogMjVweDtcbn1cblxuLmJ0bkluZ3Jlc2FyIHtcbiAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gIG1hcmdpbi1yaWdodDogMTBweDtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBmb250LWZhbWlseTogJ1JvYm90bycsXG4gICAgc2Fucy1zZXJpZjtcbn1cblxuLmlucHV0LWdyb3VwLXRleHQge1xuICBib3JkZXItc3R5bGU6IG5vbmU7XG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xufVxuXG4uZm9ybS1jb250cm9sIHtcbiAgcGFkZGluZzogMHB4O1xufVxuXG4udG9vbGJhciB7XG4gIC0tYmFja2dyb3VuZDogI0Y0RjNGMztcbn1cblxuaW1nIHtcbiAgd2lkdGg6IDE1MHB4O1xufVxuIl19 */");

/***/ })

}]);
//# sourceMappingURL=usuario-no-registrado-usuario-no-registrado-module-es2015.js.map