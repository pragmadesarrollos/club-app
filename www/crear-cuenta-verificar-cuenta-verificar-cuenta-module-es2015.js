(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["crear-cuenta-verificar-cuenta-verificar-cuenta-module"],{

/***/ "2b3x":
/*!***********************************************************************************************!*\
  !*** ./src/app/pages/acceso/crear-cuenta/verificar-cuenta/verificar-cuenta-routing.module.ts ***!
  \***********************************************************************************************/
/*! exports provided: VerificarCuentaPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VerificarCuentaPageRoutingModule", function() { return VerificarCuentaPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _verificar_cuenta_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./verificar-cuenta.page */ "Hdi2");




const routes = [
    {
        path: '',
        component: _verificar_cuenta_page__WEBPACK_IMPORTED_MODULE_3__["VerificarCuentaPage"]
    }
];
let VerificarCuentaPageRoutingModule = class VerificarCuentaPageRoutingModule {
};
VerificarCuentaPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], VerificarCuentaPageRoutingModule);



/***/ }),

/***/ "Hdi2":
/*!*************************************************************************************!*\
  !*** ./src/app/pages/acceso/crear-cuenta/verificar-cuenta/verificar-cuenta.page.ts ***!
  \*************************************************************************************/
/*! exports provided: VerificarCuentaPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VerificarCuentaPage", function() { return VerificarCuentaPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_verificar_cuenta_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./verificar-cuenta.page.html */ "t9vX");
/* harmony import */ var _verificar_cuenta_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./verificar-cuenta.page.scss */ "K2sJ");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");





let VerificarCuentaPage = class VerificarCuentaPage {
    constructor(router) {
        this.router = router;
    }
    ngOnInit() {
    }
    siguiente() {
        this.router.navigateByUrl('/cuenta-no-verificada');
    }
};
VerificarCuentaPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
];
VerificarCuentaPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-verificar-cuenta',
        template: _raw_loader_verificar_cuenta_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_verificar_cuenta_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], VerificarCuentaPage);



/***/ }),

/***/ "K2sJ":
/*!***************************************************************************************!*\
  !*** ./src/app/pages/acceso/crear-cuenta/verificar-cuenta/verificar-cuenta.page.scss ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-toolbar {\n  --background: #514cbd;\n}\n\nion-content {\n  --height: 100%;\n  --background: #6b64f8 ;\n}\n\n.stepper {\n  height: 100px;\n  background-color: #514cbd;\n  border-radius: 0px 0px 32px 32px;\n  display: flex;\n  justify-content: center;\n}\n\n.img {\n  width: 330px;\n  max-height: 75px;\n}\n\nimg {\n  width: 150px;\n}\n\n.listo {\n  font-family: \"Roboto\", sans-serif;\n  font-style: normal;\n  font-weight: 500;\n  font-size: 26px;\n  line-height: 31px;\n  text-align: center;\n  color: #ffffff;\n}\n\nion-button {\n  background: white !important;\n  color: #514cbd !important;\n  border-radius: 8px;\n}\n\n#container {\n  text-align: center;\n  position: absolute;\n  left: 0;\n  right: 0;\n  top: 50%;\n  transform: translateY(-50%);\n}\n\n.textoAcceso {\n  margin-top: 20px;\n  font-family: \"Roboto\", sans-serif;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 16px;\n  line-height: 19px;\n  text-align: center;\n  color: #FFFFFF;\n}\n\nion-footer ion-toolbar {\n  --background: #6b64f8;\n}\n\nion-footer .img {\n  width: 150px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL3ZlcmlmaWNhci1jdWVudGEucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0kscUJBQUE7QUFDSjs7QUFHRTtFQUNFLGNBQUE7RUFDQSxzQkFBQTtBQUFKOztBQUdBO0VBQ0UsYUFBQTtFQUNBLHlCQUFBO0VBQ0EsZ0NBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7QUFBRjs7QUFHQTtFQUNFLFlBQUE7RUFDQSxnQkFBQTtBQUFGOztBQUdBO0VBQ0UsWUFBQTtBQUFGOztBQUdBO0VBQ0UsaUNBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0FBQUY7O0FBR0E7RUFDRSw0QkFBQTtFQUNBLHlCQUFBO0VBQ0Esa0JBQUE7QUFBRjs7QUFHQTtFQUNFLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxPQUFBO0VBQ0EsUUFBQTtFQUNBLFFBQUE7RUFDQSwyQkFBQTtBQUFGOztBQUdBO0VBQ0UsZ0JBQUE7RUFDQSxpQ0FBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7QUFBRjs7QUFLRTtFQUNJLHFCQUFBO0FBRk47O0FBSUU7RUFDSSxZQUFBO0FBRk4iLCJmaWxlIjoidmVyaWZpY2FyLWN1ZW50YS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tdG9vbGJhciB7XG4gICAgLS1iYWNrZ3JvdW5kOiAjNTE0Y2JkO1xufVxuXG5cbiAgaW9uLWNvbnRlbnQge1xuICAgIC0taGVpZ2h0OiAxMDAlO1xuICAgIC0tYmFja2dyb3VuZDogIzZiNjRmOFxufVxuXG4uc3RlcHBlciB7XG4gIGhlaWdodDogMTAwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICM1MTRjYmQ7XG4gIGJvcmRlci1yYWRpdXM6IDBweCAwcHggMzJweCAzMnB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgXG59XG4uaW1nIHtcbiAgd2lkdGg6IDMzMHB4O1xuICBtYXgtaGVpZ2h0OiA3NXB4O1xufVxuXG5pbWcge1xuICB3aWR0aDogMTUwcHg7XG59XG5cbi5saXN0byB7XG4gIGZvbnQtZmFtaWx5OiBcIlJvYm90b1wiLCBzYW5zLXNlcmlmO1xuICBmb250LXN0eWxlOiBub3JtYWw7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIGZvbnQtc2l6ZTogMjZweDtcbiAgbGluZS1oZWlnaHQ6IDMxcHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgY29sb3I6ICNmZmZmZmY7XG59XG5cbmlvbi1idXR0b24ge1xuICBiYWNrZ3JvdW5kOiB3aGl0ZSAhaW1wb3J0YW50O1xuICBjb2xvcjogIzUxNGNiZCAhaW1wb3J0YW50O1xuICBib3JkZXItcmFkaXVzOiA4cHg7XG59XG5cbiNjb250YWluZXIge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogMDtcbiAgcmlnaHQ6IDA7XG4gIHRvcDogNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTUwJSk7XG59XG5cbi50ZXh0b0FjY2VzbyB7XG4gIG1hcmdpbi10b3A6IDIwcHg7XG4gIGZvbnQtZmFtaWx5OiAnUm9ib3RvJywgc2Fucy1zZXJpZjtcbiAgZm9udC1zdHlsZTogbm9ybWFsO1xuICBmb250LXdlaWdodDogbm9ybWFsO1xuICBmb250LXNpemU6IDE2cHg7XG4gIGxpbmUtaGVpZ2h0OiAxOXB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGNvbG9yOiAjRkZGRkZGO1xufVxuXG5cbmlvbi1mb290ZXIge1xuICBpb24tdG9vbGJhciB7XG4gICAgICAtLWJhY2tncm91bmQ6ICM2YjY0Zjg7XG4gIH1cbiAgLmltZyB7XG4gICAgICB3aWR0aDogMTUwcHg7XG4gIH1cbn1cbiJdfQ== */");

/***/ }),

/***/ "WZB3":
/*!***************************************************************************************!*\
  !*** ./src/app/pages/acceso/crear-cuenta/verificar-cuenta/verificar-cuenta.module.ts ***!
  \***************************************************************************************/
/*! exports provided: VerificarCuentaPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VerificarCuentaPageModule", function() { return VerificarCuentaPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _verificar_cuenta_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./verificar-cuenta-routing.module */ "2b3x");
/* harmony import */ var _verificar_cuenta_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./verificar-cuenta.page */ "Hdi2");







let VerificarCuentaPageModule = class VerificarCuentaPageModule {
};
VerificarCuentaPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _verificar_cuenta_routing_module__WEBPACK_IMPORTED_MODULE_5__["VerificarCuentaPageRoutingModule"]
        ],
        declarations: [_verificar_cuenta_page__WEBPACK_IMPORTED_MODULE_6__["VerificarCuentaPage"]]
    })
], VerificarCuentaPageModule);



/***/ }),

/***/ "t9vX":
/*!*****************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/acceso/crear-cuenta/verificar-cuenta/verificar-cuenta.page.html ***!
  \*****************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\n  <ion-toolbar>\n\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n  <div class=\"stepper\">\n    <img class=\"img\" src=\"/assets/img/crear-cuenta/crearCuentaVerificacion.png\">\n  </div>\n\n  <div id=\"container\">\n    <div style=\"margin-top: 10px;\">\n      <img src=\"/assets/img/splash/imgCrearPassword.png\">\n    </div>\n  \n    <div class=\"listo\">\n      ¡Listo! Ya creaste tu cuenta\n    </div>\n    <div class=\"textoAcceso\">\n      Ahora el club tiene que darte el alta\n      <br>      \n      para que puedas acceder a todo los\n      <br>\n      beneficios\n    </div>\n    <ion-button class=\"ion-margin btnSiguiente\" color=\"ligth\" (click)=\"siguiente()\" size=\"large\" expand=\"block\">IR A MI PERFIL</ion-button>\n  </div>\n \n</ion-content>\n<ion-footer class=\"ion-no-border\">\n  <ion-toolbar class=\"ion-text-center\">\n    <img routerLink=\"/login\" src=\"/assets/img/splash/logoProClubFondoV.png\">\n  </ion-toolbar>\n</ion-footer>");

/***/ })

}]);
//# sourceMappingURL=crear-cuenta-verificar-cuenta-verificar-cuenta-module-es2015.js.map