(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["deportes-deportes-module"],{

/***/ "5Rih":
/*!**********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/dashboard/perfil/deportes/deportes.page.html ***!
  \**********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\n  <app-header [titulo]=\"titulo\"></app-header>\n</ion-header>\n\n<ion-content>\n\n  <div class=\"grid-container\">\n    <div class=\"logo-deporte\">\n      <ion-icon name=\"football-outline\" class=\"football-icon\"></ion-icon>\n    </div>\n    <div class=\"deportes\">\n      <ion-fab color=\"secondary\" horizontal=\"end\" class=\"icon-deporte\">\n        <ion-icon name=\"ellipsis-vertical-outline\"></ion-icon>\n      </ion-fab>\n      <div class=\"text-container\">\n        <ion-text class=\"football\"><b>Football</b></ion-text>\n        <br>\n        <ion-text style=\"opacity: 50%;\">Primera división C</ion-text>\n        <br>\n        <ion-text style=\"opacity: 50%;\">Arquero</ion-text>\n        <br>\n        <ion-button class=\"btn-deportes-red\" color=\"transparent\">\n          <ion-icon name=\"checkmark-outline\"></ion-icon>\n          <b>confirmado</b>\n        </ion-button>\n      </div>\n    </div>\n  </div>\n  \n  <div class=\"grid-container\">\n    <div class=\"logo-deporte-gray\">\n      <ion-icon name=\"football-outline\" class=\"football-icon\"></ion-icon>\n    </div>\n    <div class=\"deportes\">\n      <ion-fab horizontal=\"end\" class=\"icon-deporte\">\n        <ion-icon name=\"ellipsis-vertical-outline\"></ion-icon>\n      </ion-fab>\n      <div class=\"text-container\">\n        <ion-text class=\"football\"><b>Football</b></ion-text>\n        <br>\n        <ion-text style=\"opacity: 50%;\">Primera división C</ion-text>\n        <br>\n        <ion-text style=\"opacity: 50%;\">Arquero</ion-text>\n        <br>\n        <ion-button class=\"btn-deportes-gray\" color=\"transparent\">\n          <ion-icon name=\"time-outline\"></ion-icon>\n          <b>Esperando confirmación</b>\n        </ion-button>\n      </div>\n    </div>\n  </div>\n</ion-content>\n\n<ion-toolbar class=\"ion-padding-start ion-padding-end\">\n  <ion-button expand=\"block\" class=\"btn-deportes-red\" color=\"transparent\">\n    <ion-icon name=\"plus-outline\"></ion-icon>\n    <b>+ Agregar nuevo deporte</b>\n  </ion-button>\n</ion-toolbar>\n");

/***/ }),

/***/ "CAp0":
/*!****************************************************************************!*\
  !*** ./src/app/pages/dashboard/perfil/deportes/deportes-routing.module.ts ***!
  \****************************************************************************/
/*! exports provided: DeportesPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeportesPageRoutingModule", function() { return DeportesPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _deportes_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./deportes.page */ "wEZ3");




const routes = [
    {
        path: '',
        component: _deportes_page__WEBPACK_IMPORTED_MODULE_3__["DeportesPage"]
    }
];
let DeportesPageRoutingModule = class DeportesPageRoutingModule {
};
DeportesPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], DeportesPageRoutingModule);



/***/ }),

/***/ "M1vj":
/*!********************************************************************!*\
  !*** ./src/app/pages/dashboard/perfil/deportes/deportes.page.scss ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".grid-container {\n  margin-right: 15px;\n  margin-left: 15px;\n  display: flex;\n  align-items: center;\n  height: 200px;\n}\n\n.btn-container {\n  display: flex;\n  align-items: center;\n  position: fixed;\n  margin-top: 300px;\n  padding: 60px;\n}\n\n.logo-deporte {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  background: var(--ion-color-secondary);\n  width: 80px;\n  height: 80px;\n  position: absolute;\n  border-radius: 8px;\n  margin-bottom: 55px;\n}\n\n.logo-deporte-gray {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  background-color: #92949C;\n  width: 80px;\n  height: 80px;\n  position: absolute;\n  border-radius: 8px;\n  margin-bottom: 55px;\n}\n\n.football-icon {\n  width: 80%;\n  height: 80%;\n  color: #fff;\n}\n\n.deportes {\n  width: 90%;\n  margin-left: 40px;\n  background: #FFFFFF;\n  box-shadow: 0px 2px 3px rgba(0, 0, 0, 0.2);\n  border-radius: 8px;\n}\n\n.football {\n  font-weight: 700;\n}\n\n.icon-deporte {\n  margin-top: 15px;\n  margin-right: 15px;\n}\n\n.text-container {\n  padding: 20px;\n  height: 90%;\n  width: 90%;\n  margin-left: 30px;\n}\n\n.confirmado {\n  display: flex;\n  align-content: center;\n  padding: 3px;\n  margin-top: 10px;\n  margin-bottom: 10px;\n  margin-right: 130px;\n  margin-top: 20px;\n  outline: none;\n  border: 2px solid var(--ion-color-secondary);\n  border-radius: 5px;\n}\n\n.btn-checkmark {\n  margin-top: 5px;\n  font-size: 15px;\n}\n\n.btn-confirmado {\n  margin-left: 10px;\n}\n\n.btn-container {\n  display: flex;\n  align-content: center;\n  justify-content: center;\n}\n\n.btn-deportes-red {\n  outline: none;\n  border: 2px solid var(--ion-color-secondary);\n  border-radius: 5px;\n  font-family: \"Roboto\", sans-serif;\n  color: var(--ion-color-secondary) !important;\n}\n\n.btn-deportes-gray {\n  outline: none;\n  border: 2px solid #92949C !important;\n  border-radius: 5px;\n  font-family: \"Roboto\", sans-serif;\n  color: #92949C !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL2RlcG9ydGVzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0FBQ0Y7O0FBRUE7RUFDRSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxhQUFBO0FBQ0Y7O0FBRUE7RUFDRSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLHNDQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7QUFDRjs7QUFFQTtFQUNFLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EseUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtBQUNGOztBQUVBO0VBQ0UsVUFBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0FBQ0Y7O0FBRUE7RUFDRSxVQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLDBDQUFBO0VBQ0Esa0JBQUE7QUFDRjs7QUFFQTtFQUNFLGdCQUFBO0FBQ0Y7O0FBRUE7RUFDRSxnQkFBQTtFQUNBLGtCQUFBO0FBQ0Y7O0FBRUE7RUFDRSxhQUFBO0VBQ0EsV0FBQTtFQUNBLFVBQUE7RUFDQSxpQkFBQTtBQUNGOztBQUVBO0VBQ0UsYUFBQTtFQUNBLHFCQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsYUFBQTtFQUNBLDRDQUFBO0VBQ0Esa0JBQUE7QUFDRjs7QUFFQTtFQUNFLGVBQUE7RUFDQSxlQUFBO0FBQ0Y7O0FBRUE7RUFDRSxpQkFBQTtBQUNGOztBQUVBO0VBQ0UsYUFBQTtFQUNBLHFCQUFBO0VBQ0EsdUJBQUE7QUFDRjs7QUFFQTtFQUNFLGFBQUE7RUFDQSw0Q0FBQTtFQUNBLGtCQUFBO0VBQ0EsaUNBQUE7RUFFQSw0Q0FBQTtBQUFGOztBQUdBO0VBQ0UsYUFBQTtFQUNBLG9DQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQ0FBQTtFQUVFLHlCQUFBO0FBREoiLCJmaWxlIjoiZGVwb3J0ZXMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmdyaWQtY29udGFpbmVyIHtcbiAgbWFyZ2luLXJpZ2h0OiAxNXB4O1xuICBtYXJnaW4tbGVmdDogMTVweDtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgaGVpZ2h0OiAyMDBweDtcbn1cblxuLmJ0bi1jb250YWluZXIge1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIG1hcmdpbi10b3A6IDMwMHB4O1xuICBwYWRkaW5nOiA2MHB4O1xufVxuXG4ubG9nby1kZXBvcnRlIHtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnkpO1xuICB3aWR0aDogODBweDtcbiAgaGVpZ2h0OiA4MHB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJvcmRlci1yYWRpdXM6IDhweDtcbiAgbWFyZ2luLWJvdHRvbTogNTVweDtcbn1cblxuLmxvZ28tZGVwb3J0ZS1ncmF5IHtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGJhY2tncm91bmQtY29sb3I6ICM5Mjk0OUM7XG4gIHdpZHRoOiA4MHB4O1xuICBoZWlnaHQ6IDgwcHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYm9yZGVyLXJhZGl1czogOHB4O1xuICBtYXJnaW4tYm90dG9tOiA1NXB4O1xufVxuXG4uZm9vdGJhbGwtaWNvbiB7XG4gIHdpZHRoOiA4MCU7XG4gIGhlaWdodDogODAlO1xuICBjb2xvcjogI2ZmZjtcbn1cblxuLmRlcG9ydGVzIHtcbiAgd2lkdGg6IDkwJTtcbiAgbWFyZ2luLWxlZnQ6IDQwcHg7XG4gIGJhY2tncm91bmQ6ICNGRkZGRkY7XG4gIGJveC1zaGFkb3c6IDBweCAycHggM3B4IHJnYmEoMCwgMCwgMCwgMC4yKTtcbiAgYm9yZGVyLXJhZGl1czogOHB4O1xufVxuXG4uZm9vdGJhbGwge1xuICBmb250LXdlaWdodDogNzAwO1xufVxuXG4uaWNvbi1kZXBvcnRlIHtcbiAgbWFyZ2luLXRvcDogMTVweDtcbiAgbWFyZ2luLXJpZ2h0OiAxNXB4O1xufVxuXG4udGV4dC1jb250YWluZXIge1xuICBwYWRkaW5nOiAyMHB4O1xuICBoZWlnaHQ6IDkwJTtcbiAgd2lkdGg6IDkwJTtcbiAgbWFyZ2luLWxlZnQ6IDMwcHg7XG59XG5cbi5jb25maXJtYWRvIHtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24tY29udGVudDogY2VudGVyO1xuICBwYWRkaW5nOiAzcHg7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gIG1hcmdpbi1yaWdodDogMTMwcHg7XG4gIG1hcmdpbi10b3A6IDIwcHg7XG4gIG91dGxpbmU6IG5vbmU7XG4gIGJvcmRlcjogMnB4IHNvbGlkIHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnkpO1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG59XG5cbi5idG4tY2hlY2ttYXJrIHtcbiAgbWFyZ2luLXRvcDogNXB4O1xuICBmb250LXNpemU6IDE1cHg7XG59XG5cbi5idG4tY29uZmlybWFkbyB7XG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xufVxuXG4uYnRuLWNvbnRhaW5lciB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWNvbnRlbnQ6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG5cbi5idG4tZGVwb3J0ZXMtcmVkIHtcbiAgb3V0bGluZTogbm9uZTtcbiAgYm9yZGVyOiAycHggc29saWQgdmFyKC0taW9uLWNvbG9yLXNlY29uZGFyeSk7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgZm9udC1mYW1pbHk6ICdSb2JvdG8nLFxuICAgIHNhbnMtc2VyaWY7XG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3Itc2Vjb25kYXJ5KSAhaW1wb3J0YW50O1xufVxuXG4uYnRuLWRlcG9ydGVzLWdyYXkge1xuICBvdXRsaW5lOiBub25lO1xuICBib3JkZXI6IDJweCBzb2xpZCAjOTI5NDlDICFpbXBvcnRhbnQ7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgZm9udC1mYW1pbHk6ICdSb2JvdG8nLFxuICAgIHNhbnMtc2VyaWY7XG4gICAgY29sb3I6ICM5Mjk0OUMgIWltcG9ydGFudDtcbn0iXX0= */");

/***/ }),

/***/ "nQQm":
/*!********************************************************************!*\
  !*** ./src/app/pages/dashboard/perfil/deportes/deportes.module.ts ***!
  \********************************************************************/
/*! exports provided: DeportesPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeportesPageModule", function() { return DeportesPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _deportes_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./deportes-routing.module */ "CAp0");
/* harmony import */ var _deportes_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./deportes.page */ "wEZ3");
/* harmony import */ var src_app_components_header_header_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/header/header.component */ "2MiI");








let DeportesPageModule = class DeportesPageModule {
};
DeportesPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _deportes_routing_module__WEBPACK_IMPORTED_MODULE_5__["DeportesPageRoutingModule"]
        ],
        declarations: [
            _deportes_page__WEBPACK_IMPORTED_MODULE_6__["DeportesPage"],
            src_app_components_header_header_component__WEBPACK_IMPORTED_MODULE_7__["HeaderComponent"]
        ]
    })
], DeportesPageModule);



/***/ }),

/***/ "wEZ3":
/*!******************************************************************!*\
  !*** ./src/app/pages/dashboard/perfil/deportes/deportes.page.ts ***!
  \******************************************************************/
/*! exports provided: DeportesPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeportesPage", function() { return DeportesPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_deportes_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./deportes.page.html */ "5Rih");
/* harmony import */ var _deportes_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./deportes.page.scss */ "M1vj");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var src_app_services_club_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/club.service */ "hXIf");
/* harmony import */ var src_app_services_storage_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/storage.service */ "n90K");







let DeportesPage = class DeportesPage {
    constructor(_clubService, _storageService, fb) {
        this._clubService = _clubService;
        this._storageService = _storageService;
        this.fb = fb;
        this.titulo = 'Deportes';
        this.listDeportes = [];
    }
    ngOnInit() {
        const tokenInfo = this._storageService.localGet('tokenInfo');
        console.log(tokenInfo);
        this._clubService.obtenerDisciplinasByUserId(tokenInfo.userId).subscribe((data) => {
            //this.dismissLoading();
            this.listDeportes = data;
            console.log(data);
        }, (error) => {
            //this.dismissLoading();
            console.log(error);
            //this.mensaje('Opss.. ocurrio un error');
        });
    }
};
DeportesPage.ctorParameters = () => [
    { type: src_app_services_club_service__WEBPACK_IMPORTED_MODULE_5__["ClubService"] },
    { type: src_app_services_storage_service__WEBPACK_IMPORTED_MODULE_6__["StorageService"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"] }
];
DeportesPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-deportes',
        template: _raw_loader_deportes_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_deportes_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], DeportesPage);



/***/ })

}]);
//# sourceMappingURL=deportes-deportes-module-es2015.js.map