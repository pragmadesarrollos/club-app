(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["enviar-enviar-module"],{

/***/ "BACE":
/*!*********************************************************************************************!*\
  !*** ./src/app/pages/dashboard/notificaciones/crear-notificaciones/enviar/enviar.page.scss ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".contenedor {\n  display: flex;\n  align-items: center;\n  width: 90%;\n  height: 100vh;\n  margin-top: 15px;\n  padding: 0;\n}\n\n.toolbar-text {\n  width: 100%;\n  margin-left: 16px;\n  margin-right: 16px;\n}\n\n.toolbar-text .text-container {\n  display: flex;\n  flex-direction: row;\n}\n\n.toolbar-text .text-container span {\n  font-size: 13px;\n}\n\n.toolbar-text .text-container .span-1 {\n  white-space: nowrap;\n  color: var(--ion-color-primary);\n}\n\n.toolbar-input {\n  width: 100%;\n}\n\n.toolbar-input .input {\n  margin-top: 20px;\n  font-size: 20px;\n  padding-bottom: 10px;\n}\n\n.footer {\n  width: 90%;\n  height: 300px;\n  margin: auto;\n}\n\n.footer .footer-div {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  margin-bottom: 100px;\n  width: 100%;\n  height: 100%;\n}\n\n.footer .footer-div button {\n  background-color: red;\n  width: 100%;\n  height: 40px;\n  border-radius: 8px;\n  color: white;\n}\n\nion-back-button, ion-title {\n  --color: white !important;\n}\n\n.title {\n  font-family: \"Founders Grotesk\";\n  font-style: normal;\n  font-weight: normal;\n  font-size: 20px;\n  line-height: 24px;\n  display: flex;\n  align-items: center;\n  color: #FFFFFF;\n}\n\n.img {\n  width: 330px;\n  max-height: 75px;\n}\n\n.textoAcceso {\n  margin-top: 20px;\n  font-family: \"Roboto\", sans-serif;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 16px;\n  line-height: 19px;\n  text-align: center;\n  color: #ffffff;\n}\n\nion-button {\n  background: var(--ion-color-primary) !important;\n  color: white !important;\n  border-radius: 8px;\n}\n\nion-card {\n  margin-left: 15px;\n  margin-right: 15px;\n  margin-top: 20px;\n}\n\nion-footer .img {\n  width: 150px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uL2Vudmlhci5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxVQUFBO0VBQ0EsYUFBQTtFQUNBLGdCQUFBO0VBQ0EsVUFBQTtBQUNGOztBQUdBO0VBQ0UsV0FBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7QUFBRjs7QUFFRTtFQUNFLGFBQUE7RUFDQSxtQkFBQTtBQUFKOztBQUVJO0VBQ0UsZUFBQTtBQUFOOztBQUdJO0VBQ0UsbUJBQUE7RUFDQSwrQkFBQTtBQUROOztBQU9BO0VBQ0UsV0FBQTtBQUpGOztBQU1FO0VBQ0UsZ0JBQUE7RUFDQSxlQUFBO0VBQ0Esb0JBQUE7QUFKSjs7QUFTQTtFQUNFLFVBQUE7RUFDQSxhQUFBO0VBQ0EsWUFBQTtBQU5GOztBQVFFO0VBQ0UsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxvQkFBQTtFQUVBLFdBQUE7RUFDQSxZQUFBO0FBUEo7O0FBU0k7RUFDRSxxQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0FBUE47O0FBY0E7RUFDRSx5QkFBQTtBQVhGOztBQWlCQTtFQUNBLCtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLGNBQUE7QUFkQTs7QUFpQkE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7QUFkQTs7QUFrQkE7RUFDQSxnQkFBQTtFQUNBLGlDQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtBQWZBOztBQWtCQTtFQUNBLCtDQUFBO0VBQ0EsdUJBQUE7RUFDQSxrQkFBQTtBQWZBOztBQWtCQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtBQWZBOztBQXFCQTtFQUNJLFlBQUE7QUFsQkoiLCJmaWxlIjoiZW52aWFyLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jb250ZW5lZG9yIHtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgd2lkdGg6IDkwJTtcbiAgaGVpZ2h0OiAxMDB2aDtcbiAgbWFyZ2luLXRvcDogMTVweDtcbiAgcGFkZGluZzogMDtcbn1cblxuXG4udG9vbGJhci10ZXh0IHtcbiAgd2lkdGg6IDEwMCU7XG4gIG1hcmdpbi1sZWZ0OiAxNnB4O1xuICBtYXJnaW4tcmlnaHQ6IDE2cHg7XG5cbiAgLnRleHQtY29udGFpbmVyIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG5cbiAgICBzcGFuIHtcbiAgICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICB9XG5cbiAgICAuc3Bhbi0xIHtcbiAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xuICAgIH1cbiAgfVxufVxuXG5cbi50b29sYmFyLWlucHV0IHtcbiAgd2lkdGg6IDEwMCU7XG5cbiAgLmlucHV0IHtcbiAgICBtYXJnaW4tdG9wOiAyMHB4O1xuICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICBwYWRkaW5nLWJvdHRvbTogMTBweDtcbiAgfVxufVxuXG5cbi5mb290ZXIge1xuICB3aWR0aDogOTAlO1xuICBoZWlnaHQ6IDMwMHB4O1xuICBtYXJnaW46IGF1dG87XG5cbiAgLmZvb3Rlci1kaXYge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBtYXJnaW4tYm90dG9tOiAxMDBweDtcblxuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcblxuICAgIGJ1dHRvbiB7XG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZWQ7XG4gICAgICB3aWR0aDogMTAwJTtcbiAgICAgIGhlaWdodDogNDBweDtcbiAgICAgIGJvcmRlci1yYWRpdXM6IDhweDtcbiAgICAgIGNvbG9yOiB3aGl0ZTtcbiAgICB9XG4gIH1cbn1cblxuXG5cbmlvbi1iYWNrLWJ1dHRvbiwgaW9uLXRpdGxle1xuICAtLWNvbG9yOiB3aGl0ZSAhaW1wb3J0YW50O1xufVxuXG5cblxuXG4udGl0bGUge1xuZm9udC1mYW1pbHk6ICdGb3VuZGVycyBHcm90ZXNrJztcbmZvbnQtc3R5bGU6IG5vcm1hbDtcbmZvbnQtd2VpZ2h0OiBub3JtYWw7XG5mb250LXNpemU6IDIwcHg7XG5saW5lLWhlaWdodDogMjRweDtcbmRpc3BsYXk6IGZsZXg7XG5hbGlnbi1pdGVtczogY2VudGVyO1xuY29sb3I6ICNGRkZGRkY7XG59XG5cbi5pbWcge1xud2lkdGg6IDMzMHB4O1xubWF4LWhlaWdodDogNzVweDtcbn1cblxuXG4udGV4dG9BY2Nlc28ge1xubWFyZ2luLXRvcDogMjBweDtcbmZvbnQtZmFtaWx5OiBcIlJvYm90b1wiLCBzYW5zLXNlcmlmO1xuZm9udC1zdHlsZTogbm9ybWFsO1xuZm9udC13ZWlnaHQ6IG5vcm1hbDtcbmZvbnQtc2l6ZTogMTZweDtcbmxpbmUtaGVpZ2h0OiAxOXB4O1xudGV4dC1hbGlnbjogY2VudGVyO1xuY29sb3I6ICNmZmZmZmY7XG59XG5cbmlvbi1idXR0b24ge1xuYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpICFpbXBvcnRhbnQ7XG5jb2xvcjogd2hpdGUgIWltcG9ydGFudDtcbmJvcmRlci1yYWRpdXM6IDhweDtcbn1cblxuaW9uLWNhcmQge1xubWFyZ2luLWxlZnQ6IDE1cHg7XG5tYXJnaW4tcmlnaHQ6IDE1cHg7XG5tYXJnaW4tdG9wOiAyMHB4O1xufVxuXG5cbmlvbi1mb290ZXIge1xuXG4uaW1nIHtcbiAgICB3aWR0aDogMTUwcHg7XG59XG59XG4iXX0= */");

/***/ }),

/***/ "fWXH":
/*!*******************************************************************************************!*\
  !*** ./src/app/pages/dashboard/notificaciones/crear-notificaciones/enviar/enviar.page.ts ***!
  \*******************************************************************************************/
/*! exports provided: EnviarPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EnviarPage", function() { return EnviarPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_enviar_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./enviar.page.html */ "sHyD");
/* harmony import */ var _enviar_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./enviar.page.scss */ "BACE");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var src_app_services_notificacion_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/notificacion.service */ "J/xi");







let EnviarPage = class EnviarPage {
    constructor(router, loadingController, toastController, notificacionService, alertController) {
        this.router = router;
        this.loadingController = loadingController;
        this.toastController = toastController;
        this.notificacionService = notificacionService;
        this.alertController = alertController;
        this.titulo = 'Enviar';
        this.destinatarios = [];
        this.titulo = this.notificacionService.titulo;
        this.descripcion = this.notificacionService.descripcion;
        //this.pregunta = this._encuestaService.pregunta;
        //this.opcion = this._encuestaService.opcion;
        this.destinatarios = this.notificacionService.destinatarios;
    }
    ngOnInit() {
    }
    siguiente() {
        this.presentLoading().then((loadRes) => {
            this.loading = loadRes;
            this.loading.present();
            this.notificacionService.enviarNotificacion({
                titulo: this.titulo,
                descripcion: this.descripcion
            }, this.destinatarios).subscribe((data) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                console.log(data);
                this.dismissLoading();
                this.router.navigate(['/menu']);
                const alert = yield this.alertController.create({
                    header: 'Notificacion',
                    message: 'Enviada correctamente.',
                    buttons: ['OK']
                });
                yield alert.present();
            }), error => {
                console.log(error);
                this.dismissLoading();
                this.mensaje('Opss.. ocurrio un error');
            });
        });
    }
    presentLoading() {
        if (this.loading) {
            this.loading.dismiss();
        }
        return new Promise((resolve) => {
            resolve(this.loadingController.create({
                message: 'Espere...'
            }));
        });
    }
    dismissLoading() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (this.loading) {
                this.loading.dismiss();
            }
        });
    }
    mensaje(mensaje) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: mensaje,
                color: 'dark',
                duration: 3000
            });
            toast.present();
        });
    }
};
EnviarPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"] },
    { type: src_app_services_notificacion_service__WEBPACK_IMPORTED_MODULE_6__["NotificacionService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"] }
];
EnviarPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-enviar',
        template: _raw_loader_enviar_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_enviar_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], EnviarPage);



/***/ }),

/***/ "i5Xo":
/*!*********************************************************************************************!*\
  !*** ./src/app/pages/dashboard/notificaciones/crear-notificaciones/enviar/enviar.module.ts ***!
  \*********************************************************************************************/
/*! exports provided: EnviarPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EnviarPageModule", function() { return EnviarPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _enviar_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./enviar-routing.module */ "xnN5");
/* harmony import */ var _enviar_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./enviar.page */ "fWXH");
/* harmony import */ var src_app_components_header_header_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/header/header.component */ "2MiI");









let EnviarPageModule = class EnviarPageModule {
};
EnviarPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _enviar_routing_module__WEBPACK_IMPORTED_MODULE_5__["EnviarPageRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"]
        ],
        declarations: [
            _enviar_page__WEBPACK_IMPORTED_MODULE_6__["EnviarPage"],
            src_app_components_header_header_component__WEBPACK_IMPORTED_MODULE_7__["HeaderComponent"]
        ]
    })
], EnviarPageModule);



/***/ }),

/***/ "sHyD":
/*!***********************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/dashboard/notificaciones/crear-notificaciones/enviar/enviar.page.html ***!
  \***********************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\n  <app-header [titulo]=\"titulo\"></app-header>\n</ion-header>\n\n<ion-content>\n  <ion-toolbar class=\"toolbar-text\">\n    <span><b>Revisá la notificación y enviar</b></span>\n  </ion-toolbar>\n\n  <ion-toolbar class=\"toolbar-text\">\n    <div class=\"text-container\">\n      <span>Titulo y descripción ></span>\n      <span>Destinatarios ></span>\n      <span class=\"span-1\">Enviar</span>\n    </div>\n  </ion-toolbar>\n\n  <ion-toolbar class=\"toolbar-input\">\n\n    <ion-list>\n      <ion-item>\n        <ion-label>Titulo: {{ titulo }}</ion-label>\n      </ion-item>\n      <ion-item>\n        <ion-label>Descripcion: {{ descripcion }}</ion-label>\n      </ion-item>\n      <!--<ion-item>\n        <ion-label>{{ pregunta }}</ion-label>\n        <ion-label>{{ opcion }}</ion-label>\n      </ion-item>-->\n      <ion-item>\n        <ion-label>Destinatarios: <b>{{ destinatarios.length }} usuarios</b></ion-label>\n      </ion-item>\n      <!--<ion-item *ngFor=\"let destinatario of destinatarios\">\n        <ion-label>{{ destinatario.usuario.rol }} > {{ destinatario.club.nombre }} > {{ destinatario.disciplina.nombre}}</ion-label>\n      </ion-item>-->\n    </ion-list>\n\n\n  </ion-toolbar>\n\n</ion-content>\n<ion-footer style=\"padding: 10px\">\n  <ion-button color=\"primary\" \n        expand=\"block\" (click)=\"siguiente()\">\n        ENVIAR</ion-button>\n</ion-footer>");

/***/ }),

/***/ "xnN5":
/*!*****************************************************************************************************!*\
  !*** ./src/app/pages/dashboard/notificaciones/crear-notificaciones/enviar/enviar-routing.module.ts ***!
  \*****************************************************************************************************/
/*! exports provided: EnviarPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EnviarPageRoutingModule", function() { return EnviarPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _enviar_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./enviar.page */ "fWXH");




const routes = [
    {
        path: '',
        component: _enviar_page__WEBPACK_IMPORTED_MODULE_3__["EnviarPage"]
    }
];
let EnviarPageRoutingModule = class EnviarPageRoutingModule {
};
EnviarPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], EnviarPageRoutingModule);



/***/ })

}]);
//# sourceMappingURL=enviar-enviar-module-es2015.js.map