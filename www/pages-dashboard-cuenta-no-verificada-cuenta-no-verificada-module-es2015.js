(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-dashboard-cuenta-no-verificada-cuenta-no-verificada-module"],{

/***/ "2TFo":
/*!*************************************************************************************!*\
  !*** ./src/app/pages/dashboard/cuenta-no-verificada/cuenta-no-verificada.module.ts ***!
  \*************************************************************************************/
/*! exports provided: CuentaNoVerificadaPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CuentaNoVerificadaPageModule", function() { return CuentaNoVerificadaPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _cuenta_no_verificada_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./cuenta-no-verificada-routing.module */ "GFKd");
/* harmony import */ var _cuenta_no_verificada_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./cuenta-no-verificada.page */ "YFEb");







let CuentaNoVerificadaPageModule = class CuentaNoVerificadaPageModule {
};
CuentaNoVerificadaPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _cuenta_no_verificada_routing_module__WEBPACK_IMPORTED_MODULE_5__["CuentaNoVerificadaPageRoutingModule"]
        ],
        declarations: [_cuenta_no_verificada_page__WEBPACK_IMPORTED_MODULE_6__["CuentaNoVerificadaPage"]]
    })
], CuentaNoVerificadaPageModule);



/***/ }),

/***/ "GFKd":
/*!*********************************************************************************************!*\
  !*** ./src/app/pages/dashboard/cuenta-no-verificada/cuenta-no-verificada-routing.module.ts ***!
  \*********************************************************************************************/
/*! exports provided: CuentaNoVerificadaPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CuentaNoVerificadaPageRoutingModule", function() { return CuentaNoVerificadaPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _cuenta_no_verificada_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./cuenta-no-verificada.page */ "YFEb");




const routes = [
    {
        path: '',
        component: _cuenta_no_verificada_page__WEBPACK_IMPORTED_MODULE_3__["CuentaNoVerificadaPage"]
    }
];
let CuentaNoVerificadaPageRoutingModule = class CuentaNoVerificadaPageRoutingModule {
};
CuentaNoVerificadaPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], CuentaNoVerificadaPageRoutingModule);



/***/ }),

/***/ "GPsF":
/*!***************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/dashboard/cuenta-no-verificada/cuenta-no-verificada.page.html ***!
  \***************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n  <div id=\"container\">\n    <div style=\"margin-top: 10px;\">\n      <img src=\"/assets/img/splash/cuentaSinVerificar.png\">\n    </div>\n\n    <div class=\"textoPrincipal\">\n      Aún el club no te verifico\n    </div>\n    <div class=\"textoSecundario\">\n      El club está chequeando la información\n    </div>\n    <div class=\"textoSecundario\">\n      y tiene que darte de alta. Esto puede\n    </div>\n    <div class=\"textoSecundario\">\n      demorar unas horas o incluso dias.\n    </div>\n    <div class=\"textoSecundario\">\n      Si tenés dudas contactacte con tu club.\n    </div>\n  </div>\n</ion-content>\n");

/***/ }),

/***/ "YFEb":
/*!***********************************************************************************!*\
  !*** ./src/app/pages/dashboard/cuenta-no-verificada/cuenta-no-verificada.page.ts ***!
  \***********************************************************************************/
/*! exports provided: CuentaNoVerificadaPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CuentaNoVerificadaPage", function() { return CuentaNoVerificadaPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_cuenta_no_verificada_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./cuenta-no-verificada.page.html */ "GPsF");
/* harmony import */ var _cuenta_no_verificada_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./cuenta-no-verificada.page.scss */ "wYHT");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");




let CuentaNoVerificadaPage = class CuentaNoVerificadaPage {
    constructor() { }
    ngOnInit() {
    }
};
CuentaNoVerificadaPage.ctorParameters = () => [];
CuentaNoVerificadaPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-cuenta-no-verificada',
        template: _raw_loader_cuenta_no_verificada_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_cuenta_no_verificada_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], CuentaNoVerificadaPage);



/***/ }),

/***/ "wYHT":
/*!*************************************************************************************!*\
  !*** ./src/app/pages/dashboard/cuenta-no-verificada/cuenta-no-verificada.page.scss ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("#container {\n  text-align: center;\n  position: absolute;\n  left: 0;\n  right: 0;\n  top: 50%;\n  transform: translateY(-50%);\n}\n\nion-content {\n  --height: 100%;\n  --background: #f4f3f3;\n}\n\n.textoPrincipal {\n  font-family: \"Roboto\", sans-serif;\n  font-style: normal;\n  font-weight: 550;\n  font-size: 26px;\n  line-height: 31px;\n  text-align: center;\n  color: #d90000;\n  margin-bottom: 10px;\n}\n\n.textoSecundario {\n  font-family: \"Roboto\", sans-serif;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 14px;\n  line-height: 21px;\n  align-items: center;\n  text-align: center;\n  color: #000000;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL2N1ZW50YS1uby12ZXJpZmljYWRhLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxPQUFBO0VBQ0EsUUFBQTtFQUNBLFFBQUE7RUFDQSwyQkFBQTtBQUNKOztBQUVBO0VBQ0ksY0FBQTtFQUNBLHFCQUFBO0FBQ0o7O0FBRUE7RUFDSSxpQ0FBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtBQUNKOztBQUVBO0VBQ0ksaUNBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7QUFDSiIsImZpbGUiOiJjdWVudGEtbm8tdmVyaWZpY2FkYS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjY29udGFpbmVyIHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGxlZnQ6IDA7XG4gICAgcmlnaHQ6IDA7XG4gICAgdG9wOiA1MCU7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01MCUpO1xufVxuXG5pb24tY29udGVudCB7XG4gICAgLS1oZWlnaHQ6IDEwMCU7XG4gICAgLS1iYWNrZ3JvdW5kOiAjZjRmM2YzO1xufVxuXG4udGV4dG9QcmluY2lwYWwge1xuICAgIGZvbnQtZmFtaWx5OiBcIlJvYm90b1wiLCBzYW5zLXNlcmlmO1xuICAgIGZvbnQtc3R5bGU6IG5vcm1hbDtcbiAgICBmb250LXdlaWdodDogNTUwO1xuICAgIGZvbnQtc2l6ZTogMjZweDtcbiAgICBsaW5lLWhlaWdodDogMzFweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgY29sb3I6ICNkOTAwMDA7XG4gICAgbWFyZ2luLWJvdHRvbTogMTBweDtcbn1cblxuLnRleHRvU2VjdW5kYXJpbyB7XG4gICAgZm9udC1mYW1pbHk6IFwiUm9ib3RvXCIsIHNhbnMtc2VyaWY7XG4gICAgZm9udC1zdHlsZTogbm9ybWFsO1xuICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIGxpbmUtaGVpZ2h0OiAyMXB4O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGNvbG9yOiAjMDAwMDAwO1xufVxuIl19 */");

/***/ })

}]);
//# sourceMappingURL=pages-dashboard-cuenta-no-verificada-cuenta-no-verificada-module-es2015.js.map