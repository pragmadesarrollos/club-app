(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["login-verificado-login-verificado-module"], {
    /***/
    "/A8G":
    /*!****************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/acceso/login-verificado/login-verificado.page.html ***!
      \****************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function A8G(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n\n  <div id=\"container\">\n    <div style=\"margin-top:-60px ;\">\n      <img src=\"/assets/img/splash/logoUser.png\">\n    </div>\n    <div class=\"holaNombre\">\n      ¡Hola {{ nombre }}!\n    </div>\n    <div class=\"textoAcceso\">\n      Tu correo esta verificado, ahora\n      <br>\n      accedé con tu correo contraseña\n    </div>\n    <form [formGroup]=\"form\" (ngSubmit)=\"validar()\">\n      <div class=\"input-group input-group-lg inputCorreo mt-4\">\n        <span class=\"input-group-text\" style=\"background-color: #e9ecef;\" id=\"inputGroup-sizing-lg\">\n          <ion-icon name=\"mail-outline\" class=\"mail-icon\"></ion-icon>\n          <!-- <svg\n            xmlns=\"http://www.w3.org/2000/svg\" width=\"20\" height=\"20\" fill=\"currentColor\" class=\"\" viewBox=\"0 0 16 16\">\n            <path\n              d=\"M6.5 1A1.5 1.5 0 0 0 5 2.5V3H1.5A1.5 1.5 0 0 0 0 4.5v8A1.5 1.5 0 0 0 1.5 14h13a1.5 1.5 0 0 0 1.5-1.5v-8A1.5 1.5 0 0 0 14.5 3H11v-.5A1.5 1.5 0 0 0 9.5 1h-3zm0 1h3a.5.5 0 0 1 .5.5V3H6v-.5a.5.5 0 0 1 .5-.5zm1.886 6.914L15 7.151V12.5a.5.5 0 0 1-.5.5h-13a.5.5 0 0 1-.5-.5V7.15l6.614 1.764a1.5 1.5 0 0 0 .772 0zM1.5 4h13a.5.5 0 0 1 .5.5v1.616L8.129 7.948a.5.5 0 0 1-.258 0L1 6.116V4.5a.5.5 0 0 1 .5-.5z\" />\n          </svg> -->\n        </span>\n        <input type=\"text\" class=\"form-control\" formControlName=\"correo\" placeholder=\"Correo electronico\"\n          style=\"border-style: none;\">\n        <span class=\" input-group-text\" style=\"background-color: #e9ecef; margin-right: 20px;\"\n          id=\"inputGroup-sizing-lg\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"35\" height=\"35\" fill=\"currentColor\"\n            class=\"bi bi-check\" viewBox=\"0 0 16 16\">\n            <path style=\"color:#42D267\"\n              d=\"M10.97 4.97a.75.75 0 0 1 1.07 1.05l-3.99 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.267.267 0 0 1 .02-.022z\" />\n          </svg></span>\n      </div>\n      <div class=\"input-group input-group-lg inputCorreo mt-2\">\n        <span class=\"input-group-text\" id=\"inputGroup-sizing-lg\"><svg xmlns=\"http://www.w3.org/2000/svg\" width=\"20\"\n            height=\"20\" fill=\"currentColor\" class=\"bi bi-key\" viewBox=\"0 0 16 16\">\n            <path\n              d=\"M0 8a4 4 0 0 1 7.465-2H14a.5.5 0 0 1 .354.146l1.5 1.5a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0L13 9.207l-.646.647a.5.5 0 0 1-.708 0L11 9.207l-.646.647a.5.5 0 0 1-.708 0L9 9.207l-.646.647A.5.5 0 0 1 8 10h-.535A4 4 0 0 1 0 8zm4-3a3 3 0 1 0 2.712 4.285A.5.5 0 0 1 7.163 9h.63l.853-.854a.5.5 0 0 1 .708 0l.646.647.646-.647a.5.5 0 0 1 .708 0l.646.647.646-.647a.5.5 0 0 1 .708 0l.646.647.793-.793-1-1h-6.63a.5.5 0 0 1-.451-.285A3 3 0 0 0 4 5z\" />\n            <path d=\"M4 8a1 1 0 1 1-2 0 1 1 0 0 1 2 0z\" />\n          </svg></span>\n        <input type=\"password\" class=\"form-control\" formControlName=\"password\" placeholder=\"Contraseña\"\n          autocomplete=\"off\" style=\"margin-right: 20px;  border-style: none; height: 5rem;\">\n\n        <ion-fab horizontal=\"end\" class=\"icon-eye\">\n          <ion-icon name=\"eye-outline\" class=\"icon\" color=\"medium\"></ion-icon>\n        </ion-fab>\n\n      </div>\n      <ion-button type=\"submit\" size=\"large\" [disabled]=\"form.invalid\" class=\"btnIngresar mt-2\" color=\"proclub\"\n        expand=\"block\">INGRESAR</ion-button>\n    </form>\n    <div class=\"recuperarClave\" (click)=\"recuperarClave()\">\n      No sé mi clave\n    </div>\n  </div>\n</ion-content>\n<ion-footer class=\"ion-no-border\">\n  <ion-toolbar class=\"ion-text-center\">\n    <img routerLink=\"/login\" src=\"/assets/img/splash/ProClubLogo.png\">\n  </ion-toolbar>\n</ion-footer>";
      /***/
    },

    /***/
    "84wE":
    /*!**************************************************************************!*\
      !*** ./src/app/pages/acceso/login-verificado/login-verificado.module.ts ***!
      \**************************************************************************/

    /*! exports provided: LoginVerificadoPageModule */

    /***/
    function wE(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginVerificadoPageModule", function () {
        return LoginVerificadoPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _login_verificado_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./login-verificado-routing.module */
      "ybL2");
      /* harmony import */


      var _login_verificado_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./login-verificado.page */
      "eYru");

      var LoginVerificadoPageModule = function LoginVerificadoPageModule() {
        _classCallCheck(this, LoginVerificadoPageModule);
      };

      LoginVerificadoPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _login_verificado_routing_module__WEBPACK_IMPORTED_MODULE_5__["LoginVerificadoPageRoutingModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"]],
        declarations: [_login_verificado_page__WEBPACK_IMPORTED_MODULE_6__["LoginVerificadoPage"]]
      })], LoginVerificadoPageModule);
      /***/
    },

    /***/
    "EjJx":
    /*!*********************************************************!*\
      !*** ./node_modules/jwt-decode/build/jwt-decode.esm.js ***!
      \*********************************************************/

    /*! exports provided: default, InvalidTokenError */

    /***/
    function EjJx(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "InvalidTokenError", function () {
        return n;
      });

      function e(e) {
        this.message = e;
      }

      e.prototype = new Error(), e.prototype.name = "InvalidCharacterError";

      var r = "undefined" != typeof window && window.atob && window.atob.bind(window) || function (r) {
        var t = String(r).replace(/=+$/, "");
        if (t.length % 4 == 1) throw new e("'atob' failed: The string to be decoded is not correctly encoded.");

        for (var n, o, a = 0, i = 0, c = ""; o = t.charAt(i++); ~o && (n = a % 4 ? 64 * n + o : o, a++ % 4) ? c += String.fromCharCode(255 & n >> (-2 * a & 6)) : 0) {
          o = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".indexOf(o);
        }

        return c;
      };

      function t(e) {
        var t = e.replace(/-/g, "+").replace(/_/g, "/");

        switch (t.length % 4) {
          case 0:
            break;

          case 2:
            t += "==";
            break;

          case 3:
            t += "=";
            break;

          default:
            throw "Illegal base64url string!";
        }

        try {
          return function (e) {
            return decodeURIComponent(r(e).replace(/(.)/g, function (e, r) {
              var t = r.charCodeAt(0).toString(16).toUpperCase();
              return t.length < 2 && (t = "0" + t), "%" + t;
            }));
          }(t);
        } catch (e) {
          return r(t);
        }
      }

      function n(e) {
        this.message = e;
      }

      function o(e, r) {
        if ("string" != typeof e) throw new n("Invalid token specified");
        var o = !0 === (r = r || {}).header ? 0 : 1;

        try {
          return JSON.parse(t(e.split(".")[o]));
        } catch (e) {
          throw new n("Invalid token specified: " + e.message);
        }
      }

      n.prototype = new Error(), n.prototype.name = "InvalidTokenError";
      /* harmony default export */

      __webpack_exports__["default"] = o; //# sourceMappingURL=jwt-decode.esm.js.map

      /***/
    },

    /***/
    "eYru":
    /*!************************************************************************!*\
      !*** ./src/app/pages/acceso/login-verificado/login-verificado.page.ts ***!
      \************************************************************************/

    /*! exports provided: LoginVerificadoPage */

    /***/
    function eYru(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginVerificadoPage", function () {
        return LoginVerificadoPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _raw_loader_login_verificado_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! raw-loader!./login-verificado.page.html */
      "/A8G");
      /* harmony import */


      var _login_verificado_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./login-verificado.page.scss */
      "w5IJ");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var src_app_services_login_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/services/login.service */
      "EFyh");
      /* harmony import */


      var src_app_services_usuario_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! src/app/services/usuario.service */
      "on2l");
      /* harmony import */


      var src_app_services_storage_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! src/app/services/storage.service */
      "n90K");
      /* harmony import */


      var jwt_decode__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! jwt-decode */
      "EjJx");

      var LoginVerificadoPage = /*#__PURE__*/function () {
        function LoginVerificadoPage(router, fb, _usuarioService, _loginService, _storageService, toastController, loadingController, alertController) {
          _classCallCheck(this, LoginVerificadoPage);

          this.router = router;
          this.fb = fb;
          this._usuarioService = _usuarioService;
          this._loginService = _loginService;
          this._storageService = _storageService;
          this.toastController = toastController;
          this.loadingController = loadingController;
          this.alertController = alertController;
          this.nombre = 'xXx';
          this.userInfo = {
            nombre: '',
            rol: '',
            club: ''
          };
          this.form = this.fb.group({
            correo: [{
              value: this._usuarioService.nombreUsuario,
              disabled: true
            }, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]
          });
        }

        _createClass(LoginVerificadoPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.obtenerNombre();
          }
        }, {
          key: "getDecodedAccessToken",
          value: function getDecodedAccessToken(token) {
            try {
              return Object(jwt_decode__WEBPACK_IMPORTED_MODULE_10__["default"])(token);
            } catch (Error) {
              return null;
            }
          }
        }, {
          key: "obtenerNombre",
          value: function obtenerNombre() {
            var _this = this;

            var mail = this._usuarioService.nombreUsuario;

            this._usuarioService.validarUsuario(mail).subscribe(function (d) {
              _this.nombre = d && d.message ? d.message : 'xXx';
            });
          }
        }, {
          key: "recuperarClave",
          value: function recuperarClave() {
            this.router.navigateByUrl('/login/olvidaste-tu-clave'); //this.presentAlertConfirm();
          }
          /*async presentAlertConfirm() {
            const alert = await this.alertController.create({
              cssClass: 'my-custom-class',
              header: 'Recuperar contraseña',
              message: 'Está seguro que desea recuperar su contraseña?',
              buttons: [
                {
                  text: 'Cancelar',
                  role: 'cancel',
                  cssClass: 'secondary',
                  handler: (blah) => {
                    console.log('Operacion cancelada');
                  }
                }, {
                  text: 'Aceptar',
                  handler: () => {
                    console.log('Operacion aceptada');
                    this.recuperar();
                  }
                }
              ]
            });
               await alert.present();
          }*/

          /*recuperar() {
            const usuario: any = {
              email: this._usuarioService.nombreUsuario
            }
               this.presentLoading().then((loadRes: any) => {
              this.loading = loadRes
              this.loading.present()
                 this._usuarioService.recuperarPassword(usuario).subscribe(data => {
                console.log(data);
                this.dismissLoading();
                this.mensaje('Enviamos un correo, revise su bandeja de entrada');
              }, error => {
                console.log(error);
                this.dismissLoading();
                this.mensaje('Opss.. ocurrio un error');
              })
            })
          }*/

        }, {
          key: "validar",
          value: function validar() {
            var _this2 = this;

            var correo = this._usuarioService.nombreUsuario;
            var password = this.form.value.password;
            var usuario = {
              email: correo,
              password: password
            };
            this.presentLoading().then(function (loadRes) {
              _this2.loading = loadRes;

              _this2.loading.present();

              _this2._loginService.login(usuario).subscribe(function (data) {
                // this._storageService.set('token', data.token);
                localStorage.setItem('token', data.token);

                var tokenInfo = _this2.getDecodedAccessToken(data.token);

                var expireDate = tokenInfo.exp;
                localStorage.setItem('tokenInfo', JSON.stringify(tokenInfo));
                console.log('tokenInfo ', tokenInfo);

                _this2._usuarioService.obtenerUsuario(tokenInfo.userId).subscribe(function (data) {
                  _this2.userInfo.nombre = data.persona['nombre'];
                  _this2.userInfo.rol = tokenInfo.rol;
                  _this2.userInfo.club = 'club';
                  /*this.fcm.getToken().then(token => {
                    console.log(token)
                    this._usuarioService.guardarToken(token).subscribe(data => {
                        console.log(data)
                    });
                  });*/

                  _this2._storageService.localSet('userInfo', _this2.userInfo);

                  _this2.dismissLoading();

                  _this2.router.navigateByUrl('/menu');
                }, function (error) {
                  _this2.dismissLoading();

                  console.log(error);

                  _this2.mensaje('Opss.. ocurrio un error');
                }); // console.log(
                //   this._storageService.get('token')
                // )

              }, function (error) {
                console.log(error);

                _this2.dismissLoading();

                _this2.error(error);
              });
            });
          }
        }, {
          key: "error",
          value: function error(_error) {
            switch (_error.error.error) {
              case 'su estado esta pendiente no puede ingresar':
                this.mensaje('Su estado esta pendiente, no puede ingresar');
                break;

              case 'The password is invalid or the user does not have a password.':
                this.mensaje('Usuario o contraseña invalidos');
                this.form.controls.password.reset(); //this.form.reset();

                break;

              case 'Access to this account has been temporarily disabled due to many failed login attempts. You can immediately restore it by resetting your password or you can try again later.':
                this.mensaje('Muchos intentos fallidos. Intente mas tarde');
                break;

              default:
                this.mensaje('Opss.. ocurrio un error');
                break;
            }
          }
        }, {
          key: "presentLoading",
          value: function presentLoading() {
            var _this3 = this;

            if (this.loading) {
              this.loading.dismiss();
            }

            return new Promise(function (resolve) {
              resolve(_this3.loadingController.create({
                message: 'Espere...'
              }));
            });
          }
        }, {
          key: "dismissLoading",
          value: function dismissLoading() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      if (this.loading) {
                        this.loading.dismiss();
                      }

                    case 1:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "mensaje",
          value: function mensaje(_mensaje) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var toast;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      _context2.next = 2;
                      return this.toastController.create({
                        message: _mensaje,
                        color: 'dark',
                        duration: 3000
                      });

                    case 2:
                      toast = _context2.sent;
                      toast.present();

                    case 4:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }]);

        return LoginVerificadoPage;
      }();

      LoginVerificadoPage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]
        }, {
          type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"]
        }, {
          type: src_app_services_usuario_service__WEBPACK_IMPORTED_MODULE_8__["UsuarioService"]
        }, {
          type: src_app_services_login_service__WEBPACK_IMPORTED_MODULE_7__["LoginService"]
        }, {
          type: src_app_services_storage_service__WEBPACK_IMPORTED_MODULE_9__["StorageService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ToastController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["LoadingController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"]
        }];
      };

      LoginVerificadoPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-login-verificado',
        template: _raw_loader_login_verificado_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_login_verificado_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
      })], LoginVerificadoPage);
      /***/
    },

    /***/
    "w5IJ":
    /*!**************************************************************************!*\
      !*** ./src/app/pages/acceso/login-verificado/login-verificado.page.scss ***!
      \**************************************************************************/

    /*! exports provided: default */

    /***/
    function w5IJ(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-content {\n  --height: 100%;\n  --background: linear-gradient(0deg, #f4f3f3 50%, #6b64f8 50%);\n}\n\nion-toolbar {\n  --background: #f4f3f3;\n}\n\nimg {\n  width: 150px;\n}\n\n#container {\n  text-align: center;\n  position: absolute;\n  left: 0;\n  right: 0;\n  top: 50%;\n  transform: translateY(-50%);\n}\n\n.holaNombre {\n  font-family: \"Roboto\", sans-serif;\n  font-style: normal;\n  font-weight: 500;\n  font-size: 26px;\n  line-height: 31px;\n  text-align: center;\n  color: #ffffff;\n}\n\n.textoAcceso {\n  margin-top: 20px;\n  font-family: \"Roboto\", sans-serif;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 16px;\n  line-height: 19px;\n  text-align: center;\n  color: #ffffff;\n}\n\n.inputCorreo {\n  margin-left: 10px;\n  margin-right: 10px;\n}\n\n.mail-icon {\n  font-size: 25px;\n}\n\n.icon {\n  font-size: 25px;\n}\n\n.icon-eye {\n  margin-right: 30px;\n  margin-top: 20px;\n}\n\n.btnIngresar {\n  margin-left: 10px;\n  margin-right: 10px;\n  font-size: 15px;\n  font-family: \"Roboto\", sans-serif;\n}\n\n.btnIngresar[disabled] {\n  opacity: 1;\n  --ion-color-base: grey;\n}\n\n.input-group-text {\n  border-style: none;\n  background-color: white;\n}\n\n.form-control {\n  padding-top: 15px;\n  padding-left: 0;\n}\n\n.form-control:focus {\n  border-color: rgba(255, 255, 255, 0.9);\n  box-shadow: inset 0 1px 1px rgba(255, 255, 255, 0.3), 0 0 8px rgba(255, 255, 255, 0.3);\n}\n\n.recuperarClave {\n  font-family: \"Roboto\", sans-serif;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 12px;\n  line-height: 14px;\n  /* identical to box height */\n  padding-top: 30px;\n  -webkit-text-decoration-line: underline;\n          text-decoration-line: underline;\n  color: #000000;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL2xvZ2luLXZlcmlmaWNhZG8ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsY0FBQTtFQUNBLDZEQUFBO0FBQ0Y7O0FBRUE7RUFDRSxxQkFBQTtBQUNGOztBQUVBO0VBQ0UsWUFBQTtBQUNGOztBQUVBO0VBQ0Usa0JBQUE7RUFDQSxrQkFBQTtFQUNBLE9BQUE7RUFDQSxRQUFBO0VBQ0EsUUFBQTtFQUNBLDJCQUFBO0FBQ0Y7O0FBRUE7RUFDRSxpQ0FBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7QUFDRjs7QUFFQTtFQUNFLGdCQUFBO0VBQ0EsaUNBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0FBQ0Y7O0FBRUE7RUFDRSxpQkFBQTtFQUNBLGtCQUFBO0FBQ0Y7O0FBRUE7RUFDRSxlQUFBO0FBQ0Y7O0FBRUE7RUFDRSxlQUFBO0FBQ0Y7O0FBRUE7RUFDRSxrQkFBQTtFQUNBLGdCQUFBO0FBQ0Y7O0FBRUE7RUFDRSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLGlDQUFBO0FBQ0Y7O0FBRUU7RUFDRSxVQUFBO0VBQ0Esc0JBQUE7QUFBSjs7QUFJQTtFQUNFLGtCQUFBO0VBQ0EsdUJBQUE7QUFERjs7QUFJQTtFQUNFLGlCQUFBO0VBQ0EsZUFBQTtBQURGOztBQUlBO0VBQ0Usc0NBQUE7RUFDQSxzRkFBQTtBQURGOztBQUlBO0VBQ0UsaUNBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsNEJBQUE7RUFDQSxpQkFBQTtFQUNBLHVDQUFBO1VBQUEsK0JBQUE7RUFFQSxjQUFBO0FBRkYiLCJmaWxlIjoibG9naW4tdmVyaWZpY2Fkby5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudCB7XG4gIC0taGVpZ2h0OiAxMDAlO1xuICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgwZGVnLCAjZjRmM2YzIDUwJSwgIzZiNjRmOCA1MCUpO1xufVxuXG5pb24tdG9vbGJhciB7XG4gIC0tYmFja2dyb3VuZDogI2Y0ZjNmMztcbn1cblxuaW1nIHtcbiAgd2lkdGg6IDE1MHB4O1xufVxuXG4jY29udGFpbmVyIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IDA7XG4gIHJpZ2h0OiAwO1xuICB0b3A6IDUwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01MCUpO1xufVxuXG4uaG9sYU5vbWJyZSB7XG4gIGZvbnQtZmFtaWx5OiBcIlJvYm90b1wiLCBzYW5zLXNlcmlmO1xuICBmb250LXN0eWxlOiBub3JtYWw7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIGZvbnQtc2l6ZTogMjZweDtcbiAgbGluZS1oZWlnaHQ6IDMxcHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgY29sb3I6ICNmZmZmZmY7XG59XG5cbi50ZXh0b0FjY2VzbyB7XG4gIG1hcmdpbi10b3A6IDIwcHg7XG4gIGZvbnQtZmFtaWx5OiBcIlJvYm90b1wiLCBzYW5zLXNlcmlmO1xuICBmb250LXN0eWxlOiBub3JtYWw7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgbGluZS1oZWlnaHQ6IDE5cHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgY29sb3I6ICNmZmZmZmY7XG59XG5cbi5pbnB1dENvcnJlbyB7XG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG59XG5cbi5tYWlsLWljb24ge1xuICBmb250LXNpemU6IDI1cHg7XG59XG5cbi5pY29uIHtcbiAgZm9udC1zaXplOiAyNXB4O1xufVxuXG4uaWNvbi1leWUge1xuICBtYXJnaW4tcmlnaHQ6IDMwcHg7XG4gIG1hcmdpbi10b3A6IDIwcHg7XG59XG5cbi5idG5JbmdyZXNhciB7XG4gIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgZm9udC1mYW1pbHk6ICdSb2JvdG8nLFxuICAgIHNhbnMtc2VyaWY7XG5cbiAgJltkaXNhYmxlZF0ge1xuICAgIG9wYWNpdHk6IDE7XG4gICAgLS1pb24tY29sb3ItYmFzZTogZ3JleTtcbiAgfVxufVxuXG4uaW5wdXQtZ3JvdXAtdGV4dCB7XG4gIGJvcmRlci1zdHlsZTogbm9uZTtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG59XG5cbi5mb3JtLWNvbnRyb2wge1xuICBwYWRkaW5nLXRvcDogMTVweDtcbiAgcGFkZGluZy1sZWZ0OiAwO1xufVxuXG4uZm9ybS1jb250cm9sOmZvY3VzIHtcbiAgYm9yZGVyLWNvbG9yOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuOSk7XG4gIGJveC1zaGFkb3c6IGluc2V0IDAgMXB4IDFweCByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuMyksIDAgMCA4cHggcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjMpO1xufVxuXG4ucmVjdXBlcmFyQ2xhdmUge1xuICBmb250LWZhbWlseTogXCJSb2JvdG9cIiwgc2Fucy1zZXJpZjtcbiAgZm9udC1zdHlsZTogbm9ybWFsO1xuICBmb250LXdlaWdodDogbm9ybWFsO1xuICBmb250LXNpemU6IDEycHg7XG4gIGxpbmUtaGVpZ2h0OiAxNHB4O1xuICAvKiBpZGVudGljYWwgdG8gYm94IGhlaWdodCAqL1xuICBwYWRkaW5nLXRvcDogMzBweDtcbiAgdGV4dC1kZWNvcmF0aW9uLWxpbmU6IHVuZGVybGluZTtcblxuICBjb2xvcjogIzAwMDAwMDtcbn1cbiJdfQ== */";
      /***/
    },

    /***/
    "ybL2":
    /*!**********************************************************************************!*\
      !*** ./src/app/pages/acceso/login-verificado/login-verificado-routing.module.ts ***!
      \**********************************************************************************/

    /*! exports provided: LoginVerificadoPageRoutingModule */

    /***/
    function ybL2(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginVerificadoPageRoutingModule", function () {
        return LoginVerificadoPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _login_verificado_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./login-verificado.page */
      "eYru");

      var routes = [{
        path: '',
        component: _login_verificado_page__WEBPACK_IMPORTED_MODULE_3__["LoginVerificadoPage"]
      }];

      var LoginVerificadoPageRoutingModule = function LoginVerificadoPageRoutingModule() {
        _classCallCheck(this, LoginVerificadoPageRoutingModule);
      };

      LoginVerificadoPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], LoginVerificadoPageRoutingModule);
      /***/
    }
  }]);
})();
//# sourceMappingURL=login-verificado-login-verificado-module-es5.js.map